# Contributing to the Business Model Sustainability Toolkit

So you'd like to contribute changes to the software or content of the Business Model Sustainability Toolkit. Thank you! This will be a quick guide to doing so.

## Preferred Method of Making Changes

The simplest way to contribute changes is to [open an issue in the GitLab project](https://gitlab.com/dial/principles/sustainability-toolkit/-/issues) describing the change you'd like to see. This will allow a DIAL team member to weigh in about whether the change is likely or unlikely to be accepted; is already being developed; or if they'd like for you to try and implement the change first an issue a merge request.

To make and submit a change yourself, here is the preferred process:

1. [Fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) to your own GitLab account space. (Internal DIAL team members can skip this step, as they will be able to create branches on the canonical project.)
2. [Create a new branch](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html#how-to-create-a-branch) using `development` as the starting point. Name the branch in lower-kebab-case format, prefixed with an issue number if one applies. Example: `160-update-odk-references` would be a good branch name for making changes that address [issue #160](https://gitlab.com/dial/principles/sustainability-toolkit/-/issues/160).
3. If you're making software changes, [follow the instructions for setting up and running the project locally](https://gitlab.com/dial/principles/sustainability-toolkit/-/blob/development/README.md). If you're only making content changes, you can probably get away with not setting up the project locally, although you'll be "flying blind" (unable to see your changes). We recommend only skipping the local setup if you're making trivial content edits (correcting typos, etc.).
4. Make your changes and commit your work in sensibly-organized commits using [good commit messages](https://cbea.ms/git-commit/).
   - Each commit should be small and specific enough to describe in a few words, e.g. "Change default API port to 3001" or "Correct links to Fondation Botnar"
   - The first line (title) of the commit should be short, under 50 characters.
   - The rest of the commit body can be as long as needed to describe what's happening in the commit, where it's not self-evident.
   - Please hard-wrap the commit body at 72 characters.
   - Please include relevant issue numbers.
   - Look through [the project commit history](https://gitlab.com/dial/principles/sustainability-toolkit/-/commits/development) for examples of our preferred commit messages and organization. Remember that commit messages are useful for other developers to understand what you did and why you did it!
5. Once you've made and committed all your changes, [open a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) to the `development` branch of the project. A DIAL team member will review your MR and provide feedback about any requested changes.

## Contributing Software Changes

To contribute software changes to the project, you will need to first get the software running on your local machine. We created the [Sustainability Toolkit Developer Environment](https://gitlab.com/dial/principles/sustainability-toolkit-devenv) repository to make this as easy as possible, and we recommend you start there.

Once you have the software running on your local machine, you can follow the [Preferred Method of Making Changes](#preferred-method-of-making-changes) instructions above.

## Contributing Content Changes

Guide content is primarily located in the `/docs/guide/` directory, which should be intuitive to navigate. In some of the files, you will notice other files being imported from outside this directory; these are usually interactive tools, which are stored in `/src/components/tools/`. Additionally, some non-Guide content pages are stored in `/src/pages/`.

This site is built using [Docusaurus](https://docusaurus.io/), and the format of these documents is [Markdown](https://docusaurus.io/docs/next/markdown-features) (with some additional extensions provided by Docusaurus). You should familiarize yourself with Markdown formatting before editing any of the content, and try to follow the formatting patterns that already exist.

As noted in [Preferred Method of Making Changes](#preferred-method-of-making-changes), it's preferable that you actually [setup and run the software on your local machine](#contributing-software-changes) in order to make content changes, since this will let you view your changes locally before submitting the change request. However, that's not the only way to suggest content changes. GitLab offers a [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) that allows you to make content changes directly on the GitLab website, create commits, and create merge requests. This is a simpler way of suggesting changes without setting up the entire software stack on your local machine, although you won't be able to preview your changes within the website this way.

### Steps for making changes via the GitLab Web IDE

1. Make sure you are logged into GitLab.
2. From within the repository file-browsing interface (such as [here](https://gitlab.com/dial/principles/sustainability-toolkit/-/tree/development)), you will see a "Web IDE" button near the top of the page. Click this button to open the Web IDE. (Make sure that the branch selector in the top left is on `development`.)
3. Use the file navigator to locate the file(s) you want to edit, and open them.
4. Make your desired changes. **If you are making content edits, please be very careful not to change any code such as JavaScript.**
   - If you close the window or tab that contains the Web IDE without committing your changes, your changes will be lost. So while you can open and close **files** within the Web IDE interface, do not close the overall browser window!
   - Use the "Preview Markdown" tab near the top of the window to view a simplified rendering of your Markdown files. Note that not all formatting will be visible here, but it's a good way to check for things like bold, italics, and links.
   - As you change files, the "Commit" button (and changed file counter) in the bottom left should update.
5. When you've made all your desired edits, click the Commit button in the bottom left.
6. Enter your commit message following the guidance provided in [Preferred Method of Making Changes](#preferred-method-of-making-changes). The first line of your message will be the commit title, so it should only be 50 characters long and should succinctly summarize your edits. (E.g., "Correct name of UN Foundation".) Include a reference to a related issue, if there is one (e.g., "Addresses #150").
7. Select the "Create a new branch" option and give your branch a name, again following the guidance above.
8. Ensure the "Start a new merge request" box is checked.
9. Click "Commit". Your changes will be committed to a new branch, and a merge request form will automatically populate.
10. Fill out the Merge Request form, providing a simple title and description (which will likely already be pre-populated from your commit message).
11. Create the merge request!

A DIAL internal team member will take a look at your requested edits and suggest any changes, or accept your changes. Congratulations, you contributed to the Business Model Sustainability Toolkit!
