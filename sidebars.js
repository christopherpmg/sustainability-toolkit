/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
	guide: [
		{
			type: 'doc',
			id: 'guide/introduction',
			label: 'Introduction',
		},
		{
			type: 'category',
			label: 'Value Proposition',
			collapsed: true,
			items: [
				'guide/value-proposition/1.0-overview',
				'guide/value-proposition/1.1-product-lifecycle-management',
				'guide/value-proposition/1.2-adaptability',
				{
					type: 'doc',
					id: 'guide/value-proposition/1.2b-core-modular-hackable',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
				'guide/value-proposition/1.3-replicability',
				{
					type: 'doc',
					id: 'guide/value-proposition/1.3b-codification',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
				'guide/value-proposition/1.4-evidence-of-impact',
			],
		},
		{
			type: 'category',
			label: 'Customer Segments',
			collapsed: true,
			items: [
				'guide/customer-segments/2.0-overview',
				'guide/customer-segments/2.1-political-economy-of-aid',
				'guide/customer-segments/2.2-buyers-users-target-impact-segments',
				{
					type: 'doc',
					id: 'guide/customer-segments/2.2b-buti-segmentation',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Channels',
			collapsed: true,
			items: [
				'guide/channels/3.0-overview',
				'guide/channels/3.1-reaching-your-customer-segments',
				{
					type: 'doc',
					id: 'guide/channels/3.1b-channel-strategy-mix',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Customer Relationships',
			collapsed: true,
			items: [
				'guide/customer-relationships/4.0-overview',
				'guide/customer-relationships/4.1-building-relationships-with-aid-sector-customers',
				{
					type: 'doc',
					id: 'guide/customer-relationships/4.1b-decision-making-stakeholder-matrix',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Revenue Streams',
			collapsed: true,
			items: [
				'guide/revenue-streams/5.0-overview',
				{
					type: 'doc',
					id: 'guide/revenue-streams/5.1-revenue-models-in-the-aid-sector',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Key Activities',
			collapsed: true,
			items: [
				'guide/key-activities/6.0-overview',
				'guide/key-activities/6.1-identify-your-key-activities',
				'guide/key-activities/6.2-approaches-to-carry-out-your-key-activities',
				{
					type: 'doc',
					id: 'guide/key-activities/6.2b-agile-waterfall',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Key Resources',
			collapsed: true,
			items: [
				'guide/key-resources/7.0-overview',
				'guide/key-resources/7.1-resource-sustainability',
			],
		},
		{
			type: 'category',
			label: 'Key Partnerships',
			collapsed: true,
			items: [
				'guide/key-partnerships/8.0-overview',
				'guide/key-partnerships/8.1-inner-value-network',
				{
					type: 'doc',
					id: 'guide/key-partnerships/8.1b-value-network',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
				'guide/key-partnerships/8.2-outer-value-network',
				{
					type: 'doc',
					id: 'guide/key-partnerships/8.2b-component-comparison',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Cost Structure',
			collapsed: true,
			items: [
				'guide/cost-structure/9.0-overview',
				'guide/cost-structure/9.1-cost-management-and-cost-restrictions-in-the-aid-sector',
				{
					type: 'doc',
					id: 'guide/cost-structure/9.2-cost-reduction-models',
					className: 'toolkit__menu__list-item--interactive-tool',
				},
			],
		},
		{
			type: 'category',
			label: 'Organizational Development',
			collapsed: true,
			items: [
				'guide/organizational-development/10.0-overview',
				'guide/organizational-development/10.1-organizational-structure',
				'guide/organizational-development/10.2-organizational-culture',
				'guide/organizational-development/10.3-organizational-model',
			],
		},
		{
			type: 'category',
			label: 'End Game',
			collapsed: true,
			items: [
				'guide/end-game/11.0-overview',
				'guide/end-game/11.1-end-game-options',
			],
		},
		'guide/glossary',
	],
}

module.exports = sidebars
