# Recoil

We're using Recoil to track state across components. As Recoil is somewhat new and best practices aren't necessarily solidified yet, we wanted to be sure to document our intended organization of recoil code.

We're using a *somewhat modified version* of the organization described in [this article](https://wes-rast.medium.com/recoil-project-structure-best-practices-79e74a475caa).

## Directory structure

Code organization of our recoil objects are as follows:

* All recoil code is in the `/recoil` directory.
* All data is in the `/recoil/data` directory.
* `Atoms` get their own folder in the data directory `/recoil/data/<nameOfAtomWithoutSuffix>`.
* `Selectors` are in the same directory as the atom they're modifying.
* `Atom Effects` are in a `/recoil/effects` directory.

Filenames should match the name of the object they're exporting and exported objects should adhere to the naming conventions of this document.

## Naming Atoms

Suffix the atom classes and indexed folders with `Atom` (e.g. `interactiveStateAtom`).

## Naming selectors

Selectors should be named in terms of how they modify their atoms, and should be prefixed by `with` or `without`.

For instance:
* Correcting a string for basic typos: `withTypoCorrection`.
* Filtering a list of favorite foods to remove anything with dairy: `withoutDairy`.

## Adding Effects

We have a special directory of `effects` within `recoil` which contain [Atom Effects](https://recoiljs.org/docs/guides/atom-effects)

## Example directory

Below is an example directory structure with a single atom, selector, and atom effect.

```
- recoil
|- data
| |- interactiveState
| | |- interactiveStateAtom.jsx
| | |- withProgressReport.jsx
| | |- index.js
|- effects
| |- localStorageEffect.js
```
