---
slug: /guide/glossary
---

# Glossary

## Abbreviations

<dl>
	<dt id="api"><a href="#api">API</a></dt>
	<dd>Application programming interface</dd>
	<dt id="b-corp"><a href="#b-corp">B Corp</a></dt>
	<dd>Benefit Corporation</dd>
	<dt id="butis"><a href="#butis">BUTIs</a></dt>
	<dd>Buyer, user, and target impact segments</dd>
	<dt id="crm"><a href="#crm">CRM</a></dt>
	<dd>Customer relationship management</dd>
	<dt id="csr"><a href="#csr">CSR</a></dt>
	<dd>Corporate social responsibility</dd>
	<dt id="dial"><a href="#dial">DIAL</a></dt>
	<dd>Digital Impact Alliance</dd>
	<dt id="ela"><a href="#ela">ELA</a></dt>
	<dd>End-user license agreement</dd>
	<dt id="erp"><a href="#erp">ERP</a></dt>
	<dd>Enterprise resource planning</dd>
	<dt id="foss"><a href="#foss">FOSS</a></dt>
	<dd>Free and open source software</dd>
	<dt id="icr"><a href="#icr">ICR</a></dt>
	<dd>Indirect cost recovery</dd>
	<dt id="ict"><a href="#ict">ICT</a></dt>
	<dd>Information and communication technology</dd>
	<dt id="ingo"><a href="#ingo">INGO</a></dt>
	<dd>International nongovernmental organization</dd>
	<dt id="iot"><a href="#iot">IoT</a></dt>
	<dd>Internet of things</dd>
	<dt id="it"><a href="#it">IT</a></dt>
	<dd>Information technology</dd>
	<dt id="ivn"><a href="#ivn">IVN</a></dt>
	<dd>Inner value network</dd>
	<dt id="kii"><a href="#kii">KII</a></dt>
	<dd>Key informant interview</dd>
	<dt id="kpi"><a href="#kpi">KPI</a></dt>
	<dd>Key performance indicator</dd>
	<dt id="m&e"><a href="#m&e">M&E</a></dt>
	<dd>Monitoring and evaluation</dd>
	<dt id="mno"><a href="#mno">MNO</a></dt>
	<dd>Mobile network operator</dd>
	<dt id="mvp"><a href="#mvp">MVP</a></dt>
	<dd>Minimum viable product</dd>
	<dt id="ngo"><a href="#ngo">NGO</a></dt>
	<dd>Nongovernmental organization</dd>
	<dt id="oda"><a href="#oda">ODA</a></dt>
	<dd>Overseas development assistance</dd>
	<dt id="oecd-dac"><a href="#oecd-dac">OECD DAC</a></dt>
	<dd>Organisation for Economic Co-operation and Development’s Development Assistance Committee</dd>
	<dt id="ovn"><a href="#ovn">OVN</a></dt>
	<dd>Outer value network</dd>
	<dt id="plm"><a href="#plm">PLM</a></dt>
	<dd>Product lifecycle management</dd>
	<dt id="sdgs"><a href="#sdgs">SDGs</a></dt>
	<dd>Sustainable Development Goals</dd>
	<dt id="sms"><a href="#sms">SMS</a></dt>
	<dd>Short message service</dd>
	<dt id="sroi"><a href="#sroi">SROI</a></dt>
	<dd>Social return on investment</dd>
	<dt id="vfm"><a href="#vfm">VFM</a></dt>
	<dd>Value for money</dd>
	<dt id="un"><a href="#un">UN</a></dt>
	<dd>United Nations</dd>
	<dt id="undesa"><a href="#undesa">UNDESA</a></dt>
	<dd>United Nations Department of Social and Economic Affairs</dd>
	<dt id="uneca"><a href="#uneca">UNECA</a></dt>
	<dd>United Nations Economic Commission for Africa</dd>
	<dt id="unocha"><a href="#unocha">UNOCHA</a></dt>
	<dd>United Nations Office of the Coordination of Humanitarian Affairs</dd>
</dl>

## Glossary

<dl>
	<dt id="adaptability"><a href="#adaptability">Adaptability</a></dt>
	<dd>The ability of a solution to be modified or adjusted for different users and contexts.</dd>
	<dt id="agile"><a href="#agile">Agile</a></dt>
	<dd>An iterative approach to project work based on short life stages. This is particularly useful for digital solution development as it allows managers to update and create a more responsive product.</dd>
	<dt id="aid-sector"><a href="#aid-sector">Aid sector</a></dt>
	<dd>This is composed of two different sectors: the humanitarian sector and the development sector.</dd>
	<dt id="balance-sheet"><a href="#balance-sheet">Balance sheet</a></dt>
	<dd>A report showing the financial condition of the organization at a specific moment in time. Also referred to as a statement of financial position.</dd>
	<dt id="benefit-corporation-b-corp"><a href="#benefit-corporation-b-corp">Benefit Corporation (B Corp)</a></dt>
	<dd>Term used for a for-profit entity that is certified by the nonprofit B Lab as voluntarily meeting high standards of transparency, accountability, and performance.</dd>
	<dt id="bilateral-aid"><a href="#bilateral-aid">Bilateral aid</a></dt>
	<dd>Aid that is transferred directly from the donor country to another government.</dd>
	<dt id="break-even-point"><a href="#break-even-point">Break-even point</a></dt>
	<dd>The point at which total revenue and total cost are equal, meaning there is no profit or loss for the organization.</dd>
	<dt id="buyers"><a href="#buyers">Buyers</a></dt>
	<dd>Those who are involved in the purchasing decision and actually purchase and/or adopt the product. One of the three distinct customer segments.</dd>
	<dt id="cash-flow"><a href="#cash-flow">Cash flow</a></dt>
	<dd>The movement of cash into and out of an organization or the difference between cash receipts and cash disbursements over a period of time.</dd>
	<dt id="channels"><a href="#channels">Channels</a></dt>
	<dd>How an organization communicates with and reaches its customer segments to deliver a value proposition. They include online/social media, conferences, convening, word of mouth, partnering, and procurement processes.</dd>
	<dt id="clockspeed"><a href="#clockspeed">Clockspeed</a></dt>
	<dd>The lifecycles that underpin an industry. In the aid sector, the clockspeed is the timing and sequencing of core processes, such as the longevity of big INGOs, short funding cycles (typically three to five years for development aid and three to 12 months for humanitarian aid), and the impact of policy review cycles.</dd>
	<dt id="codification"><a href="#codification">Codification</a></dt>
	<dd>The process of documenting and sometimes automating the key components of your solution, such as systems, processes, rules, decisions, and intangible knowledge.</dd>
	<dt id="conferences"><a href="#conferences">Conferences</a></dt>
	<dd>Formal events attracting a large number of participants. They are considered a channel for engaging with buyers and users.</dd>
	<dt id="contribution"><a href="#contribution">Contribution</a></dt>
	<dd>A donation, gift, or transfer of cash or other assets.</dd>
	<dt id="convening"><a href="#convening">Convening</a></dt>
	<dd>A less formal gathering than a conference that is designed to bring people together in more organic forums to network and learn together. Examples include webinars, in-person meetups, workshops, training series, and host speakers.</dd>
	<dt id="cost-structure"><a href="#cost-structure">Cost structure</a></dt>
	<dd>All the costs and expenses an organization incurs to operate its business model.</dd>
	<dt id="crowdsourcing"><a href="#crowdsourcing">Crowdsourcing</a></dt>
	<dd>Using a disparate group of people to provide knowledge, skills, and expertise for your organization, often utilizing online platforms.</dd>
	<dt id="customer-relationships"><a href="#customer-relationships">Customer relationships</a></dt>
	<dd>The type of interaction an organization establishes with specific customers, i.e., the buyer, user, and target impact segments.</dd>
	<dt id="customer-segment"><a href="#customer-segment">Customer segment</a></dt>
	<dd>The different groups of people an organization aims to reach and serve. They include the buyer, user, and target impact segments.</dd>
	<dt id="digital-solutions"><a href="#digital-solutions">Digital solutions</a></dt>
	<dd>The combination of digital products or services and the systems and processes they integrate into, such as training, teaching, and data collection activities.</dd>
	<dt id="direct-costs"><a href="#direct-costs">Direct costs</a></dt>
	<dd>Expenses that can be traced to a specific cost object, which may be a specific product, service, or project. They can include expenses related to software and equipment, as well as labor if it is specific to the cost object.</dd>
	<dt id="e-waste"><a href="#e-waste">E-waste</a></dt>
	<dd>Discarded electronic appliances such as mobile phones that may pose a risk to people and the environment. E-waste should be a consideration when developing a digital solution.</dd>
	<dt id="end-game"><a href="#end-game">End game</a></dt>
	<dd>How a product or service is delivered when it is fully mature. It is particularly focused on the organization or networks that will be responsible for the ongoing delivery of the digital product or service.</dd>
	<dt id="financial-sustainability"><a href="#financial-sustainability">Financial sustainability</a></dt>
	<dd>The way in which an organization generates revenue from various sources to cover its costs and continue operating.</dd>
	<dt id="funding-gaps"><a href="#funding-gaps">Funding gaps</a></dt>
	<dd>The significant gaps in funding that can create crippling cash flow issues.</dd>
	<dt id="funding-problem"><a href="#funding-problem">Funding problem</a></dt>
	<dd>A problem the digital solution is addressing that is known, understood, and seen as within scope, but must also be adequately funded by the aid sector.</dd>
	<dt id="geographically-tied-funding"><a href="#geographically-tied-funding">Geographically tied funding</a></dt>
	<dd>Funding for a specific country and often for a particular part of the country.</dd>
	<dt id="grants"><a href="#grants">Grants</a></dt>
	<dd>Contributed assets given by an individual or organization with no reciprocal receipt of services or goods. Sometimes given with legal restrictions imposed upon its use.</dd>
	<dt id="income-statement"><a href="#income-statement">Income statement</a></dt>
	<dd>A financial report that summarizes income and expenses and resulting surplus or deficit for a given period of time. Also referred to as a profit and loss statement.</dd>
	<dt id="indirect-costs"><a href="#indirect-costs">Indirect costs</a></dt>
	<dd>Expenses that cannot be traced to a specific cost object but are needed for the general operation of an organization. They include expenses related to supplies, utilities, and administrative services, among others. Often referred to as overhead costs.</dd>
	<dt id="in-kind-contribution"><a href="#in-kind-contribution">In-kind contribution</a></dt>
	<dd>A contribution made in goods or services rather than cash.</dd>
	<dt id="inner-value-network"><a href="#inner-value-network">Inner value network</a></dt>
	<dd>A network composed of individuals and organizations that are involved in the core design and development of an organization’s digital solution or that have a close working relationship with the organization.</dd>
	<dt id="insourcing-of-activities"><a href="#insourcing-of-activities">Insourcing of activities</a></dt>
	<dd>The process of performing specific tasks or activities within the operational infrastructure of the organization. The transfer of skills back into the organization from an external source for a strategic purpose.</dd>
	<dt id="key-activities"><a href="#key-activities">Key activities</a></dt>
	<dd>The most critical tasks that the organization needs to perform in order to make the sustainable business model work.</dd>
	<dt id="key-partnerships"><a href="#key-partnerships">Key partnerships</a></dt>
	<dd>Other organizations that you have a formal working relationship with and are important for making the business model work and are involved in creating and delivering value in regards to your product or service.</dd>
	<dt id="key-resources"><a href="#key-resources">Key resources</a></dt>
	<dd>The most important physical, human, intellectual, or financial assets an organization uses to make its business model successful and sustainable.</dd>
	<dt id="known-problem"><a href="#known-problem">Known problem</a></dt>
	<dd>The problem the digital solution is addressing is well-known and has evidence to prove it is a problem.</dd>
	<dt id="lean"><a href="#lean">Lean</a></dt>
	<dd>An iterative approach, much like agile, that is predicated on resource optimization and continuous improvement and seeks to identify and solve problems.</dd>
	<dt id="lessons-learned"><a href="#lessons-learned">Lessons learned</a></dt>
	<dd>The learning gained—or the knowledge and understanding acquired—from the positive and negative experiences of a project.</dd>
	<dt id="minimum-viable-product-mvp"><a href="#minimum-viable-product-mvp">Minimum viable product (MVP)</a></dt>
	<dd>A product with enough features to attract early-adopter customers and validate a product idea early in the product development cycle. In industries such as software, the MVP can help the product team receive user feedback as quickly as possible to iterate and improve the product.</dd>
	<dt id="multilateral-aid"><a href="#multilateral-aid">Multilateral aid</a></dt>
	<dd>Aid that goes to international organizations to support their work.</dd>
	<dt id="online-or-social-media"><a href="#online-or-social-media">Online or social media</a></dt>
	<dd>A channel that involves using the internet to reach customer segments and stakeholders via a website or social media platforms, such as Facebook, LinkedIn, and Twitter.</dd>
	<dt id="organizational-development"><a href="#organizational-development">Organizational development</a></dt>
	<dd>The effort that focuses on an organization’s capability to change and achieve greater effectiveness by developing and improving strategy, structure, and processes.</dd>
	<dt id="outer-value-network"><a href="#outer-value-network">Outer value network</a></dt>
	<dd>A network of individuals and organizations that are linked to the inner value network of an organization but are not involved in the design and development of that organization’s digital solution and do not have a close working relationship with the organization.</dd>
	<dt id="outsourcing-of-activities"><a href="#outsourcing-of-activities">Outsourcing of activities</a></dt>
	<dd>The process of hiring an outside organization to complete specific tasks or activities for a strategic purpose. The transfer of skills to an external source.</dd>
	<dt id="overhead-costs"><a href="#overhead-costs">Overhead costs</a></dt>
	<dd>The costs of an organization’s general administration. See fixed costs.</dd>
	<dt id="partnering"><a href="#partnering">Partnering</a></dt>
	<dd>A commitment by organizations, governments, and businesses to work together. The working relationship can be very structured, such as in a joint venture, or it can be informal.</dd>
	<dt id="path-dependence"><a href="#path-dependence">Path dependence</a></dt>
	<dd>This occurs when a decision or way to do business is dependent on how it has been done in the past. It is not based on current conditions.</dd>
	<dt id="procurement-processes"><a href="#procurement-processes">Procurement processes</a></dt>
	<dd>The methods used to acquire a good or service by an organization, government, or business.</dd>
	<dt id="product-lifecycle-management"><a href="#product-lifecycle-management">Product lifecycle management</a></dt>
	<dd>The handling of a product as it moves through the stages of its product lifecycle (PLC): introduction, growth, maturity, decline, and end of life. The three key elements of PLM are software, processes, and methods.</dd>
	<dt id="replicability"><a href="#replicability">Replicability</a></dt>
	<dd>The ability of a solution to be easily copied or reproduced by anyone in different settings or areas.</dd>
	<dt id="revenue"><a href="#revenue">Revenue</a></dt>
	<dd>The income generated from providing the digital service or product to customers. Can be derived from a number of sources, such as grants, contracts, subscriptions, and donations.</dd>
	<dt id="sectorally-tied-funding"><a href="#sectorally-tied-funding">Sectorally tied funding</a></dt>
	<dd>Funding available for specific, well-defined sectors, such as health; education; and water, sanitation, and hygiene.</dd>
	<dt id="social-return-on-investment-sroi"><a href="#social-return-on-investment-sroi">Social return on investment (SROI)</a></dt>
	<dd>The Social Return on Investment (SROI) is a method that consents to measure social outputs and value social outcomes in monetary terms (Boyd 2004).</dd>
	<dt id="sustainability"><a href="#sustainability">Sustainability</a></dt>
	<dd>Creating long-term value by taking into consideration how a given organization operates without negatively impacting the environment, community, or society.</dd>
	<dt id="target-impact-segment"><a href="#target-impact-segment">Target impact segment</a></dt>
	<dd>Composed of the people who ultimately benefit from the digital solution and are the ones usually experiencing the problem the digital solution seeks to resolve. One of the three distinct customer segments.</dd>
	<dt id="users"><a href="#users">Users</a></dt>
	<dd>Those who physically use the digital solution and are the ones for whom the product was designed. One of the three distinct customer segments.</dd>
	<dt id="value-network"><a href="#value-network">Value network</a></dt>
	<dd>Composed of all the actors that are involved in creating and delivering value in regards to the product or service.</dd>
	<dt id="value-proposition"><a href="#value-proposition">Value proposition</a></dt>
	<dd>A combination of goods and services that provide value to the buyer, user, and target impact segments.</dd>
	<dt id="within-scope-problem"><a href="#within-scope-problem">Within scope problem</a></dt>
	<dd>The problem the digital solution is addressing is seen as a humanitarian or development problem or as being within scope for the aid sector or a proportion of aid funders.</dd>
	<dt id="word-of-mouth"><a href="#word-of-mouth">Word of mouth</a></dt>
	<dd>When one customer or stakeholder recommends a service or solution to another person.</dd>
</dl>
