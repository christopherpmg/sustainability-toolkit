---
slug: "."
sidebar_label: "Overview"
pagination_label: "Key Resources Overview"
relevant_principles: ['design-for-scale','build-for-sustainability','be-collaborative']
related_topics: ['value-proposition','key-activities']
---

import Link from '@docusaurus/Link'
import useBaseUrl from '@docusaurus/useBaseUrl'

# Key Resources Overview

**Key resources** are the most important physical, human, intellectual, and financial assets your organization uses to make your business model successful and sustainable. They include:

<div className="admonition admonition-tip admonition-tip--pixelated alert alert--success">
	<div className="admonition-heading">
		<h5>Relevant Principles</h5>
	</div>
	<div className="admonition-content">
		<div className="relevant-principles">
			<Link
				to="https://digitalprinciples.org/principle/design-for-scale/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DesignForScale.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Design for Scale
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/build-for-sustainability/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Sustainability.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Build for Sustainability
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-collaborative/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Collaborative.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Collaborative
				</span>
			</Link>
		</div>
	</div>
</div>

- **Physical resources:** Equipment, raw materials, buildings, manuals, and procedures, e.g., any hardware you use is a physical resource
- **Human resources:** Key people and skills that are needed for the business model to work
- **Intellectual resources:** Intellectual property, codified systems and processes, and the intangible know-how of your team
- **Financial resources:** Cash and lines of credit

Resources can be tangible or intangible and are defined in the following way:

- **Tangible resources:** Physical resources that you can see, e.g., a training manual or a finance assistant
- **Intangible resources:** Nonphysical resources, e.g., customer knowledge, connections, and networks

Not all of your resources should be listed in your business model canvas. Only the main ones are important for this process. Your **key resources** can be described as those that:

1. Your business model would fail without
2. Are difficult to replace
3. Make you distinctive or provide you with a competitive advantage

For many business models, you may not have all of the key resources within your organization, so you may need to partner with other organizations and individuals to access them.

Questions that you may want to consider are: Is there a risk for the business model if the resource may not be available in the future? To what extent will the resource allow for growth? When and how do you decide whether a key resource should be held within your organization or can sit with a partner? Have you developed tangible or intangible assets as key resources through your [key activities](../key-activities) that you could use to innovate your business model?

To complete the key resources building block in your Business Model Sustainability Canvas, you will need to look at the most important inputs and assets your organization uses to make your business model work and determine whether any are accessed through a partner. In [section 7.1](./7.1-resource-sustainability), there is information on the importance of making effective resource decisions that will support long-term sustainability.

## Section 7.1: Resourcing Decisions

This section highlights the importance of evaluating your key resources through the lens of sustainability.

**Key discussion areas:**

- Understand how the insourcing or outsourcing of activities has an impact on the key resources you develop
- Highlight how key resources can become key capabilities that will potentially deliver new revenue, reduce costs, and create new impact for BUTI segments

## Key Takeaways

1. Key resources are the assets that you need to have in place (or have access to through a partner) for a sustainable business model.
2. When defining the necessary resources, concentrate on what’s necessary to perform key activities in your business model.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Identify your key resources and determine whether they are yours or a partner’s.

:::
