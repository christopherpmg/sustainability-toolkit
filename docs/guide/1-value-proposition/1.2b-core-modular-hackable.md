---
hide_table_of_contents: true
---

# Core, Modular, or Hackable

When seeking to create a sustainable and scalable solution, it’s critical to assess which parts of your innovation are:

- **Core:** The same everywhere for everyone
- **Modular:** You provide a different set of options for certain components of the solution that customers can choose from.
- **Hackable:** Customers are enabled to make their own changes to the solution

This is not only applicable to product solutions but also to process and service solutions.

### Exercise Steps

1. Using the following table, identify all the components in your solution. This should include all the key software components, hardware components (where applicable), and other services that are part of your solution.
2. Articulate whether this component is currently core, modular, or hackable.
3. Identify whether this component needs to be core, modular, or hackable in the future to support sustainability.
4. Describe what actions you will need to take to make the change.

import CoreModularHackable from '../../../src/components/tools/CoreModularHackable'

<CoreModularHackable />
