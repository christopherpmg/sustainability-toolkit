---
slug: "."
sidebar_label: "Overview"
pagination_label: "Customer Relationships Overview"
relevant_principles: ['design-with-user','understand-existing-ecosystem','design-for-scale','build-for-sustainability','be-collaborative']
related_topics: ['channels','customer-segments','revenue-streams']
---

import Link from '@docusaurus/Link'
import useBaseUrl from '@docusaurus/useBaseUrl'

# Customer Relationships Overview

The **customer relationship** outlines the type of interaction you have with your buyer, user, and target impact (BUTI) segments. Examples are:

- An impersonal relationship through a service level agreement or a website
- A collaborative relationship in a strategic partnership
- A mediated relationship through a third party, such as an implementing NGO

When thinking through the customer relationships you are likely to have, you will have to determine what type of relationship you need to build for each of your customer segments.

While in a traditional market these segments might be blended into a single consumer (e.g., an individual is the buyer, user, and target impact segments for a product), in the humanitarian and development sectors these segments can present differently and the relationship you have with each segment can be mediated through the other segments. For instance, if your digital solution is for community health workers, you may have a transactional relationship with the buyer (e.g., the ministry of health) through a procurement process, a personal relationship with users (e.g., the community health workers) through a workshop training session you conducted, and a mediated relationship with the target impact segment (e.g., pregnant women) through the users (e.g., the community health workers).

Moreover, BUTI segments can either be misaligned—when there is a clear difference between the buyer, user, and target impact segments—or aligned—when there is overlap between them. A good example of aligned BUTI segments is when a government director is the buyer of a platform and later uses it for prioritizing citizens' concerns.

<div className="admonition admonition-tip admonition-tip--pixelated alert alert--success">
	<div className="admonition-heading">
		<h5>Relevant Principles</h5>
	</div>
	<div className="admonition-content">
		<div className="relevant-principles">
			<Link
				to="https://digitalprinciples.org/principle/design-with-the-user/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DesignWithUser.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Design With the User
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/understand-the-existing-ecosystem/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_UnderstandExistingEcosystem.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Understand the Existing Ecosystem
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/design-for-scale/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DesignForScale.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Design for Scale
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/build-for-sustainability/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Sustainability.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Build for Sustainability
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-collaborative/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Collaborative.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Collaborative
				</span>
			</Link>
		</div>
	</div>
</div>

The types of relationships you have with each segment will have a significant impact on the sustainability of your business model. Insufficient knowledge and engagement with any segment is likely to be detrimental. Examples include:

- **Buyer segment:** If you lack a thorough understanding of the policy direction and budgeting method of the buyer and are not aligning with their procurement system, you may find yourself at a disadvantage when it comes to them purchasing or adopting your solution.
- **User segment:** If you do not keep up to date about the needs of your users and are not providing them with a good support system (i.e., training, technical support, etc.) based on their needs, you may find that your solution is not being used effectively.
- **Target impact segment:** If you lose sight of your target impact segment by only having a mediated relationship through the users, you will not truly understand their needs. You will also lack a nuanced understanding of the impact your digital solution is having.

In addition to understanding the wants and needs of each customer segment, you should familiarize yourself with the general characteristics of how relationships are built and managed in the aid sector and how this might differ from private and government sectors.

To complete the customer relationships building block in your Business Model Sustainability Canvas, you will need to identify your BUTI segments. Section 4.1 provides guidelines for identifying and carrying out relationships with each segment.

## Section 4.1: Building Relationships With Aid-Sector Customers

This section provides an overview of how to build and maintain relationships with different consumer segments that support the sustainability of the business model and the ongoing impact of the digital solution.

**Key discussion areas:**

- Explore key factors you will need to consider when developing your relationship approach with each segment
- Provide tips on the relationship’s potential impact on your business model

## Key Takeaways

1. There are three segments to build relationships with: buyer, user, and target impact.

2. Map out and understand the decision-makers and decision-making processes across and within BUTI segments.

3. Your preferred revenue model, the type of relationship you have with your customer, and the channel you use to access them should all work well together.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Indicate the type of relationship with each of the Buyer, User and Target Impact Segments on a separate sticky for each one.

:::
