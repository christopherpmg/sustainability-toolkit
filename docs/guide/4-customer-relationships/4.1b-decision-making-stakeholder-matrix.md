---
hide_table_of_contents: true
---

# Decision-Making Stakeholder Matrix

This tool will help you identify the decision-makers who influence the adoption, purchase, use, and support of your digital solution and where they sit in the decision-making process. It consists of four steps:

1. **Identify**: Think about who the decision-makers are and who will benefit from the decision.
2. **Understand**: Consider what is important to each of the stakeholders and what role each plays in the decision-making process.
3. **Map**: Consider the level of impact the decision has on each stakeholder and how much influence each may have over the decision.    
4. **Action plan for “yes”**: Determine what it will take to get stakeholders to say “yes” to your digital solution. Questions to consider for each of the columns in this step include:
   1. What are the potential objections and/or barriers stakeholders may have to the decision?
      - Does your product or service create any conflict of interest for them?
      - Have these stakeholders previously expressed resistance, and are they still prone to resist?
      - What could stakeholders do to stop the decision from taking place?
   2. What are the potential enablers of getting each stakeholder to say “yes”?
      - How could stakeholders benefit, and does your product or service align with their mission and/or vision?
      - Are there any existing connections that may influence their decision in a positive way?
      - Have these stakeholders previously expressed interest, and are they still likely to be interested?
   3. What engagement activities are needed to get each stakeholder to say “yes”?
      - Examples of activities include roundtable discussions, workshops, meetings, third party endorsements, demonstrations, free trials etc.

import StakeholderMatrix from '../../../src/components/tools/StakeholderMatrix'

<StakeholderMatrix />
