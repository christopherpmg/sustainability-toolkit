---
hide_table_of_contents: true
---

# BUTI Segmentation

This is the **Buyer, User, and Target Impact Segment Alignment Exercise**. The purpose of this exercise is to analyze the alignment or misalignment between your buyer, user, and target impact segments. It should be carried out in a workshop session with your team whenever possible.

1. Complete a [Value Proposition Canvas](https://ocw.tudelft.nl/wp-content/uploads/download_VPC.pdf) customer profile for each segment.
2. Identify the top 2-3 jobs to be done (JTBD) for each segment and write them in the table below.
3. For each JTBD, map out the level of desirability on a scale from +5 (Very Desirable) to -5 (Very Undesirable).
4. Identify areas of Strong Alignment, Sufficient Alignment, and Misalignment.

import ButiSegmentation from '../../../src/components/tools/ButiSegmentation'

<ButiSegmentation />

For JTBD that are misaligned across the segments, you will need to make a decision on the likely impact of the misalignment:

- Low misalignment: No action required.
- Medium misalignment: A need to change some aspects of your value proposition or some level of advocacy and influence for one or more of the segments.
- High misalignment: Potentially need to rethink your value proposition or reassess your ability to build a sustainable business model that covers all of these segments. This might mean a need to change the buyer or user segment.

