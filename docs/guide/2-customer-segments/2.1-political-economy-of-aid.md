---
---

# Political Economy of Aid

## Understanding the Aid Sector

While it is easy to conflate humanitarian and development aid, it is important to remember that they are two different sectors. The humanitarian sector is categorized as largely short-term responses to crises, such as natural disasters. Whereas the development aid consists of the long-term delivery of solutions to systemic societal problems.[^1] These problems relate to areas such as public health, agriculture, gender equality, and financial inclusion. 

:::tip Key Resources

To learn more about the aid sector, see:

- [Humanitarian Sector Overview](https://higuide.elrha.org/humanitarian-parameters/)
- [Development Aid Overview ](https://www.afd.fr/en/development-aid-whats-it-all-about)

:::

Primary actors in the humanitarian sector have traditionally included United Nations (UN) agencies such as the UN Refugee Agency (UNHCR), nongovernmental organizations, local government institutions, and donor agencies. The UN Office for the Coordination of Humanitarian Affairs (UNOCHA) is ultimately responsible for coordinating humanitarian emergencies and working with member states and local actors to address these crises. There has been a shift to localize humanitarian responses and allow member states to lead them.

Similar to humanitarian aid, development aid includes international NGOs, governmental departments, and social movement actors. Development aid is categorized as either bilateral or multilateral. Bilateral aid is transferred directly from the donor country to another government. Multilateral aid goes to international organizations to support their work.[^2] There has been a growing involvement of the private sector in both humanitarian and development initiatives. 

When developing your digital solution, it’s critical to understand how it fits into the political economy of the aid sector in order to ensure its financial sustainability. The political economies of humanitarian and development aid have a lot of similar characteristics: 

- Funding flows are often through grants or contracts.
- Aid can be political in nature.
- Aid funding is almost always allocated in two dimensions: geography and sectors. (Sectors might include health or education. It is unusual for digital to be treated as a sector in its own right.)
- There are often reporting requirements on the outcomes or impact of the funding. 
- Overhead costs are traditionally underinvested in, leading to a chronic underinvestment in digital transformation in the sector.

Despite their similarities, there are some differences between humanitarian and development aid. Humanitarian funding and decisions are in tighter cycles (usually three- to 12-month funding cycles), compared to development funding and decision processes, which are in longer, often three- to five-year cycles. This can have significant implications on how the two sectors behave in terms of their adoption of digital solutions.

## Funding Flows: Innovation Grants vs. Mainstream Grants

There has been a significant increase in innovation grants in humanitarian and development aid over the past 10 years, which has provided funding for many digital innovations. The increase in innovation funding and digital innovations across humanitarian and development aid has meant that there are many digital solutions that reach a certain level of maturity and adoption, having run one or more pilots, but then fail to progress to a sustainable solution.

There are many reasons for this, but one of the key reasons is that digital solution developers are unable to progress from innovation grants to more mainstream development and humanitarian grants and contracts. 

## Funding Flows: Grants and Contracts

In 2019, more than US $152.8 billion was provided in overseas development assistance (ODA) funding by the Organisation for Economic Co-operation and Development's Development Assistance Committee (OECD DAC) donors. The majority of this funding goes directly to governments and multilateral institutions and can be used to fund new projects that use digital solutions. Many digital solutions that are designed to be used to support the delivery of public services, such as health and education, will need to engage with governments as endorsers or customers if they want to have a sustainable business model to deliver impact at scale. 

A significant proportion of solutions will end up being programmed through grants or contracts. For organizations that are majority grant- and contract-funded (e.g., the UN, Red Cross/Crescent Movement, INGOs) this is where most of the money that will be used to pay for a digital solution comes from. 

## Funding: Key Distinctions and Implications

The key features of this funding and the implications for you are examined in the table below.

| Feature | Definition | Impact for Digital Solution Developer |
| ------- | ---------- | ------------------------------------- |
| Geographically tied | Funding is for a specific country and often for a particular part of the country. | Regional and/or global purchasing by a buyer can be hard to achieve, as expenditures will often be linked to a geographical location. |
| Sectorally tied | Funding is available for specific, well-defined sectors, e.g., health; education; water, sanitation and hygiene. | If your digital solution is specific to a sector (e.g., education technology) then this can be helpful. If your organization is cross-sectoral or sector agnostic, such as a last-mile supply chain digital solution, grant funding will be much harder to access. |
| ICR  | Indirect cost recovery | This is the percentage of funds in a grant that can be retained by an organization and ranges between 0%-20%. The average is probably 10%. This is the only flexible funding that grantees can use for overhead and is unlikely to be passed through to any suppliers, such as your digital solution. |
| VFM | Value for money | Many grants/contracts require a significant part of the decision-making to be based upon value for money or cost competitiveness. |
| Known problem | The problem your solution addresses is well-known and has evidence to prove it is a problem. | If members of the aid sector do not see it as a problem, then it will be almost impossible to sell them a solution. |
| Within scope problem | The problem your solution addresses is seen as a humanitarian or development problem. | If the problem is not already within scope of the aid funders, then it is unlikely that your digital solution will be used. For example, road traffic accidents are a significant cause of death for children globally, but are barely touched upon by the aid sector. In such a case, it might require advocating so the problem becomes within scope for humanitarian and development funders, but this will be a long process. |
| Funded problem | The problem your solution addresses is known, understood, and seen as within scope of the aid sector, but must also be adequately funded. | If the aid sector is not already putting funds into this problem, then it is unlikely to attract funding.  |
| Funding gaps | There are often significant gaps in funding that can create crippling cash flow issues. | There are three main funding gaps: <ol><li>Time lags between contract signature and payment</li><li>Gaps between consecutive grants or contracts</li><li>Elongated decision-making processes and timeframes by funders and purchasers</li></ol> |

## Key Takeaways

1. Understand how your digital solution fits into the political economy of the aid sector. 

2. The aid sector is primarily funded on geographical and sectoral lines. Ensure that you’re clear on where and what sector your digital solution is aimed at and that there is funding for the problem your solution addresses.

3. Innovation funding doesn’t last. You need a plan on how to access mainstream grants and contracts, either directly or through subcontracting.

4. The aid sector moves slowly when it comes to decision-making and funding for digital solutions. Be careful with your cash flow and expect delays.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Check that the revenue streams and buyer segment you have identified are sustainable based on your understanding of the political economy of aid.
	
:::

[^1]: Humanitarian Coalition. [From Humanitarian to Development Aid](https://www.humanitariancoalition.ca/from-humanitarian-to-development-aid)
[^2]: OECD. [DAC Glossary of Key Terms and Concepts](https://www.oecd.org/dac/dac-glossary.htm)
