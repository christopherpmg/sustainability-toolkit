---
slug: "."
sidebar_label: "Overview"
pagination_label: "Organizational Development Overview"
relevant_principles: ['understand-existing-ecosystem','design-for-scale','build-for-sustainability','be-collaborative']
related_topics: ['value-proposition','revenue-streams','key-activities','key-partnerships','cost-structure']
---

import Link from '@docusaurus/Link'
import useBaseUrl from '@docusaurus/useBaseUrl'

# Organizational Development Overview

**Organizational development** is defined as the efforts that focus on an organization’s capability to change and achieve greater effectiveness by developing and improving strategy, structure, and processes.

Creating a sustainable business model often requires organizational change and development.
If you are successful in creating a sustainable business model, you will often experience growth. But scaling the sustainable impact of your digital solution is not the same as sustainably scaling your organization. In fact, impact can be scaled through a really small organization and a large inner value network. For example, DataKind has small central organizations (chapters), but its IVN and product/service have a huge global impact.

However, the vast majority of organizations that have developed a successful digital solution will need to change, and there will likely be pressure to compromise the values that the organization was originally built on.

To complete the organizational development building block in your Business Model Sustainability Canvas, you will have to explore some key areas of change as you create a sustainable business model and develop your organization. The organization you have been until will probably need to change in order to become sustainable.

<div className="admonition admonition-tip admonition-tip--pixelated alert alert--success">
	<div className="admonition-heading">
		<h5>Relevant Principles</h5>
	</div>
	<div className="admonition-content">
		<div className="relevant-principles">
			<Link
				to="https://digitalprinciples.org/principle/understand-the-existing-ecosystem/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_UnderstandExistingEcosystem.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Understand the Existing Ecosystem
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/design-for-scale/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DesignForScale.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Design for Scale
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/build-for-sustainability/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Sustainability.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Build for Sustainability
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-collaborative/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Collaborative.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Collaborative
				</span>
			</Link>
		</div>
	</div>
</div>

## Section 10.1 Organizational Structure

There are many different types of organizations that develop digital products and services. Network- and partnership-based models are covered in the [key partnerships](../key-partnerships) building block. In this section, we will look at key aspects for independent organization structures:

- **Registration:** Should your sustainable business model be as a nonprofit, for-profit, or social enterprise (e.g., B Corporation) organization? What factors should you consider in making that decision?
- **Governance:** What form should your governance be: a formal board, an advisory board, or a form of secretariat?
- **Management and teams:** What are the key elements to be aware of as your organization adapts and grows?

We will also show some of the key pitfalls and how to navigate through them if you are an intrapreneurial team within an established organization (see [6.2 Key Activities](../key-activities/6.2-approaches-to-carry-out-your-key-activities)).

## Section 10.2 Organizational Culture

Even prior to establishing a formal organization, the founders will have started creating a culture and making decisions that cause both good and bad path dependencies. Path dependence is the scope of action available to an organization based on decisions made in the past. It is important to be intentional about your organization’s methods and remain flexible as your digital product/service and your internal and external circumstances evolve.

Understanding your culture is key to knowing what is helpful for growth and what might be holding you back. In this section we will:

- Explain what pirates, privateers, and petty officers are, and how having too many of the wrong roles will negatively impact your organization.
- Present an overview of what constitutes culture and how to assess changes you may need to make.
- Provide some simple tips on how to ensure you keep your entrepreneurial culture when there is pressure to formalize and bureaucratize systems and processes.

## 10.3 The Social and Environmental Impact of your Organizational Model

As you aim to become sustainable, you’ll have to make decisions that may not only compromise your culture and values, but also have a detrimental impact on society and the environment. This section will highlight some of these pitfalls, such as your approach to freelancers, your partnerships, whom you receive funding from, whom you partner with, the social and environmental impact of your solution and business model, and the hardware your solution is used on. It will provide you with approaches to ensure that your business model and organization have a positive social and environmental impact. Guidance is provided on:

- **Freelancers:** What should be your approach?
- **Accepting funding:** Who will you accept funding from?
- **Partnering:** What is your due diligence process?
- **Impact:** How will you measure your organization’s social and environmental impact?

## Key Takeaways

1. Organizational change and development are required to create a sustainable business model.

2. Scaling the sustainable impact of your digital solution is not the same as sustainably scaling your organization.

3. As your organization adapts and grows, it is important to consider your organizational structures, such as registration, governance, and management and team.

4. Understanding your organizational culture is key to knowing what is helpful for growth and what might hold you back. As you build your organization’s sustainability, you should strive to advance practices that promote positive social and environmental impact and to measure that impact.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Identify what type of registration is best for your sustainability.
- Determine what form of governance is right for your organization.
- Insert the short culture statement that you have created in the culture web exercise.

:::
