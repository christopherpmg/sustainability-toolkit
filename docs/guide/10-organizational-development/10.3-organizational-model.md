---
sidebar_label: "Organizational Model"
related_topics: ['value-proposition','revenue-streams','key-partnerships','cost-structure']
---

# Social and Environmental Impact of your Organizational Model

As you build your organization’s sustainability, decisions may need to be made that could compromise your culture and values and consequently have a detrimental impact on society and the environment.

You should be aware of some of these pitfalls—such as your approach to freelancers, whom you receive funding from, your due diligence process for your partnerships, and the social and environmental impact of your solution and business model—and strive to commit your organization to advancing practices that improve your social and environmental impact.

**While defined in many ways, corporate social responsibility** (also known as triple bottom line, circular economy, cradle to cradle) is the idea that businesses and organizations have a responsibility to address economic, social, and environmental issues in ways that benefit people, communities, and society. It is a broad concept that covers ethical business practices, workplace and employee issues, human rights, social causes, and environmental practices.[^1]

This section provides you with some approaches to ensure that your business model and organization can have a **positive social and environmental impact**. Guidance is provided on:

- **Freelancers:** What should be your approach?
- **Accepting funding:** Whom will you accept funding from?
- **Partnering:** What is your due diligence process?
- **Impact:** How can you measure your organization’s social and environmental impact?

## Freelance Workers

Freelance workers are used by organizations that want more flexibility (i.e., the ability to increase/decrease team size quickly and without significant expense). While the advantages of hiring freelancers are clear, this type of contracted work may bring risks if not managed in an ethical and fair manner. As an organization, you must do your due diligence and ensure that you are not procuring services resulting from modern-day slavery or working with organizations that are not providing a living wage. You should ask questions such as: Is the organization to which you are outsourcing activities to trustworthy and ethically sound? Is it paying its employees a livable wage? Is the organization fully committed to preventing modern-day slavery? The gig economy is often associated with work that offers no minimum wage and no unemployment insurance or benefits. To remain sustainable in the long term, your organization should be committed to ethical outsourcing practices.

## Funding

Funding is key to the continued success of your digital solution. Yet, funding can be generated from a source that is performing unethical work. You must ensure your funders abide by high ethical standards. You may want to associate solely with funding entities that are committed to values and principles that align with yours, and are making a positive social and economic impact.

:::info Case Study: WFP and Palantir

In February 2019, the U.N. World Food Programme (WFP) and Palantir, a data analytics firm, announced a new partnership aimed at helping WFP use its data to streamline the delivery of food and cash-based assistance. However, WFP’s choice of Palantir as a partner has received significant criticism from human rights, humanitarian, and technology-for-good organizations.

Because Palantir is a private-sector company that has previously provided services to the U.S. military, significant concerns were raised about data privacy and ownership of data in relation to humanitarian principles and standards. An open letter was sent to WFP expressing deep concern about the partnership, and various commentaries on the wider risks to the humanitarian sector have been published.

At the time of writing, this is an ongoing issue, and the actual impact on WFP’s humanitarian mandate and operations are unknown. But the concerns raised and the negative perception of WFP’s decision-making process are likely to have continuing repercussions.

*Taken from [https://higuide.elrha.org/toolkits/search/search-for-collaborators/find-the-right-partner-organisations/](https://higuide.elrha.org/toolkits/search/search-for-collaborators/find-the-right-partner-organisations/)*

:::

:::tip Key Resources

To evaluate your potential partnerships, use:

- [Partnership Assessment Tool](http://thepartneringinitiative.org/wp-content/uploads/2014/08/Partnering-Toolbook-en-20113.pdf#page=47), p. 41

For information on ways to enhance your social impact, see:

- Eradicating [Modern Slavery](https://www.ifc.org/wps/wcm/connect/5e5238a6-98b3-445e-a2d6-efe44260b7f8/GPN_Managing-Risks-Associated-with-Modern-Slavery.pdf?MOD=AJPERES&CVID=mR5Bx5h)
- Advancing Workplace and Employee Rights (e.g., [living wage](https://www.ethicaltrade.org/issues/living-wage-workers))
- Focusing on [Ethical Funding/Investing](https://corporatefinanceinstitute.com/resources/knowledge/trading-investing/ethical-investing/)
- Upholding [Anti-Corruption practices](https://www.oecd.org/corruption/Anti-CorruptionEthicsComplianceHandbook.pdf)

For information on ways to enhance your environmental impact, see:

- Eliminating Environmental Pollution (e.g., [greenhouse gasses](https://youmatter.world/en/definition/definitions-greenhouse-effect-what-is-it-definition-and-role-in-global-warming/))
- [Reducing Energy Consumption](https://hbr.org/2017/01/energy-strategy-for-the-c-suite)
- Implementing [Renewable Energy Options](https://www.nrdc.org/stories/renewable-energy-clean-facts)
- [Eco-Designing Products](https://youmatter.world/en/definition/definition-eco-design-examples-definition/) (or designing products that can be reused or repurposed before recycling)

:::

## Partnerships

Every organization relies on partnerships—whether implementing partners or a consortium of partners—that can add value in many forms, including financial and nonfinancial. (See [Key Partnerships](../key-partnerships).) To ensure that this expected value significantly outweighs the costs and risks, you should perform effective due diligence on your partnerships to identify any glaring red flags related to negative social and environmental issues, such as workplace abuses, employee health and safety, pollution, green-house gas emissions, and general waste.

## Your Social Impact

In addition to measuring social impact, your organization should also consider the many ways it can help solve societal problems and contribute to the development of society using the tools in the Key Resources section.

## Your Environmental Impact

Your organization should consider the many ways it can reduce its environmental impact using the tools in the Key Resources section.

## Measuring Your Organization’s Impact

Today, organizations are expected to have CSR strategies in place, but how do they measure the impact they are having on society and the environment? Even with all of the resources available to measure your organization's social and environmental impact, it is difficult to do it on your own, and doing so does not provide the credibility that external verification brings.

One of the best ways to build credibility and trust is to undergo an external verification process, and become a certified B Corp. The B impact assessment for B Corp status is free and will provide a good baseline from which you can plan how to improve your social and environmental impact, and how you can embed it in your legal structures.

## Key Takeaways

1. As you aim to build your organization’s sustainability, you should strive to commit to advancing practices that create positive social and environmental impact.

2. To simplify and gain independent endorsement, have your social and environmental impact verified by a third party, such as for a B Corp certification.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Outline how your organizational model and practice will create positive social and environmental impact.

:::

[^1]: [https://online.hbs.edu/blog/post/types-of-corporate-social-responsibility](https://online.hbs.edu/blog/post/types-of-corporate-social-responsibility)
