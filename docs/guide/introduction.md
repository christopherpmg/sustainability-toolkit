---
id: introduction
title: Introduction
pagination_label: Introduction to the Guide
slug: /guide
---

# Introduction to the Business Model Sustainability Guide

The [Principles for Digital Development](https://digitalprinciples.org/) are nine living guidelines designed to help integrate best practices into technology-enabled programs. They include guidance for every phase of the project lifecycle and are part of an ongoing effort among development practitioners to share knowledge and support continuous learning. The creation of the Digital Principles was a community-driven effort, and they are a synthesis of lessons learned through the use of information and communication technologies (ICTs) in development projects.

Digital solutions—whether products or services—are essential to transforming the aid sector.[^1] As humanitarian and development interventions become more complex and costly, a business-as-usual approach is no longer feasible. Building effective digital solutions in a more sustainable way is necessary to ensure cost-effectiveness, scale, and financial sustainability, which in turn supports their positive, long-term impact.

However, it can be difficult for digital development practitioners to apply the Principles for Digital Development to their work and investments. In particular, the principle of [Build for Sustainability](https://digitalprinciples.org/principle/build-for-sustainability/) is often an elusive concept that may look different across initiatives.

[^1]: Defined as the ecosystem of development and humanitarian organizations, donors, researchers, and host governments in the Global South.

## What Is the Business Model Sustainability Toolkit?

The Business Model Sustainability (BMS) Toolkit, funded by Fondation Botnar in partnership with the Digital Impact Alliance (DIAL), helps social enterprises, NGOs, and small businesses think about their own sustainability and the sustainability of their digital solutions. It consists of a Business Model Sustainability Canvas and a Business Model Sustainability Guide, which includes guidance, case studies, and interactive tools for users.

Focusing on the principle of Build for Sustainability—a key consideration to ensure the long-term success of a digital solution—the BMS Toolkit will help digital development practitioners build their confidence and capacity to apply it to their work. When developing a sustainable digital solution, it is important to understand and embody the foundational tenets of Build for Sustainability, which include:

- Maintaining user and stakeholder support, as well as maximizing long-term impact
- Ensuring user and stakeholder contributions are not minimized due to interruptions, such as a loss of funding
- Creating solutions that are more likely to be embedded into policies, daily practices, and user workflow

## Who Is the Toolkit for?

- **Digital product/service owners/managers** - the entrepreneurs and intrapreneurs (an entrepreneur within a larger existing organization) who have driven the development of a digital product or service and are grappling with how to build a sustainable business model.
- **Advisors to these digital product/service owners/managers**, including mentors, consultants, and accelerator and lab managers.

## What Does the Toolkit Aim to Achieve?

The goal of the Business Model Sustainability Toolkit is to enable intrapreneurs and entrepreneurs in any corner of the world who have developed digital solutions to be used for development or humanitarian purposes to create a business model to sustain the impact of their solution.

An organization utilizing the BMS Toolkit will better understand how it can generate and deliver value in a sustainable way, and in turn realize its economic, social, and/or environmental impact goals. It will be able to develop business models that will enable the sustainable and scalable use of digital programs, tools, or products to significantly impact development and humanitarian outcomes.

## What Is Included in the Toolkit?

The **Business Model Sustainability Toolkit** consists of a Business Model Sustainability Canvas and a Business Model Sustainability Guide.

The **Business Model Sustainability Canvas** is a visual framework for how an organization works. It illustrates what the organization does, for whom and with whom, the resources it needs to do that, and how funds flow in and out of the organization. The BMS Canvas is made up of nine building blocks taken from the [Business Model Canvas](https://www.strategyzer.com/canvas), which were identified by Alex Osterwalder and Yves Pigneur of Strategyzer, and two additional building blocks on organizational development and end game that are derived from our experience in developing sustainable business models.

- The 11 key building blocks include value proposition/solution, customer segments, channels/sales, customer relations, revenue streams, key activities, key resources, partners, cost structure, organizational development, and end game.

The **Business Model Sustainability Guide** aims to provide organizations developing digital solutions with practical and actionable guidance on each of the building blocks. Each building block section in the guide includes most of the following:

- An overview of the building block and instructional guidance
- Illustrative case studies of real organizations
- Interactive tools that capture inputted data
  - Note: External tools are also linked, but data will not be automatically captured.
- Additional key resources for more information
- Links to relevant Principles for Digital Development

