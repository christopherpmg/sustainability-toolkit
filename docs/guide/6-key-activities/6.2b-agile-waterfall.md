---
hide_table_of_contents: true
---

# Agile Gap Analysis Tool

To anticipate whether issues and friction are likely with key partners in your inner value network that you are collaborating with, it is useful to identify where you are and where [partners in your IVN](../key-partnerships/8.1b-value-network) are in terms of being agile or traditional in how projects are approached.

1. **Identify how agile you are.** Identify how agile you are using the statement below. For each statement, indicate your level of agility from 1 to 5.
2. **Assess the agility of your collaboration partner organizations.** Once you have established the level of agility of your own organization, the next step is to assess the agility level of your partners. You can do this with them in a joint workshop environment or ask them to do it themselves. If they are unable to do either of these things, you may have to make your best assessment of where they are positioned using the same set of statements.
3. **Assess the gap using this framework:**
   - If the gap between you and your partner is greater than 15 points, then you should assess the feasibility and cost benefits of collaborating on this project.
   - If the gap is greater than 5 points, but smaller than 15 points, then you should try some methods for managing/mitigating the gap.
   - If the gap is 5 points or less, you are well aligned and no further additional action is needed.

import AgileWaterfall from '../../../src/components/tools/AgileWaterfall'

<AgileWaterfall />

### Assess the feasibility and cost benefits of collaborating on this project

If the gap between you and your partner is greater than 10 points, then there are likely to be frictions in your partnership. These frictions could lead to significant delays, cost increases, stress on both sides, and potentially the breakdown of the project all together. 

Therefore, you should consider as a team whether you believe the project will truly be worth it. Remember that days spent trying to address issues arising from the gap could be used to implement a project with better aligned partners.

### Methods for managing/mitigating the gap

Tactics to help mitigate the misalignment in levels of agility include:

- **Anticipate**: When engaging with a humanitarian or development organization, carry out some research about them and check for indicators of traditional approaches. For example, if an organization is primarily funded by institutional donors, it is likely to have built its institutional processes around traditional waterfall/blueprint methodologies such as logical frameworks. Use the interactive tool above to identify areas of potential conflict. 
- **Check language and assumptions**: Don’t always assume that you are dealing with individuals and teams that are using traditional methods. For instance, emergency rapid response teams often work in highly flexible and iterative ways that are agile, but they may use different terminology for it.
- **Have the conversation**: Don’t skirt around the issues. Address them constructively and in an upfront way. 
- **Find a bridging person**: If you do not have someone who has experience in both worlds (i.e., the tech and aid sectors) then try to bring an independent broker/facilitator to early discussions who can assist with #2 and #3 above.
- **Establish non-negotiables**: What are your non-negotiables? What are you unwilling to do or change from your current ways of working? Be clear on these internally and stick to them. It is better to walk away from a potential customer or collaborator than to waste significant time trying to resolve systemic issues. Be very clear of the opportunity cost in doing so.
