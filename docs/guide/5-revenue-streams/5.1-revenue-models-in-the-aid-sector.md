---
related_topics: ['customer-segments','customer-relationships','value-proposition']
---

import Link from '@docusaurus/Link'
import RevenueModelCardDeck from '../../../src/components/tools/RevenueModelExplorer/RevenueModelCardDeck'
import RevenueModelExplorerModal from '../../../src/components/tools/RevenueModelExplorer/RevenueModelExplorerModal'
import RevenueModelExplorerUserDeck from '../../../src/components/tools/RevenueModelExplorer/RevenueModelExplorerUserDeck'

# Revenue Models in the Aid Sector

Revenue models are one of the key building blocks in the Business Model Sustainability Canvas, and for many people it is the most significant area of focus. In this building block, you will need to not only capture the expected revenue in terms of a financial amount, but also what the revenue models are that you are looking to develop.

You may need to blend or combine a number of revenue models to create a resilient and sustainable business model. Key revenue models to explore are discussed below.

1. **First**, review the cards that represent each of the [Revenue Streams](#revenue-streams) and decide which are possible for your business model.
2. **Next**, use the [Impact Matrix Tool](#impact-matrix-tool) to prioritize your possible revenue streams.

## Revenue Streams

### Project-Based Revenue Model

:::tip Key Resources

Two useful approaches to help with accurate forecasting of costs to ensure you are able to break even on a project are:

- [Reference class forecasting](https://www.researchgate.net/publication/263747196_From_Nobel_Prize_to_Project_Management_Getting_Risks_Right), which is the practice of comparing your proposed project with other similar projects
- [Feasibility blueprinting](https://www.servicedesigntoolkit.org/assets2013/posters/EN/F8-blueprint-A0.pdf), which is helpful when using a service delivery lens in your planning 

:::

With a project-based revenue model, a sale is tied to the delivery of a time-bound individual project or program. Many sales and partnerships with aid organizations come through project-based financing. The key mechanisms for this are through contracting or subcontracting as part of the buyer’s project budget, which in most cases is funded by a back donor. However, there are opportunities for directly receiving funds from donors, in particular from innovation funds.

**Tips:**

1. Ensure that you have a revenue model mix and are not reliant on just grants.
2. Closely monitor and proactively manage your grant/contract pipeline to ensure that you don’t have a number of grants ending at the same time, leading to a funding cliff.
3. Push for a deliverables contract instead of a time and materials contract as much as possible and factor in sufficient working capital into these contracts for ongoing development.

<RevenueModelCardDeck revenueModelCategoryId="project" />

### Product- or Service-Based Revenue Models

The product- or service-based revenue model can sometimes be used in projects but is more closely related to enterprise-level delivery across multiple projects and service areas in an aid organization or government department. There are a number of revenue streams in this model.

<RevenueModelCardDeck revenueModelCategoryId="product" />

### Indirect Revenue Models

Cross subsidization models are based on the practice of one set of customers paying one price for a product that subsidizes the price of the same or similar product for another set of customers. This is particularly attractive in markets and contexts where the purchasing power of your buyer, user, and/or target impact segment is severely constrained. There are different variants of this model.

<RevenueModelCardDeck revenueModelCategoryId="indirect" />

### Fundraising Models

For digital development solutions, there is also the possibility of fundraising. This model seeks to generate financial support from members of the public, philanthropists, and your network. The models below are for projectized funding, where those providing the funds are able to specify a tangible end product or service that the funds should go towards.

<RevenueModelCardDeck revenueModelCategoryId="fundraising" />

### Investment Streams

Strictly speaking, investments should not be seen as revenue. However, the original Business Model Canvas does not make a distinction between the two. Investments can be a mixture of designated funds for projects and undesignated funds for core operations and growth. Investments can be sourced from philanthropists, innovation funders, and networks. Undesignated funds that can be used to build your business model are the most precious funds available to intrapreneur and entrepreneur teams to establish their business model and potentially scale it. 

However, such investments sometimes expect a financial and social impact return. They are also not a recurring revenue stream, so should not be relied upon as a sustainable revenue stream. Rather, they should be seen as “rocket fuel” that can get your business model into a sustainable orbit.

<RevenueModelCardDeck revenueModelCategoryId="investment" />

### Cost Reduction Models (CRMs)

There are numerous models of services and gifts in kind, which some organizations account for as revenue. However, most of these models help reduce costs rather than increase revenue. Therefore, we cover them in the [Cost Structure](../cost-structure) building block.

<RevenueModelExplorerModal />
<RevenueModelExplorerUserDeck />

<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>

## Impact Matrix Tool

Use the matrix below to prioritize which revenue streams to test. The matrix will highlight the strategies with the most potential to help your digital solution find a viable path to a sustainable revenue model and maintain the social impact it was designed for.

The horizontal line in the matrix represents your streams’ revenue potential—from negative revenue potential (i.e., it would cost more to put in place than you would earn from it) on the left to high revenue potential on the right. The vertical line indicates the potential effect of the revenue model on your social impact—from reducing your social impact at the bottom to increasing your social impact at the top. The upper right-hand quadrant is your **zone of interest**.

Map each revenue stream on the matrix. Give considerable thought to each and discuss them with your team. You should keep in mind the following as you complete this step: 

- Where is the best placement on the matrix of each revenue stream for your digital solution’s unique circumstances? 
- Which revenue streams offer the highest revenue potential while retaining or improving your intended social impact?
- How does each fit with your mandate, your key partners, and your current obligations?
- Is there value in pursuing it? Keep in mind the purpose of your solution, the needs of the end users, your in-house capacity, and your market positioning.

For those revenue streams falling within the upper right-hand quadrant of the matrix (the zone of interest), there are three additional questions to consider: 

1. What is the cost of developing the revenue stream? Is it affordable? 
2. How much time will it take to develop the revenue stream? Is it feasible? 
3. Do you have the capability in house to develop the revenue stream? 

import RevenueModelAssessor from '../../../src/components/tools/RevenueModelAssessor'

<RevenueModelAssessor />

<p className="inline-credit">
	This tool is modified from the Digital Impact Alliance’s
	{' '}
	<Link href="https://digitalimpactalliance.org/wp-content/uploads/2021/01/Sustainability-Guidbook.pdf">
		Sustainability Guidebook
	</Link>.
</p>

## Key Takeaways

1. Try to diversify your revenue models to create a resilient and sustainable business model.

2. Each revenue model has a number of potential revenue streams that you should consider when applicable to your digital solution and organization.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Indicate what revenue model and revenue streams you will be using.
- Indicate the value of each revenue stream, either per sale, per month, or per year.

:::
