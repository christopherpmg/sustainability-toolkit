---
related_topics: ['channels','value-proposition']
---

import Link from '@docusaurus/Link'

# Outer Value Network

## Outer Value Network Actors

The outer value network is made up of people, businesses, government departments, and civil society organizations that have an influence over your business model, service, or product but do not have a direct working relationship with you. Examples include complementors, competitors, regulators, and funders.

While some of these actors can enhance your value for customers, such as complementors, or sustain the development of your digital product or service, such as funders, others can reduce your potential value or even stop your business model from operating entirely, such as regulators and competitors.

## Complementors

Complementors are individuals, organizations, products, and services that complement your digital solution and provide benefit to your customers (or your buyer, user or target impact segment). This could be:

- Open source software, such as [ODK](https://getodk.org/), that you build your product or service on
- Other software that your product has APIs for
- Enabling factors that you will need, such as mobile phone penetration or internet access; products and services that your solution relies on being in place

Complementors can be critical for your business model, but they do not need to be part of your inner value network. Often they can remain in the outer value network because you do not have a direct working relationship with them (e.g., ISPs). However, you do need to identify which complementors you rely on and ensure that you monitor any changes or developments concerning them. This can be done through subscribing to newsletters, following them on Twitter, or regularly checking their websites.

## Regulators

Regulators are those who set the rules, policies, and laws that will affect how your business model and product or service perform. The governments of the countries where your solution is used are the ultimate regulators. They will decide on things such as data protection laws and the registration and licenses required to operate in that country.

:::tip Key Resources

For more information, see:
- Digital Regulation Handbook, Chapter 7 on [“Regulatory responses to evolving technologies"](https://www.itu.int/en/ITU-D/Regulatory-Market/Pages/DigiReg20.aspx)

:::

You will need to adhere to regulations in order to ensure sustainability.  You might be required to register your organization or business in a country in order to operate. If you are entering a country due to a humanitarian crisis, you may find that regulations are suspended or thresholds are lowered to facilitate the humanitarian response. However, after a year or two these regulations are often reimposed. This can catch organizations and businesses by surprise when they discover they can no longer legally operate in a  country because they are not registered or don’t have a license to operate. Be sure to evaluate early on whether regulations are likely to change or not.

Most sectors, including health, education, and finance, will have regulatory requirements of some sort. There will also be regulations regarding newer technologies, such as distributed ledger systems for cryptocurrencies or digital identification programs. If there are regulations that negatively affect your business model, product or service, you’ll have to consider whether you can change your product, service or business model in response; if you need to advocate for changes in the law or policies; or if you should change your strategy for that country.

If you receive funding from institutional donors, such as the U.S. government or the E.U., you will also be subject to various regulations, particularly regarding procurement processes and eligibility criteria. These processes can create significant barriers to entry for smaller start-up organizations, so funding opportunities from such entities need to be assessed carefully to ensure it’s worth the effort and you’ll be able to fulfill the requirements.

Regulatory intelligence and compliance tools like Visualping.io will help you track regulatory information automatically and provide alerts on any changes that occur. Such tools allow you to receive alerts about specific pages such as state, federal, and international regulatory authorities.

:::info Case Study: Regulation

1. **GDPR regulations:** When the [General Data Protection Regulation (GDPR)](https://gdpr-info.eu/) came into force in the E.U. on May 25, 2019, it had far-reaching implications for many digital actors. GDPR, as well as data protection regulations in other countries (e.g., Canada, Kenya, Nigeria) require digital actors to change their operating model depending on the context in which they work. Storing certain types of data requires steps not previously required (i.e., informed consent) and violations of these regulations come with hefty fines and additional liabilities. These liabilities need to be accounted for in the financial management of a business, not only in its operations.
2. **Regulatory loopholes:** [M-Pesa](https://www.safaricom.co.ke/personal/m-pesa/m-pesa-home), the Kenyan mobile money transfer service, was originally developed by Vodafone in the United Kingdom in 2005. However, U.K. banking and financial regulations are extremely restrictive, so M-Pesa could not have succeeded there. Therefore, Vodafone contacted Safaricom in Kenya, where banking and financial regulations are less restrictive, and requested doing a pilot. This enabled [M-Pesa to be launched and flourish](https://www.techchange.org/work/the-story-of-m-pesa-2/) in Kenya.

:::

## Funders

You are likely to seek funders in order to obtain financial support for your digital product or solution. There are three primary types of funders:

- Government: Government agencies at the municipal, provincial, and national levels
- Foundations: Community, family, and private foundations
- Companies: Small businesses and large corporations

Before approaching a funder, you should conduct research about their areas of interest, mission, and vision. The better you understand their objectives, the better you can tailor your proposal and get them to be a key partner within your OVN. The most effective way to reach out to a funder is to get an introduction from someone in your network. For information about projectized funding, where funders can require that their money go to a particular end product or service, see [Revenue Models](../revenue-streams/5.1-revenue-models-in-the-aid-sector).

## Competitors

It is critical to keep an eye on what your competitors are doing in terms of their product, service, and business model development. Keeping track of their new features, services, or revenue model approaches will inform your own strategy. Based on the Digital Principle of  [Reuse and Improve](https://digitalprinciples.org/principle/reuse-and-improve/), changes by a competitor might actually create an opportunity for collaboration that reduces duplication and increases social impact.

You can’t analyze your competitors if you don’t know who they are. The key to identifying possible competitors is to put yourself in the shoes of your target customer. Using focus groups, questionnaires, and surveys can help you gather information on the solutions and products that are available, including their strengths and weaknesses. There are three types of competitors:

1. Primary or direct competitors target the same customer segments as you, have a similar product offering, or both. You should regularly track these businesses and have an in-depth understanding of their operations and offerings.
2. Secondary competitors may offer a variation of your product, either at the high or low end of the spectrum. Or they may have a similar target market but an entirely different product.
3. Tertiary competitors have products that are tangentially related to yours but may not be your direct competition just yet.

In order to monitor your competition effectively, it is good to develop a clear features table for your product or service and be able to assess the competition against it. It is also worth studying your competitors’ business model using a business model canvas to reverse engineer how their business model works (there may even be things that they are doing with their business model that are opportunities for you).

:::caution Interactive Tool: Component Comparison Tool

- Methodology: **Group or individual exercise**
- Time estimate: **3+ hours**

Use your completed [Core, Modular, Hackable Tool](../value-proposition/1.2b-core-modular-hackable) to get a list of the components or key features of your digital product or service and use this to fill in column 1 of the Component Comparison Tool. Create a new column for each competitor and see whether their product and/or services have the same features as you and if they are inferior or superior to yours.

<Link
	to="./8.2b-component-comparison"
	className="button button--warning"
>
	Go to the tool
</Link>

:::

## Key Takeaways

1. Actors such as organizations, individuals, and government departments are part of your outer value network. You will not have an ongoing working relationship with them, but they and their actions can have a significant impact on enhancing or blocking the value you are seeking to create.

2. Tracking what these actors are doing is an ongoing exercise that should be carried out regularly.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Identify any important actors in your outer value network,  such as complementors, regulators, and competitors, that your business model relies on to enhance your offering and mark them as being part of your OVN.

:::
