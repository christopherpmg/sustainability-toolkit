---
related_topics: ['value-proposition','key-resources','key-activities','key-partnerships']
---

# End Game Options

As your digital solution matures, a key consideration is how you will determine what your end game should be. You need to think about both the social problem you are trying to solve and the operational model you are using when deciding which end game option to pursue. This section outlines the most common end game options:

1. Give away
2. Replication
3. Government adoption
4. Local organization adoption
5. Merger or acquisition
6. Mission achievement
7. Sustained service by your organization
8. Winding down

*(Adapted from [Guglev & Stern, SSIR](https://web.archive.org/web/20210316210126/https://www.philanthropy.org.au/images/site/misc/Tools__Resources/Publications/2015/Winter_2015_Whats_Your_Endgame.pdf))*

These eight options aim to sustain and expand an existing digital product or service. When thinking about which of these models fits your organization and the digital solution you are designing, you should also consider what key resources, activities, and partnerships need to be in place to get you to your desired end game.

## Give Away

Digital public goods are often thought of as being open source. While this is clearly not always the case, every digital solution that is developed for a social purpose should seriously consider this option. FOSS (free and open source software) is a strong model for ensuring that a digital solution is used by as many organizations and people as possible, creating the potential for social impact at scale.

The main approach to ensuring the ongoing success of FOSS is to hand it over to a well-managed developer community to provide updates and support to users. This [primer](http://static1.squarespace.com/static/590e91662994cac296ccd1e8/5945adb6c0302673fa31d085/5945add3c0302673fa31d413/1497738707724/Primer-on-Open-Source-and-the-Creative-Commons-for-Aid-and-Development-Code-Innovation.pdf?format=original) provides a good overview of open sourcing, including advice on creative commons licensing.

**Potential future role for your organization:** There is no potential future role for your organization.

:::info Case Study: Speed Evidence

The Speed Evidence project was an initiative originating at World Vision that sought to improve the ability of aid agencies to deliver more effective, contextualized responses and for disaster-affected communities to have access to more contextualized information. It was a project about building an information management platform as well as creating a behavioral shift in organizations about how they make decisions.

After delivering a working prototype, the initiative was shut down due to lack of organizational buy-in. Before closing, the full code and platform was made available on github for free for anyone to use. Read more about the project [here](https://speedevidence.wordpress.com) and the key learnings [here](https://www.wvi.org/sites/default/files/Speed%20Evidence.pdf).

:::

## Replication

Replication serves as an end game to expand the use of a product or service without having to expand the organization that created it. To pursue this model, the nonprofit needs to demonstrate the efficacy of its approach and then find other organizations that can deliver its product or model in different contexts. In many cases, other organizations are able to implement that approach more effectively because they have a stronger existing infrastructure or they have greater trust from or understanding of the community they are applying the product or service to.

**Potential future role for your organization:** Your organization can either serve as a certification body that maintains quality standards or as a center of excellence that demonstrates best practices to potential replicators and provides training.

:::info Case Study: RepRap and WeRobotics/Flying Labs Network

RepRap is a cheap open source 3D printer (under a GNU public license) that can print its own components. It was intentionally designed to be completely replicable by being able to print the next version of itself, as with [Von Neumann’s universal constructor](https://en.wikipedia.org/wiki/Von_Neumann_universal_constructor). It was designed to be produced and used in low-resource environments, with the intention of bringing 3D printing to the masses. When RepRap was started, a 3D printer cost close to $10,000. Today, a RepRap printer can be created for $300 and is the basis for a large proportion of the desktop 3D printers now in use across the world.

*Source: [All3DP](https://all3dp.com/history-of-the-reprap-project/)*

[WeRobotics](https://werobotics.org/) created the [Flying Labs Network](https://werobotics.org/flying-labs/) to provide, promote, and enable local expertise and leadership to accelerate the positive impact of humanitarian, health, development, and environmental solutions. Flying Labs strengthens local expertise in drones, robotics, data, and AI through the professional training it organizes for local partners, including government, industry, international organizations, nonprofit organizations, research centers, and universities. It also implements a wide array of applications and research/pilot projects across key social good sectors with local and international partners. The endgame for WeRobotics when it created Flying Labs Network was always replication—the “demonstration of efficacy of a replicable operation and impact model.”

[Read more about the Flying Labs Network here](https://blog.werobotics.org/2021/06/28/do-social-good-organizations-have-an-endgame/).

:::

## Government Adoption

In this model, your digital solution can reach a significant scale when adopted by a national government or achieve a targeted impact when adopted by a local or municipal government. This is common when the scale of the problem that your digital solution is solving is so large that government involvement becomes indispensable or when the state is the provider of this public good in that country, e.g., health or primary education.

**Potential future role for your organization:** Once government adoption occurs, your organization may continue to serve as an advisor to or service provider for government agencies. For example, if you are a social enterprise that has built a digital services platform and your program is being adopted by a government, your lead developers may need to continue providing Level 3 technical support to users, even as you train government staff to provide Level 1 and Level 2 technical support. Ensure that the new owner of the program has budgeted for your staff time during the transition period.

:::info Case Study: BBC Media Action

[BBC Media Action](https://www.bbc.com/mediaaction/) successfully scaled and transitioned one of the largest mobile health programs in the world to the Indian government. The complementary suite of services was designed by BBC Media Action to strengthen families’ reproductive, maternal, neonatal, and child health behaviors. Mobile Academy was a training course to refresh frontline health workers’ (FLHWs’) knowledge and improve their interpersonal communication skills. Mobile Kunji was a job aid to support FLHWs’ interactions with families. Kilkari delivered weekly audio information to families’ phones to reinforce FLHWs’ counseling.

In April 2019, when Mobile Academy and Kilkari were transitioned to the government, 206,000 FLHWs had graduated and Kilkari had reached 10 million subscribers. Lessons learned include: (1) private-sector business models are challenging in low-resource settings; (2) you may pilot apples but scale oranges; (3) trade-offs are required between ideal solution design and affordability; (4) program components should be reassessed before scaling; (5) operational viability at scale is a prerequisite for sustainability; (6) consider the true cost of open source software; (7) taking informed consent in low-resource settings is challenging; (8) big data offers promise, but social norms and SIM change constrain use; (9) successful government engagements require significant capacity; (10) define governance structures and roadmaps up front.

[Read more about the lessons learned here.](https://gh.bmj.com/content/6/Suppl_5/e005341)

:::

## Local Organization Adoption

Under this model, a ​​local organization takes ownership of the digital product. More commonly, it is adopted by an organization that works closely with the user segment of your digital solution or by the user segment itself. Transitioning your product or service to a local organization is a long-term process that requires a clear understanding of the end-to-end service requirements that the local organization will need to manage. The local organization may not be able to adopt the same business model as you have used, so you should work through this guide with them to design an appropriate business model that can absorb the transition.

**Potential future role for your organization:** As with the government adoption option, this model allows for the possibility of continuing to provide technical support, updates, or new versions.

## Mergers or Acquisitions

A merger is when two or more organizations combine, and an acquisition is when one organization takes over another one. Although more common in the private sector, mergers and acquisitions do occur in the humanitarian and development sectors. Positive reasons for a merger or acquisition can be to enable scaling to new countries, combine support services, or provide complementary services such as the digitization of a social service.

However, many mergers and acquisitions take place when the organization being acquired is in some form of financial distress. If it looks like your organization may be coming under financial pressure in the near future, you should assess whether a merger or acquisition should be considered. You should not wait until your organization is in financial distress, as the likelihood of success for the merger will decrease as the level of financial stress your organization is in increases.

In the private sector, mergers and acquisitions often result in the owners of the business receiving a pay out. This doesn’t usually happen in the nonprofit sector, where the payoff for the organization that is being acquired is to see a greater chance for its organizational objectives to be met.

**Potential future role for your organization:** There are many types of mergers. In some mergers, your organization will remain almost intact but will be part of a larger group with more resources to support you. In others, however, your organization will be absorbed into the body of the other organization and cease to exist. Whatever type of merger or acquisition you go through, you should ensure that it continues to deliver a quality service or product to your customers and that your staff’s welfare is looked after.

:::info Case Study: IMC Worldwide

In 2018, [IMC Worldwide](https://www.imcworldwide.com/) acquired the International Solutions Group (ISG) to expand its presence in the United States and its monitoring and evaluation services. ISG had developed skills and capabilities IMC did not have and was seeking. These skills were deemed to be critical to its strategy and growth plans. For ISG, the acquisition became its end game.  [Read more here](https://www.imcworldwide.com/news/we-are-pleased-to-welcome-international-solutions-group-to-imc-worldwide/).

:::

## Mission Achievement

An organization that uses a mission achievement model has a well-defined and plausibly achievable goal related to solving a discrete problem and maintaining the focus on a targeted impact. Once the challenge is addressed, the organization winds down permanently or moves on to a new achievable goal only if it has an especially valuable asset or capability that can be deployed for another social purpose.

## Sustained Service by Your Organization

This model makes sense when an organization can attend to an enduring social need that a local organization or government cannot or will not satisfy. When thinking about extending the lifecycle of your digital solution under the sustained service model, you should take into account how maintenance, updates, and technical support will be sustainable through the extended life of the product. It’s wise to review your approach regularly to ensure not only environmental and financial sustainability, but also to ensure users and stakeholders continue to gain value from your digital solution.

:::info Case Study: Dimagi

[Dimagi](https://www.dimagi.com/) was founded in 2002 out of the MIT Media Lab and the Harvard-MIT Health Sciences and Technology programs. Dimagi develops open source software for low-resource settings and built some of the first mobile solutions for frontline health programs in developing countries.

Today, Dimagi continues to operate on the foundation that mobile solutions are key to improving the quality and efficiency of global development programs. While it started in health care, it now works across sectors with organizations of all sizes and has supported more than 600 projects.

Dimagi is a recognized social enterprise B Corp, or a certified Benefit Corporation, reflecting its commitment to making an impact.

:::

## Winding Down

Sometimes it doesn’t matter how good your product or service is or how pressing the social need you are trying to improve is—you are simply unable to make your business model sustainable. In such cases, you will need to wind your organization down. In doing this, you can still achieve some sustainable impact by reducing the negative impact on stakeholders and ensuring key assets and learnings are shared for others to use and learn from. Four ways of doing this are:

1. Don’t wait until it is too late to wind down. Leave enough time to pay off your liabilities and provide a good handover of any knowledge, resources, or assets to other organizations and communities you are working with.
2. Even though you are going through a tough time personally, ensure that you are empathetic to the worries, concerns, and fears of your staff, volunteers, and partners.
3. Even if your software was proprietary before, convert it into FOSS using a creative commons license and put the resources on Github or another online repository. Ensure that you are able to do this without contravening any prior agreements with funders or investors.
4. Share your lessons learned, which can prove invaluable for others. Two great examples of honest reflections are from Laura Walker McDonald, a former colleague at DIAL, who unpacks lessons from her time as CEO of SIMLab covering both [strategy](http://simlab.org/blog/2017/12/13/what-happened-part-i/) and [operations](http://simlab.org/blog/2017/12/19/what-happened-part-ii/), and from Paul Currion and Ben Joakim, co-founders of Disberse, who have left their [website](https://www.disberse.com/) as documentary evidence of their learning from trying to set up a blockchain service for aid financing. Their case studies are below.

:::info Case Study: SIMLab

Social Impact Lab (SIMLab) began life as a nonprofit with a mission to lower barriers to social change through mobile. It was the original institutional home of the FrontlineSMS project, a suite of software that helps organizations build services with text messages. In 2014, the team spun off FrontlineSMS as a for-profit to enable the expansion of its practice to cover work beyond implementing SMS and the Frontline too.

SIMLab ran into trouble because while its mission was attractive and working, its fundraising model was not. It had a confused message, no market focus, no real business model, and bad luck, which resulted in it shutting down. SIMLab sought to use its experience to shed light on the many elements of a technology project that you have to get right to make it successful, such as evidence-based design, learning from your work, responsible data, sustainability, and effective messaging.

The [SIMLab website](http://simlab.org/) has been left up, including two reflections about the decision to close. They can be found [here](http://simlab.org/blog/2017/12/13/what-happened-part-i/) and [here](http://simlab.org/blog/2017/12/19/what-happened-part-ii/).

:::

:::info Case Study: Disberse

Disberse set out to transform the way money flows for social impact. It did so for four years, building a new type of financial institution addressing key problems for the aid industry. However, the combination of the COVID pandemic and a last minute withdrawal by an investor resulted in the decision to close Disberse. Wanting to encourage anyone thinking of starting their own moonshot project, Disberse converted its [website](https://www.disberse.com/) into a learning hub, where it shared its journey and the lessons learned along the way.

:::

:::info Case Study: FrontlineSMS

FrontlineSMS was a product that automated connections to communities using SMS, missed calls, mobile payments, and API requests to trigger specific activities or actions. It was a fully featured system that could segment lists of users, filter responses, automate replies, and much more. After 15 years of operations, the team at FrontlineSMS decided to wind down its business, not because they had to, but because they wanted to. As they said in a [blog post](https://www.frontlinesms.com/blog/2021/6/28/frontlinesms-is-closing), “We are choosing to stop. We are closing for a range of reasons, but one of them—honestly—is to challenge the notion of success in this space. Companies do not need to be large, or go on forever, to succeed.”  This is not common, but a choice available to everyone. You can read more about the decision [here](https://www.frontlinesms.com/blog/2021/6/28/frontlinesms-is-closing) and [here](https://www.frontlinesms.com/blog/2021/7/19/frontline-business).

:::

## Key Takeaways

1. There are eight end game options. Each one entails different considerations and choosing depends largely on the role your organization wants to fulfill moving forward and the resources required to do so.

2. To reach the desired end game, your organization has to actively build the needed capabilities as the organization matures.

3. The eight end game options are not mutually exclusive. For instance, the give away and winding down options are often combined.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Outline your end game choice.

:::
