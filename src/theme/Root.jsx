import React, { Suspense } from 'react'
import BrowserOnly from '@docusaurus/BrowserOnly'
import PropTypes from 'prop-types'
import { RecoilRoot } from 'recoil'
import {
	Flip,
	ToastContainer,
} from 'react-toastify'
import {
	StyledEngineProvider,
	ThemeProvider,
} from '@mui/material/styles'
import SessionManager from '../components/SessionManager'
import sustkMuiTheme from './sustkMuiTheme'

function Root({ children }) {
	return (
		<StyledEngineProvider injectFirst>
			<ThemeProvider theme={sustkMuiTheme}>
				<RecoilRoot>
					{children}
					<ToastContainer
						draggable={false}
						transition={Flip}
					/>
					<BrowserOnly>
						{() => (
							<Suspense fallback="Loading...">
								<SessionManager />
							</Suspense>
						)}
					</BrowserOnly>
				</RecoilRoot>
			</ThemeProvider>
		</StyledEngineProvider>
	)
}

Root.propTypes = {
	children: PropTypes.node.isRequired,
}

export default Root
