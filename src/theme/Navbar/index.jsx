import React from 'react'
import OriginalNavbar from '@theme-original/Navbar'
import Link from '@docusaurus/Link'
import styles from './index.module.css'

export default function Navbar(props) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props.
	return (
		<>
			<div className={styles.powerbar}>
				Powered by the
				{' '}
				<Link href="https://digitalprinciples.org/">
					Principles
					{' '}
					<i>for</i>
					{' '}
					Digital Development
				</Link>
			</div>
			<OriginalNavbar {...props} />
		</>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}
