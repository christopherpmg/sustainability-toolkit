import { createTheme } from '@mui/material/styles'

let sustkMuiThemeMutable = createTheme({
	palette: {
		primary: {
			light: '#66AAE2', // --sustk-color-brand-light
			main: '#0072CE', // --sustk-color-brand
			dark: '#00569B', // --sustk-color-brand-dark
		},
		secondary: {
			light: '#66A3A4', // --sustk-color-teal-light
			main: '#006667', // --sustk-color-teal
			dark: '#004D4D', // --sustk-color-teal-dark
		},
		error: {
			light: '#F26350', // --sustk-color-red-light
			main: '#EF3C24', // --sustk-color-red
			dark: '#BF2600', // --sustk-color-red-dark
		},
		warning: {
			light: '#FFB149', // --sustk-color-orange-light
			main: '#FF9E1B', // --sustk-color-orange
			dark: '#DC731C', // --sustk-color-orange-dark
		},
		success: {
			light: '#33AD5C', // --sustk-color-green-light
			main: '#009933', // --sustk-color-green
			dark: '#007326', // --sustk-color-green-dark
		},
	},
	typography: {
		fontFamily: `'Montserrat', system-ui, -apple-system,
			BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif,
			'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'`, // --sustk-font-family-sans-serif
		fontWeightMedium: 600, // --sustk-font-weight-semibold
	},
	transitions: {
		duration: {
			standard: 200, // --sustk-transition-timing
		},
	},
})

// This is where we add attributes that need to reference previously-set attributes.
sustkMuiThemeMutable = createTheme(sustkMuiThemeMutable, {
	palette: {
		info: {
			light: sustkMuiThemeMutable.palette.primary.light,
			main: sustkMuiThemeMutable.palette.primary.main,
			dark: sustkMuiThemeMutable.palette.primary.dark,
		},
	},
})

// The linter kindly requests that we only export variables defined with `const`, not `let`.
const sustkMuiTheme = sustkMuiThemeMutable

export default sustkMuiTheme
