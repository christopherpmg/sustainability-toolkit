import { useEffect } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
	useResetRecoilState,
} from 'recoil'
import { debounce } from 'debounce'
import {
	isBrowserRuntime,
	makePatient,
	loadItemFromApiStorage,
	saveItemToApiStorage,
} from '../utils'
import {
	API_SAVE_DEBOUNCE_DURATION,
	SESSION_STATE,
} from '../constants'
import authTokenAtom from '../recoil/data/authToken'
import sessionStateAtom from '../recoil/data/sessionState'
import channelActivitiesAtom from '../recoil/data/channelActivities'
import nextProductComponentIdAtom from '../recoil/data/nextProductComponentId'
import productComponentsAtom from '../recoil/data/productComponents'
import stickiesAtom from '../recoil/data/stickies'
import valuePropositionsAtom from '../recoil/data/valuePropositions'
import networkStakeholdersAtom from '../recoil/data/networkStakeholders'
import stakeholdersAtom from '../recoil/data/stakeholders'
import competitorsAtom from '../recoil/data/competitors'
import componentComparisonsAtom from '../recoil/data/componentComparisons'
import selectedRevenueModelIdsAtom from '../recoil/data/selectedRevenueModelIds'
import agileEvaluationsAtom from '../recoil/data/agileEvaluations'
import revenueModelAssessmentsAtom from '../recoil/data/revenueModelAssessments'

const patientDebouncedFunctions = {}

function getPatientDebouncedFunction(key, functionToDebounce) {
	if (!(key in patientDebouncedFunctions)) {
		patientDebouncedFunctions[key] = debounce(
			makePatient(functionToDebounce),
			API_SAVE_DEBOUNCE_DURATION,
		)
	}

	return patientDebouncedFunctions[key]
}

function SessionManager() {
	if (!isBrowserRuntime()) {
		return null
	}

	const authToken = useRecoilValue(authTokenAtom)
	const resetAuthToken = useResetRecoilState(authTokenAtom)
	const sessionState = useRecoilValue(sessionStateAtom)
	const setSessionState = useSetRecoilState(sessionStateAtom)

	// Add atoms here that we want saved on the server
	const sessionAtoms = [
		channelActivitiesAtom,
		nextProductComponentIdAtom,
		productComponentsAtom,
		stickiesAtom,
		valuePropositionsAtom,
		networkStakeholdersAtom,
		stakeholdersAtom,
		competitorsAtom,
		componentComparisonsAtom,
		selectedRevenueModelIdsAtom,
		agileEvaluationsAtom,
		revenueModelAssessmentsAtom,
	]

	const saveSessionValueToServer = async (key, value, currentAuthToken) => {
		const result = await saveItemToApiStorage(key, value, currentAuthToken)
		if (result.status === 403) {
			setSessionState(SESSION_STATE.LOGGING_OUT)
		}
	}

	const sessionAtomAccessors = sessionAtoms.map((sessionAtom) => ({
		key: sessionAtom.key,
		value: useRecoilValue(sessionAtom),
		setter: useSetRecoilState(sessionAtom),
		resetter: useResetRecoilState(sessionAtom),
		debouncedPatientSaveSessionValueToServer:
			getPatientDebouncedFunction(
				sessionAtom.key,
				saveSessionValueToServer,
			),
	}))

	sessionAtomAccessors.forEach(({
		key,
		value,
		debouncedPatientSaveSessionValueToServer,
	}) => {
		useEffect(async () => {
			if (sessionState === SESSION_STATE.LOGGED_IN) {
				debouncedPatientSaveSessionValueToServer.clear()
				await debouncedPatientSaveSessionValueToServer(key, value, authToken)
			}
		}, [value])
	})

	const loadSessionValueFromServer = async (key, setter) => {
		const item = await loadItemFromApiStorage(key, authToken)
		if (item !== null) {
			setter(item.value)
		}
	}

	const saveAllSessionValuesToServer = async () => {
		const saveRequests = sessionAtomAccessors.map(
			({
				key,
				value,
				debouncedPatientSaveSessionValueToServer,
			}) => debouncedPatientSaveSessionValueToServer(key, value, authToken),
		)

		return Promise.all(saveRequests)
	}

	const loadAllSessionValuesFromServer = async () => {
		const loadRequests = sessionAtomAccessors.map(
			({ key, setter }) => loadSessionValueFromServer(key, setter),
		)

		return Promise.all(loadRequests)
	}

	const resetAllSessionValues = () => {
		sessionAtomAccessors.forEach(
			({ resetter }) => resetter(),
		)
	}

	useEffect(async () => {
		switch (sessionState) {
		case SESSION_STATE.LOGGING_IN:
			// Load all (non-null) data from the server
			await loadAllSessionValuesFromServer()
			setSessionState(SESSION_STATE.LOGGED_IN)
			break
		case SESSION_STATE.LOGGED_IN:
			// Save current state *after* login is completely successful
			// to ensure no data is lingering on the client that hasn't
			// been saved.
			await saveAllSessionValuesToServer()
			break
		case SESSION_STATE.LOGGING_OUT:
			// Wipe auth data and reset all atoms to their default state
			resetAuthToken()
			resetAllSessionValues()
			setSessionState(SESSION_STATE.LOGGED_OUT)
			break
		case SESSION_STATE.RECONNECTED:
			setSessionState(SESSION_STATE.LOGGED_IN)
			break
		default:
			break
		}
	}, [sessionState])

	return null
}

export default SessionManager
