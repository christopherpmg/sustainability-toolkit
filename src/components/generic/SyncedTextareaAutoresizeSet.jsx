import React, { useState } from 'react'
import PropTypes from 'prop-types'
import SyncedTextAreaAutoresize from './SyncedTextareaAutoresize'

function getMaxValue(arr) {
	const safeArray = arr.filter(Number)

	return Math.max(0, ...safeArray)
}

function SyncedTextareaAutoresizeSet({
	textValues,
	updateTextValue,
	minRows,
	wrapper,
}) {
	const [textAreaInternalHeights, setTextAreaInternalHeights] = useState([])
	// We NEED to add a tiny random value to our height.  This ensures height is re-applied
	// to the SyncedTextAreaAutoresize components every single time any height changes.
	// Without this, the underlying TextAreaAutoresize component will override the max height
	// based on its own height logic, ignoring our most recently applied max value.
	const rowHeight = getMaxValue(textAreaInternalHeights) + Math.random() / 100000

	const handleTextAreaHeightChange = (newHeight, index) => {
		const newInternalHeights = [...textAreaInternalHeights]
		newInternalHeights[index] = newHeight
		setTextAreaInternalHeights(newInternalHeights)
	}

	const Wrapper = wrapper

	return (
		<>
			{
				textValues.map((_, index) => (
					// We are truly mapping indices to these elements, and so array index IS the correct key
					// eslint-disable-next-line react/no-array-index-key
					<Wrapper key={index}>
						<SyncedTextAreaAutoresize
							index={index}
							value={textValues[index]}
							onChange={updateTextValue}
							onHeightChange={handleTextAreaHeightChange}
							height={rowHeight}
							minRows={minRows}
						/>
					</Wrapper>
				))
			}
		</>
	)
}

SyncedTextareaAutoresizeSet.defaultProps = {
	minRows: 1,
	wrapper: 'div',
}

SyncedTextareaAutoresizeSet.propTypes = {
	textValues: PropTypes.arrayOf(PropTypes.string).isRequired,
	updateTextValue: PropTypes.func.isRequired,
	minRows: PropTypes.number,
	wrapper: PropTypes.elementType,
}

export default SyncedTextareaAutoresizeSet
