import React from 'react'
import PropTypes from 'prop-types'
import TextareaAutosize from 'react-textarea-autosize'

function SyncedTextAreaAutoresize({
	height,
	index,
	onHeightChange,
	onChange,
	...remainingProps
}) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props
	// so that this component can be treated identically to the MUI Select component.

	const handleHeightChange = (newHeight) => {
		onHeightChange(newHeight, index)
	}

	const handleValueChange = (event) => {
		onChange(event.target.value, index)
	}

	return (
		<TextareaAutosize
			style={{ height }}
			onHeightChange={handleHeightChange}
			onChange={handleValueChange}
			{...remainingProps}
		/>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}

SyncedTextAreaAutoresize.propTypes = {
	height: PropTypes.number.isRequired,
	index: PropTypes.number.isRequired,
	onHeightChange: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired,
}

export default SyncedTextAreaAutoresize
