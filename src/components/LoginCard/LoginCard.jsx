import React from 'react'
import styles from './LoginCard.module.css'

function LoginCard() {
	return (
		<div className={`
			card
			shadow--md
			${styles.loginCard}
		`}
		>
			<div className="card__header">
				<h2>
					Log in to the Toolkit
				</h2>
			</div>
			<div className="card__body">
				<div className={styles.loginBlock}>
					<p>
						Click the button below and log in to the Sustainability Toolkit.
						You’ll be returned to the Toolkit automatically.
					</p>
					<a
						href="/api/auth/login?callbackPath=/loginWithToken"
						className={`
							button
							button--primary
							button--block
						`}
					>
						Log in
					</a>
				</div>
				<hr />
				<div className={styles.signupBlock}>
					<p>
						<b>Don’t have an account on the Tookit yet?</b>
						{' '}
						Click on the button below to create an account.
					</p>
					<a
						href="/api/auth/signup?callbackPath=/loginWithToken"
						className={`
							button
							button--primary
							button--block
						`}
					>
						Sign Up
					</a>
				</div>
			</div>
		</div>

	)
}

export default LoginCard
