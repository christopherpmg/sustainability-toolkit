import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	Select,
	MenuItem,
	TextField,
	IconButton,
} from '@mui/material'
import TextareaAutosize from 'react-textarea-autosize'
import { XCircleIcon, ChevronDownIcon } from '@heroicons/react/outline'
import { productComponentType } from '../../../types'
import styles from '../Tools.modules.css'
import { PRODUCT_COMPONENT_ADAPTABILITY_TYPE } from '../../../constants'

function ProductComponentEditorRow({
	productComponent,
	updateProductComponent,
	removeProductComponent,
}) {
	const handleNameChange = (event) => {
		updateProductComponent({
			...productComponent,
			name: event.target.value,
		})
	}

	const handleNowChange = (event) => {
		updateProductComponent({
			...productComponent,
			now: event.target.value,
		})
	}

	const handleFutureChange = (event) => {
		updateProductComponent({
			...productComponent,
			future: event.target.value,
		})
	}

	const handleActionsChange = (event) => {
		updateProductComponent({
			...productComponent,
			actions: event.target.value,
		})
	}

	const handleRemoveClick = () => {
		// This is only temporary until we add a proper MUI Dialog confirmation
		// eslint-disable-next-line no-alert
		if (window.confirm('Are you sure you want to delete this component?')) {
			removeProductComponent(productComponent)
		}
	}

	return (
		<TableRow>
			<TableCell
				component="th"
				scope="row"
			>
				{ productComponent.id }
			</TableCell>
			<TableCell>
				<TextField
					aria-label="Component"
					value={productComponent.name}
					onChange={handleNameChange}
				/>
			</TableCell>
			<TableCell>
				<Select
					className={styles.select}
					IconComponent={ChevronDownIcon}
					value={productComponent.now}
					onChange={handleNowChange}
					aria-label="Now"
				>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.CORE}>Core</MenuItem>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.MODULAR}>Modular</MenuItem>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.HACKABLE}>Hackable</MenuItem>
				</Select>
			</TableCell>
			<TableCell>
				<Select
					className={styles.select}
					value={productComponent.future}
					IconComponent={ChevronDownIcon}
					onChange={handleFutureChange}
					aria-label="Future"
				>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.CORE}>Core</MenuItem>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.MODULAR}>Modular</MenuItem>
					<MenuItem value={PRODUCT_COMPONENT_ADAPTABILITY_TYPE.HACKABLE}>Hackable</MenuItem>
				</Select>
			</TableCell>
			<TableCell>
				<TextareaAutosize
					value={productComponent.actions}
					onChange={handleActionsChange}
					aria-label="Actions to make change"
					minRows={1}
				/>
			</TableCell>
			<TableCell>
				<IconButton
					className={styles.remove}
					aria-label="Delete component"
					onClick={handleRemoveClick}
					size="small"
				>
					<XCircleIcon width="24" />
				</IconButton>
			</TableCell>
		</TableRow>
	)
}

ProductComponentEditorRow.propTypes = {
	productComponent: productComponentType.isRequired,
	updateProductComponent: PropTypes.func.isRequired,
	removeProductComponent: PropTypes.func.isRequired,
}

export default ProductComponentEditorRow
