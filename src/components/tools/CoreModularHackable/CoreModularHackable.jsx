import React, { useCallback } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableFooter,
	TableRow,
	TableCell,
} from '@mui/material'
import styles from '../Tools.modules.css'
import InnerTableAlertRow from '../InnerTableAlertRow'
import productComponentsAtom, { throughNowFilter } from '../../../recoil/data/productComponents'
import nextProductComponentIdAtom from '../../../recoil/data/nextProductComponentId'
import { replaceKeyedObject } from '../../../utils'
import { PRODUCT_COMPONENT_ADAPTABILITY_TYPE } from '../../../constants'
import CoreModularHackableEditorRow from './CoreModularHackableEditorRow'
import CoreModularHackableFooter from './CoreModularHackableFooter'
import { generateProductComponent } from './utils'

function CoreModularHackable() {
	const nextProductComponentId = useRecoilValue(nextProductComponentIdAtom)
	const setNextProductComponentId = useSetRecoilState(nextProductComponentIdAtom)
	const productComponents = useRecoilValue(throughNowFilter([
		PRODUCT_COMPONENT_ADAPTABILITY_TYPE.CORE,
		PRODUCT_COMPONENT_ADAPTABILITY_TYPE.MODULAR,
		PRODUCT_COMPONENT_ADAPTABILITY_TYPE.HACKABLE,
		'',
	]))
	const setProductComponents = useSetRecoilState(productComponentsAtom)

	const incrementNextProductComponentId = (incrementAmount = 1) => {
		setNextProductComponentId(nextProductComponentId + incrementAmount)
	}

	const updateProductComponent = useCallback((productComponent) => {
		setProductComponents((previousProductComponents) => (
			replaceKeyedObject(productComponent, previousProductComponents)
		))
	}, [])

	const removeProductComponent = useCallback((productComponent) => {
		setProductComponents((previousProductComponents) => (
			previousProductComponents.filter((item) => item.key !== productComponent.key)
		))
	}, [])

	const addProductComponent = useCallback(() => {
		const newProductComponent = generateProductComponent(nextProductComponentId)
		incrementNextProductComponentId()
		setProductComponents((previousProductComponents) => [
			...previousProductComponents,
			newProductComponent,
		])
	}, [])

	const hasProductComponents = productComponents.length > 0

	// TODO: Build out Download PDF functionality (#129)
	// const showDownloadButton = (hasProductComponents)
	const showDownloadButton = false

	return (
		<div className={styles.interactive}>
			<TableContainer>
				<Table aria-label="Core, Modular, Hackable Tool">
					<TableHead>
						<TableRow className={styles.header}>
							<TableCell component="th">
								ID
							</TableCell>
							<TableCell component="th">
								Component
							</TableCell>
							<TableCell
								component="th"
								width={125}
							>
								Now
							</TableCell>
							<TableCell
								component="th"
								width={125}
							>
								Future
							</TableCell>
							<TableCell component="th">
								Actions to make change
							</TableCell>
							<TableCell />
						</TableRow>
					</TableHead>
					<TableBody>
						{hasProductComponents ? productComponents.map((productComponent) => (
							<CoreModularHackableEditorRow
								key={productComponent.key}
								productComponent={productComponent}
								updateProductComponent={updateProductComponent}
								removeProductComponent={removeProductComponent}
							/>
						)) : (
							<InnerTableAlertRow colSpan={6}>
								It looks like you don’t have any components!
								Click “Add another component” below.
							</InnerTableAlertRow>
						)}
					</TableBody>
					<TableFooter>
						<TableRow>
							<TableCell />
							<TableCell colSpan={4}>
								<CoreModularHackableFooter
									addProductComponent={addProductComponent}
									showDownloadButton={showDownloadButton}
								/>
							</TableCell>
							<TableCell />
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
			<p className="inline-credit">© Ian Gray</p>
		</div>
	)
}

export default CoreModularHackable
