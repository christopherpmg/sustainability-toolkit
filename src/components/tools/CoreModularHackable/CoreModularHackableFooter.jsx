import React from 'react'
import PropTypes from 'prop-types'
import {
	PlusCircleIcon,
	DownloadIcon,
} from '@heroicons/react/outline'
import styles from '../Tools.modules.css'

function CoreModularHackableFooter({
	addProductComponent,
	showDownloadButton,
}) {
	return (
		<div className={styles.controls}>
			<button
				className={`
					button
					button--link
					button--with-icon
					${styles.controlButton}
				`}
				tabIndex="0"
				type="button"
				aria-label="add"
				onClick={addProductComponent}
			>
				<PlusCircleIcon />
				Add another component
			</button>
			{showDownloadButton ? (
				<button
					className={`
						button
						button--link
						button--with-icon
						${styles.controlButton}
					`}
					tabIndex="0"
					type="button"
					aria-label="download"
					disabled
				>
					<DownloadIcon />
					Download PDF
				</button>
			) : null}
		</div>
	)
}

CoreModularHackableFooter.propTypes = {
	addProductComponent: PropTypes.func.isRequired,
	showDownloadButton: PropTypes.bool.isRequired,
}

export default CoreModularHackableFooter
