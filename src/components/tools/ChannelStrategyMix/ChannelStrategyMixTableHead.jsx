import React from 'react'
import {
	TableHead,
	TableRow,
	TableCell,
} from '@mui/material'
import {
	ACTIVITY_CHANNEL,
	ACTIVITY_CHANNEL_DISPLAY_ORDER,
	FOUNDATIONAL_ACTIVITY_CHANNEL_DISPLAY_ORDER,
	DIRECT_ACTIVITY_CHANNEL_DISPLAY_ORDER,
} from '../../../constants'
import styles from '../Tools.modules.css'

function getActivityChannelLabel(channel) {
	switch (channel) {
	case ACTIVITY_CHANNEL.ONLINE:
		return 'Online'
	case ACTIVITY_CHANNEL.CONFERENCE:
		return 'Conference'
	case ACTIVITY_CHANNEL.CONVENING:
		return 'Convening'
	case ACTIVITY_CHANNEL.WORD_OF_MOUTH:
		return 'Word of Mouth'
	case ACTIVITY_CHANNEL.PARTNERSHIPS:
		return 'Partnerships'
	case ACTIVITY_CHANNEL.PROCUREMENT_PROCESSES:
		return 'Procurement'
	default:
		return ''
	}
}

function ChannelStrategyMixTableHead() {
	return (
		<TableHead>
			<TableRow className={styles.header}>
				<TableCell colSpan={2} />
				<TableCell
					className={styles.superHeaderCell}
					colSpan={FOUNDATIONAL_ACTIVITY_CHANNEL_DISPLAY_ORDER.length}
				>
					Foundational Channels
				</TableCell>
				<TableCell
					className={styles.superHeaderCell}
					colSpan={DIRECT_ACTIVITY_CHANNEL_DISPLAY_ORDER.length}
				>
					Direct Channels
				</TableCell>
			</TableRow>
			<TableRow className={`
				${styles.header}
				${styles.hasSuperHeader}
			`}
			>
				<TableCell colSpan={2} />
				{
					ACTIVITY_CHANNEL_DISPLAY_ORDER.map((activityChannel) => (
						<TableCell
							component="th"
							key={activityChannel}
						>
							{ getActivityChannelLabel(activityChannel) }
						</TableCell>
					))
				}
			</TableRow>
		</TableHead>
	)
}

export default ChannelStrategyMixTableHead
