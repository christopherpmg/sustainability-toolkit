import React from 'react'
import PropTypes from 'prop-types'
import {
	TableBody,
	TableRow,
	TableCell,
} from '@mui/material'
import {
	ACTIVITY_STAGE_DISPLAY_ORDER,
} from '../../../constants'
import styles from '../Tools.modules.css'
import ChannelStrategyMixEditorRow from './ChannelStrategyMixEditorRow'

function ChannelStrategyMixTableBody({
	updateChannelActivity,
	channelActivities,
}) {
	return (
		<TableBody>
			<TableRow>
				<TableCell
					component="th"
					className={`
						${styles.superHeaderCell}
						${styles.rotatedCell}
					`}
					rowSpan={ACTIVITY_STAGE_DISPLAY_ORDER.length + 1}
				>
					<div className={styles.rotatedCellInner}>
						Strategy
					</div>
				</TableCell>
			</TableRow>
			{
				ACTIVITY_STAGE_DISPLAY_ORDER.map((activityStage, stageIndex) => (
					<ChannelStrategyMixEditorRow
						key={activityStage}
						stage={activityStage}
						channelActivitiesForStage={channelActivities[stageIndex]}
						updateChannelActivity={updateChannelActivity}
					/>
				))
			}
		</TableBody>
	)
}

ChannelStrategyMixTableBody.propTypes = {
	channelActivities: PropTypes.arrayOf(
		PropTypes.arrayOf(PropTypes.string),
	).isRequired,
	updateChannelActivity: PropTypes.func.isRequired,
}

export default ChannelStrategyMixTableBody
