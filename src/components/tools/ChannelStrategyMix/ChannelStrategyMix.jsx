import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
} from '@mui/material'
import styles from '../Tools.modules.css'
import channelActivitiesAtom,
{ withStageAndChannelStructure } from '../../../recoil/data/channelActivities'
import ChannelStrategyMixTableHead from './ChannelStrategyMixTableHead'
import ChannelStrategyMixTableBody from './ChannelStrategyMixTableBody'

function getChannelActivityHash(channelActivity) {
	return `${channelActivity.stage}_${channelActivity.channel}`
}

function ChannelStrategyMix() {
	const channelActivitiesHashMap = useRecoilValue(channelActivitiesAtom)
	const channelActivities = useRecoilValue(withStageAndChannelStructure)
	const setChannelActivitiesHashMap = useSetRecoilState(channelActivitiesAtom)
	const updateChannelActivity = (channelActivity) => {
		const newChannelActivitiesHashMap = {
			...channelActivitiesHashMap,
		}
		const channelActivityHash = getChannelActivityHash(channelActivity)
		newChannelActivitiesHashMap[channelActivityHash] = channelActivity
		setChannelActivitiesHashMap(newChannelActivitiesHashMap)
	}

	return (
		<div className={styles.interactive}>
			<TableContainer>
				<Table aria-label="Channel Strategy Mix">
					<ChannelStrategyMixTableHead />
					<ChannelStrategyMixTableBody
						channelActivities={channelActivities}
						updateChannelActivity={updateChannelActivity}
					/>
				</Table>
			</TableContainer>
			<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>
		</div>
	)
}

export default ChannelStrategyMix
