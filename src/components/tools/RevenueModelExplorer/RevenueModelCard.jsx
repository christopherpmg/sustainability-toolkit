import React from 'react'
import PropTypes from 'prop-types'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import { revenueModelType } from '../../../types'
import revenueModelExplorerCurrentStateAtom from '../../../recoil/data/revenueModelExplorerCurrentState'
import selectedRevenueModelIdsAtom from '../../../recoil/data/selectedRevenueModelIds'
import styles from './RevenueModelCard.module.css'

function RevenueModelCard({
	revenueModel,
	revenueModelCategoryId,
}) {
	const setRevenueModelExplorerCurrentState = useSetRecoilState(
		revenueModelExplorerCurrentStateAtom,
	)
	const selectedRevenueModelIds = useRecoilValue(selectedRevenueModelIdsAtom)
	const setSelectedRevenueModelIds = useSetRecoilState(selectedRevenueModelIdsAtom)

	const toggleSelectedRevenueModelId = (revenueModelId) => {
		setSelectedRevenueModelIds((previousState) => {
			if (previousState.includes(revenueModelId)) {
				return previousState.filter((id) => id !== revenueModelId)
			}

			return [
				...previousState,
				revenueModelId,
			]
		})
	}

	const handleSelectCardClick = () => {
		toggleSelectedRevenueModelId(revenueModel.id)
	}

	const handleReadMoreClick = () => {
		setRevenueModelExplorerCurrentState((previousState) => ({
			...previousState,
			openRevenueModel: (previousState.openRevenueModel === revenueModel ? null : revenueModel),
			openRevenueModelCategoryId:
				(previousState.openRevenueModelCategoryId === revenueModelCategoryId
					? null
					: revenueModelCategoryId
				),
		}))
	}

	const isSelected = selectedRevenueModelIds.includes(revenueModel.id)

	return (
		<div
			key={revenueModel.id}
			className={`
				card
				${styles.card}
				${styles[`deck--${revenueModelCategoryId}`]}
			`}
		>
			<div className={`
				card__header
				${styles.cardHeader}
			`}
			>
				{revenueModel.title}
			</div>
			<div className={`
				card__body
				${styles.cardBody}
			`}
			>
				<div className={styles.cardBodySummary}>
					<p>{revenueModel.summary}</p>
				</div>
			</div>
			<div className={`
				card__footer
				${styles.cardFooter}
			`}
			>
				<div className={`
					button-set
					${styles.cardButtons}
				`}
				>
					<button
						className="button button--link"
						type="button"
						onClick={handleReadMoreClick}
					>
						Read more
					</button>
					<button
						className={`
							button
							${isSelected ? 'button--success' : 'button--secondary button--outline'}
						`}
						type="button"
						onClick={handleSelectCardClick}
					>
						{isSelected
							? 'Selected'
							: 'Select'}
					</button>
				</div>
			</div>
		</div>
	)
}

RevenueModelCard.propTypes = {
	revenueModel: revenueModelType.isRequired,
	revenueModelCategoryId: PropTypes.string.isRequired,
}

export default React.memo(RevenueModelCard)
