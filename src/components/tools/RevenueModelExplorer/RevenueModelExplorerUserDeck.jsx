import React from 'react'
import { useRecoilValue } from 'recoil'
import Link from '@docusaurus/Link'
import selectedRevenueModelIdsAtom from '../../../recoil/data/selectedRevenueModelIds'
import styles from './RevenueModelExplorerUserDeck.module.css'

function RevenueModelExplorerUserDeck() {
	const selectedRevenueModelIds = useRecoilValue(selectedRevenueModelIdsAtom)

	const revenueModelsCount = selectedRevenueModelIds.length

	return (
		<div className={styles.userDeck}>
			<div>
				{`You’ve selected ${revenueModelsCount} revenue model${revenueModelsCount !== 1 ? 's' : ''}.`}
			</div>
			<Link
				to="#impact-matrix-tool"
				className={`
					button button--sm
					${revenueModelsCount > 0 ? 'button--success' : 'button--secondary'}
				`}
			>
				Done? Go to the Impact Matrix Tool ↓
			</Link>
		</div>
	)
}

export default RevenueModelExplorerUserDeck
