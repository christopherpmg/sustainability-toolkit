import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	Box,
	Modal,
} from '@mui/material'
import revenueModelExplorerCurrentStateAtom from '../../../recoil/data/revenueModelExplorerCurrentState'
import selectedRevenueModelIdsAtom from '../../../recoil/data/selectedRevenueModelIds'
import styles from './RevenueModelExplorerModal.module.css'
import cardStyles from './RevenueModelCard.module.css'

function RevenueModelExplorerModal() {
	const revenueModelExplorerCurrentState = useRecoilValue(revenueModelExplorerCurrentStateAtom)
	const setRevenueModelExplorerCurrentState = useSetRecoilState(
		revenueModelExplorerCurrentStateAtom,
	)
	const selectedRevenueModelIds = useRecoilValue(selectedRevenueModelIdsAtom)
	const setSelectedRevenueModelIds = useSetRecoilState(selectedRevenueModelIdsAtom)

	const toggleSelectedRevenueModelId = (revenueModelId) => {
		setSelectedRevenueModelIds((previousState) => {
			if (previousState.includes(revenueModelId)) {
				return previousState.filter((id) => id !== revenueModelId)
			}

			return [
				...previousState,
				revenueModelId,
			]
		})
	}

	const handleSelectClick = () => {
		const { openRevenueModel: { id: openRevenueModelId } } = revenueModelExplorerCurrentState

		toggleSelectedRevenueModelId(openRevenueModelId)
	}

	const handleCloseClick = () => {
		setRevenueModelExplorerCurrentState((previousState) => ({
			...previousState,
			openRevenueModel: null,
			openRevenueModelCategoryId: null,
		}))
	}

	const {
		openRevenueModel,
		openRevenueModelCategoryId,
	} = revenueModelExplorerCurrentState

	const isOpen = !!openRevenueModel
	const isSelected = openRevenueModel && selectedRevenueModelIds.includes(openRevenueModel.id)

	return (
		<Modal
			open={isOpen}
			onBackdropClick={handleCloseClick}
			className={styles.modal}
		>
			<Box className={styles.box}>
				{isOpen && (
					<div
						key={openRevenueModel.id}
						className={`
							card
							${cardStyles.card}
							${cardStyles[`deck--${openRevenueModelCategoryId}`]}
						`}
					>
						<div className={`
							card__header
							${cardStyles.cardHeader}
						`}
						>
							{openRevenueModel.title}
						</div>
						<div className={`
							card__body
							${cardStyles.cardBody}
						`}
						>
							<div className={cardStyles.cardBodyDetail}>
								{openRevenueModel.detail}
							</div>
						</div>
						<div className={`
							card__footer
							${cardStyles.cardFooter}
						`}
						>
							<div className="button-set button-set--vertical">
								<button
									className={`
										button
										button--block
										${isSelected ? 'button--success' : 'button--secondary button--outline'}
									`}
									type="button"
									onClick={handleSelectClick}
								>
									{isSelected
										? 'Selected'
										: 'Select this revenue model'}
								</button>
								<button
									className="button button--link"
									type="button"
									onClick={handleCloseClick}
								>
									Close
								</button>
							</div>

						</div>
					</div>

				)}
			</Box>
		</Modal>
	)
}

export default RevenueModelExplorerModal
