import React from 'react'
import PropTypes from 'prop-types'
import { useSetRecoilState } from 'recoil'
import revenueModelAssessmentsAtom from '../../../recoil/data/revenueModelAssessments'
import { revenueModelType } from '../../../types'
import styles from './RevenueModelAssessor.module.css'

function RevenueModelAssessorCheckbox({
	revenueModel,
	quadrant,
	checked,
}) {
	const setRevenueModelAssessments = useSetRecoilState(revenueModelAssessmentsAtom)

	const toggleRevenueModelAssessment = (newCheckedState) => {
		setRevenueModelAssessments((previousState) => {
			const revenueModelAssessments = { ...previousState }
			delete revenueModelAssessments[revenueModel.id]
			if (newCheckedState) {
				revenueModelAssessments[revenueModel.id] = quadrant
			}

			return revenueModelAssessments
		})
	}

	const handleCheckboxChange = (event) => {
		toggleRevenueModelAssessment(event.target.checked)
	}

	return (
		<label className={`
			${styles.revenueModelCheckboxWrapper}
			${styles[`revenueModelCheckboxWrapper--${quadrant}`]}
			${checked && styles.checked}
		`}
		>
			<input
				type="checkbox"
				checked={checked}
				onChange={handleCheckboxChange}
				className={styles.revenueModelCheckbox}
			/>
			<span className={styles.revenueModelCheckboxLabel}>
				{revenueModel.title}
			</span>
		</label>

	)
}

RevenueModelAssessorCheckbox.propTypes = {
	revenueModel: revenueModelType.isRequired,
	quadrant: PropTypes.string.isRequired,
	checked: PropTypes.bool.isRequired,
}

export default RevenueModelAssessorCheckbox
