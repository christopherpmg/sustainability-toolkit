import React from 'react'
import RevenueModelAssessorQuadrant from './RevenueModelAssessorQuadrant'
import styles from './RevenueModelAssessor.module.css'

function RevenueModelAssessor() {
	return (
		<div className={styles.wrapper}>
			<div className={`
				${styles.axisLabel}
				${styles.xAxisLabelHigh}
			`}
			>
				High Revenue Potential
			</div>
			<div className={`
				${styles.axisLabel}
				${styles.xAxisLabelLow}
			`}
			>
				Negative Revenue Potential
			</div>
			<div className={`
				${styles.axisLabel}
				${styles.yAxisLabelHigh}
			`}
			>
				High Impact
			</div>
			<div className={`
				${styles.axisLabel}
				${styles.yAxisLabelLow}
			`}
			>
				Low Impact
			</div>
			<div className={styles.matrix}>
				<RevenueModelAssessorQuadrant
					key="high-high"
					quadrant="high-high"
				/>
				<RevenueModelAssessorQuadrant
					key="high-low"
					quadrant="high-low"
				/>
				<RevenueModelAssessorQuadrant
					key="low-high"
					quadrant="low-high"
				/>
				<RevenueModelAssessorQuadrant
					key="low-low"
					quadrant="low-low"
				/>
			</div>
		</div>
	)
}

export default RevenueModelAssessor
