import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
} from '@mui/material'
import { PlusCircleIcon } from '@heroicons/react/outline'
import { productComponentType } from '../../../types'
import {
	PRODUCT_COMPONENT_ADAPTABILITY_TYPE,
} from '../../../constants'
import toolStyles from '../Tools.modules.css'
import ComponentComparisonRow from './ComponentComparisonRow'
import styles from './ComponentComparison.module.css'

function getLabelForComponentType(componentType) {
	switch (componentType) {
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.CORE:
		return 'Core Components'
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.MODULAR:
		return 'Modular Components'
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.HACKABLE:
		return 'Hackable Components'
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.WRAPAROUND:
		return 'Complementary Services'
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.NOT_PRESENT:
		return 'Components Not in Your Solution'
	default:
		return ''
	}
}

function ComponentComparisonSection({
	componentType,
	productComponentsForType,
	componentComparisonsForType,
	updateProductComponent,
	updateComponentComparison,
	removeProductComponent,
	addProductComponentWithType,
	shouldShowAddRemoveUi,
}) {
	return (
		<>
			<TableRow>
				<TableCell component="th" rowSpan={Math.max(2, productComponentsForType.length + 1)}>
					<div>
						{getLabelForComponentType(componentType)}
					</div>
					{ shouldShowAddRemoveUi ? (
						<button
							className={`
										button
										button--link
										button--with-icon
										${toolStyles.controlButton}
									`}
							tabIndex="0"
							type="button"
							aria-label="Add new"
							onClick={() => addProductComponentWithType(componentType)}
						>
							<PlusCircleIcon />
							Add
						</button>
					) : null }
				</TableCell>
			</TableRow>
			{productComponentsForType.length ? (
				productComponentsForType.map((productComponent, index) => {
					const componentComparisonsForProductComponent = componentComparisonsForType[index]

					return (
						<ComponentComparisonRow
							key={productComponent.key}
							productComponent={productComponent}
							componentComparisonsForProductComponent={componentComparisonsForProductComponent}
							updateComponentComparison={updateComponentComparison}
							updateProductComponent={updateProductComponent}
							removeProductComponent={removeProductComponent}
							shouldShowRemoveButton={shouldShowAddRemoveUi}
						/>

					)
				})
			) : (
				<TableRow>
					<TableCell colSpan={6} className={styles.emptyRowCell}>
						You don’t have any components of this type.
					</TableCell>
				</TableRow>
			)}
		</>
	)
}

ComponentComparisonSection.propTypes = {
	componentType: PropTypes.string.isRequired,
	productComponentsForType: PropTypes.arrayOf(productComponentType).isRequired,
	componentComparisonsForType: PropTypes.arrayOf(
		PropTypes.arrayOf(PropTypes.string),
	).isRequired,
	updateProductComponent: PropTypes.func.isRequired,
	updateComponentComparison: PropTypes.func.isRequired,
	removeProductComponent: PropTypes.func.isRequired,
	addProductComponentWithType: PropTypes.func.isRequired,
	shouldShowAddRemoveUi: PropTypes.bool.isRequired,
}

export default React.memo(ComponentComparisonSection)
