import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	FormControl,
	Select,
	MenuItem,
	IconButton,
} from '@mui/material'
import {
	ChevronDownIcon,
	XCircleIcon,
} from '@heroicons/react/outline'
import { stakeholderType } from '../../../types'
import toolStyles from '../Tools.modules.css'
import styles from './StakeholderMatrix.module.css'
import StakeholderMatrixEditorInput from './StakeholderMatrixEditorInput'

function StakeholderMatrixEditorRow({
	stakeholder,
	updateStakeholder,
	removeStakeholder,
}) {
	const handleFullNameChange = (event) => {
		updateStakeholder({
			...stakeholder,
			fullName: event.target.value,
		})
	}

	const handleImportantChange = (event) => {
		updateStakeholder({
			...stakeholder,
			important: event.target.value,
		})
	}

	const handleRoleChange = (event) => {
		updateStakeholder({
			...stakeholder,
			role: event.target.value,
		})
	}

	const handleImpactChange = (event) => {
		updateStakeholder({
			...stakeholder,
			impact: event.target.value,
		})
	}

	const handleInfluenceChange = (event) => {
		updateStakeholder({
			...stakeholder,
			influence: event.target.value,
		})
	}

	const handleObjectionsChange = (event) => {
		updateStakeholder({
			...stakeholder,
			objections: event.target.value,
		})
	}

	const handleEnablersChange = (event) => {
		updateStakeholder({
			...stakeholder,
			enablers: event.target.value,
		})
	}

	const handleEngagementActivitiesChange = (event) => {
		updateStakeholder({
			...stakeholder,
			engagementActivities: event.target.value,
		})
	}

	const handleContactChange = (event) => {
		updateStakeholder({
			...stakeholder,
			contact: event.target.value,
		})
	}

	const handleRemoveClick = () => {
		// This is only temporary until we add a proper MUI Dialog confirmation
		// eslint-disable-next-line no-alert
		if (window.confirm('Are you sure you want to delete this stakeholder?')) {
			removeStakeholder(stakeholder)
		}
	}

	return (
		<TableRow>
			<TableCell>
				<StakeholderMatrixEditorInput
					label="Name of stakeholder"
					id={`${stakeholder.key}-fullName`}
					value={stakeholder.fullName}
					onChange={handleFullNameChange}
				/>
			</TableCell>
			<TableCell>
				<StakeholderMatrixEditorInput
					label="What’s important to stakeholder?"
					id={`${stakeholder.key}-important`}
					value={stakeholder.important}
					onChange={handleImportantChange}
					multiline
				/>
				<StakeholderMatrixEditorInput
					label="What role does stakeholder play in decision-making?"
					id={`${stakeholder.key}-role`}
					value={stakeholder.role}
					onChange={handleRoleChange}
					multiline
				/>
			</TableCell>
			<TableCell>
				<FormControl>
					<label htmlFor={`${stakeholder.key}-impact`}>
						Impact
					</label>
					<Select
						id={`${stakeholder.key}-impact`}
						className={toolStyles.select}
						value={stakeholder.impact}
						onChange={handleImpactChange}
						IconComponent={ChevronDownIcon}
					>
						<MenuItem value="Low">Low</MenuItem>
						<MenuItem value="Medium">Medium</MenuItem>
						<MenuItem value="High">High</MenuItem>
					</Select>
				</FormControl>
				<FormControl>
					<label htmlFor={`${stakeholder.key}-influence`}>
						Impact
					</label>
					<Select
						id={`${stakeholder.key}-influence`}
						className={toolStyles.select}
						value={stakeholder.influence}
						onChange={handleInfluenceChange}
						IconComponent={ChevronDownIcon}
					>
						<MenuItem value="Low">Low</MenuItem>
						<MenuItem value="Medium">Medium</MenuItem>
						<MenuItem value="High">High</MenuItem>
					</Select>
				</FormControl>
			</TableCell>
			<TableCell>
				<div className={styles.actionPlanCell}>
					<StakeholderMatrixEditorInput
						label="Potential objections or barriers to the decision"
						id={`${stakeholder.key}-objections`}
						value={stakeholder.objections}
						onChange={handleObjectionsChange}
						multiline
					/>
					<StakeholderMatrixEditorInput
						label="Potential enablers to get the stakeholder to say “yes”"
						id={`${stakeholder.key}-enablers`}
						value={stakeholder.enablers}
						onChange={handleEnablersChange}
						multiline
					/>
					<StakeholderMatrixEditorInput
						label="Engagement activity to get the stakeholder to say “yes”"
						id={`${stakeholder.key}-engagementActivities`}
						value={stakeholder.engagementActivities}
						onChange={handleEngagementActivitiesChange}
						multiline
					/>
					<StakeholderMatrixEditorInput
						label="Responsible contact person"
						id={`${stakeholder.key}-contact`}
						value={stakeholder.contact}
						onChange={handleContactChange}
						multiline
					/>
				</div>
			</TableCell>
			<TableCell>
				<IconButton
					className={toolStyles.remove}
					aria-label="Delete stakeholder"
					onClick={handleRemoveClick}
					size="small"
				>
					<XCircleIcon width="24" />
				</IconButton>
			</TableCell>
		</TableRow>
	)
}

StakeholderMatrixEditorRow.propTypes = {
	stakeholder: stakeholderType.isRequired,
	updateStakeholder: PropTypes.func.isRequired,
	removeStakeholder: PropTypes.func.isRequired,
}

export default React.memo(StakeholderMatrixEditorRow)
