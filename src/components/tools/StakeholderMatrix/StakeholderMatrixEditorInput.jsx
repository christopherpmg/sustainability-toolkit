import React from 'react'
import PropTypes from 'prop-types'
import {
	FormControl,
	OutlinedInput,
	FormHelperText,
} from '@mui/material'

function StakeholderMatrixEditorInput({
	id,
	value,
	label,
	description,
	multiline,
	onChange,
}) {
	return (
		<FormControl>
			<label htmlFor={id}>
				{label}
			</label>
			<OutlinedInput
				id={id}
				value={value}
				onChange={onChange}
				multiline={multiline}
				aria-describedby={description}
			/>
			{description ? (
				<FormHelperText>
					{description}
				</FormHelperText>
			) : null}
		</FormControl>
	)
}

StakeholderMatrixEditorInput.propTypes = {
	id: PropTypes.string.isRequired,
	value: PropTypes.string,
	label: PropTypes.string.isRequired,
	description: PropTypes.string,
	multiline: PropTypes.bool,
	onChange: PropTypes.func.isRequired,
}

StakeholderMatrixEditorInput.defaultProps = {
	value: '',
	multiline: false,
	description: null,
}

export default StakeholderMatrixEditorInput
