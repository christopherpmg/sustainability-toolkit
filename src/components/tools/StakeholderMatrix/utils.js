import { v4 as uuidv4 } from 'uuid'

export function generateStakeholder() {
	return {
		key: uuidv4(),
		fullName: '',
		important: '',
		role: '',
		impact: '',
		influence: '',
		objections: '',
		enablers: '',
		engagementActivities: '',
		contact: '',
	}
}

export function generateStakeholders(count) {
	return Array.from(Array(count), () => generateStakeholder())
}

export function getStakeholderIndex(stakeholder, stakeholders) {
	return stakeholders.findIndex((s) => stakeholder.key === s.key)
}
