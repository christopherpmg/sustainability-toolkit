import React, { useCallback } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableFooter,
	TableRow,
	TableCell,
} from '@mui/material'
import toolStyles from '../Tools.modules.css'
import InnerTableAlertRow from '../InnerTableAlertRow'
import stakeholdersAtom from '../../../recoil/data/stakeholders'
import styles from './StakeholderMatrix.module.css'
import StakeholderMatrixEditorRow from './StakeholderMatrixEditorRow'
import StakeholderMatrixFooter from './StakeholderMatrixFooter'
import {
	generateStakeholder,
	getStakeholderIndex,
} from './utils'

function StakeholderMatrix() {
	const stakeholders = useRecoilValue(stakeholdersAtom)
	const setStakeholders = useSetRecoilState(stakeholdersAtom)

	const addStakeholder = useCallback(() => {
		const newStakeholder = generateStakeholder()

		setStakeholders((previousStakeholders) => ([
			...previousStakeholders,
			newStakeholder,
		]))
	}, [])

	const updateStakeholder = useCallback((stakeholder) => {
		setStakeholders((previousStakeholders) => {
			const updateIndex = getStakeholderIndex(stakeholder, previousStakeholders)
			const newStakeholders = [...previousStakeholders]
			newStakeholders[updateIndex] = stakeholder

			return newStakeholders
		})
	}, [])

	const removeStakeholder = useCallback((stakeholder) => {
		setStakeholders((previousStakeholders) => {
			const removeIndex = getStakeholderIndex(stakeholder, previousStakeholders)

			return previousStakeholders.filter((_, index) => index !== removeIndex)
		})
	}, [])

	const hasStakeholders = stakeholders.length > 0

	// TODO: Build out Download PDF functionality (#129)
	// const showDownloadButton = (hasStakeholders)
	const showDownloadButton = false

	return (
		<div className={toolStyles.interactive}>
			<TableContainer>
				<Table
					aria-label="Decision-Making Stakeholder Matrix"
					className={styles.stakeholderMatrix}
				>
					<TableHead>
						<TableRow className={toolStyles.header}>
							<TableCell component="th" style={{ width: '20%' }}>
								1. Identify
							</TableCell>
							<TableCell component="th" style={{ width: '20%' }}>
								2. Understand
							</TableCell>
							<TableCell component="th" style={{ width: '10%' }}>
								3. Map
							</TableCell>
							<TableCell component="th" style={{ width: '45%' }}>
								4. Make Action Plan
							</TableCell>
							<TableCell component="th" style={{ width: '5%' }} />
						</TableRow>
					</TableHead>
					<TableBody>
						{hasStakeholders
							? stakeholders.map((stakeholder) => (
								<StakeholderMatrixEditorRow
									key={stakeholder.key}
									stakeholder={stakeholder}
									updateStakeholder={updateStakeholder}
									removeStakeholder={removeStakeholder}
								/>
							)) : (
								<InnerTableAlertRow colSpan={6}>
									It looks like you don’t have any stakeholders!
									Click “Add another stakeholder” below.
								</InnerTableAlertRow>
							)}
					</TableBody>
					<TableFooter>
						<TableRow>
							<TableCell colSpan={6}>
								<StakeholderMatrixFooter
									addStakeholder={addStakeholder}
									showDownloadButton={showDownloadButton}
								/>
							</TableCell>
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
			<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>
		</div>
	)
}

export default StakeholderMatrix
