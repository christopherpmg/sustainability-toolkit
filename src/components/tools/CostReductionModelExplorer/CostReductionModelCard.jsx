import React from 'react'
import PropTypes from 'prop-types'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import { costReductionModelType } from '../../../types'
import costReductionModelExplorerCurrentStateAtom from '../../../recoil/data/costReductionModelExplorerCurrentState'
import selectedCostReductionModelIdsAtom from '../../../recoil/data/selectedCostReductionModelIds'
import styles from './CostReductionModelCard.module.css'

function CostReductionModelCard({
	costReductionModel,
	costReductionModelCategoryId,
}) {
	const setCostReductionModelExplorerCurrentState = useSetRecoilState(
		costReductionModelExplorerCurrentStateAtom,
	)
	const selectedCostReductionModelIds = useRecoilValue(selectedCostReductionModelIdsAtom)
	const setSelectedCostReductionModelIds = useSetRecoilState(selectedCostReductionModelIdsAtom)

	const toggleSelectedCostReductionModelId = (costReductionModelId) => {
		setSelectedCostReductionModelIds((previousState) => {
			if (previousState.includes(costReductionModelId)) {
				return previousState.filter((id) => id !== costReductionModelId)
			}

			return [
				...previousState,
				costReductionModelId,
			]
		})
	}

	const handleSelectCardClick = () => {
		toggleSelectedCostReductionModelId(costReductionModel.id)
	}

	const handleReadMoreClick = () => {
		setCostReductionModelExplorerCurrentState((previousState) => ({
			...previousState,
			openCostReductionModel: (
				previousState.openCostReductionModel === costReductionModel ? null : costReductionModel
			),
			openCostReductionModelCategoryId:
				(previousState.openCostReductionModelCategoryId === costReductionModelCategoryId
					? null
					: costReductionModelCategoryId
				),
		}))
	}

	const isSelected = selectedCostReductionModelIds.includes(costReductionModel.id)

	return (
		<div
			key={costReductionModel.id}
			className={`
				card
				${styles.card}
				${styles[`deck--${costReductionModelCategoryId}`]}
			`}
		>
			<div className={`
				card__header
				${styles.cardHeader}
			`}
			>
				{costReductionModel.title}
			</div>
			<div className={`
				card__body
				${styles.cardBody}
			`}
			>
				<div className={styles.cardBodySummary}>
					<p>{costReductionModel.summary}</p>
				</div>
			</div>
			<div className={`
				card__footer
				${styles.cardFooter}
			`}
			>
				<div className={`
					button-set
					${styles.cardButtons}
				`}
				>
					<button
						className="button button--link"
						type="button"
						onClick={handleReadMoreClick}
					>
						Read more
					</button>
					<button
						className={`
							button
							${isSelected ? 'button--success' : 'button--secondary button--outline'}
						`}
						type="button"
						onClick={handleSelectCardClick}
					>
						{isSelected
							? 'Selected'
							: 'Select'}
					</button>
				</div>
			</div>
		</div>
	)
}

CostReductionModelCard.propTypes = {
	costReductionModel: costReductionModelType.isRequired,
	costReductionModelCategoryId: PropTypes.string.isRequired,
}

export default React.memo(CostReductionModelCard)
