import React from 'react'
import PropTypes from 'prop-types'
import costReductionModelsData from './costReductionModelsData'
import CostReductionModelCard from './CostReductionModelCard'
import styles from './CostReductionModelCardDeck.module.css'

function CostReductionModelCardDeck({ costReductionModelCategoryId }) {
	const costReductionModelCategory = costReductionModelsData[costReductionModelCategoryId]

	return costReductionModelCategory ? (
		<div className={styles.deck}>
			{costReductionModelCategory.costReductionModels.map((costReductionModel) => (
				<CostReductionModelCard
					key={costReductionModel.id}
					costReductionModel={costReductionModel}
					costReductionModelCategoryId={costReductionModelCategoryId}
				/>
			))}
		</div>
	) : null
}

CostReductionModelCardDeck.propTypes = {
	costReductionModelCategoryId: PropTypes.string.isRequired,
}

export default CostReductionModelCardDeck
