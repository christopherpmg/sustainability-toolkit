import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	Box,
	Modal,
} from '@mui/material'
import costReductionModelExplorerCurrentStateAtom from '../../../recoil/data/costReductionModelExplorerCurrentState'
import selectedCostReductionModelIdsAtom from '../../../recoil/data/selectedCostReductionModelIds'
import styles from './CostReductionModelExplorerModal.module.css'
import cardStyles from './CostReductionModelCard.module.css'

function CostReductionModelExplorerModal() {
	const costReductionModelExplorerCurrentState = useRecoilValue(
		costReductionModelExplorerCurrentStateAtom,
	)
	const setCostReductionModelExplorerCurrentState = useSetRecoilState(
		costReductionModelExplorerCurrentStateAtom,
	)
	const selectedCostReductionModelIds = useRecoilValue(selectedCostReductionModelIdsAtom)
	const setSelectedCostReductionModelIds = useSetRecoilState(selectedCostReductionModelIdsAtom)

	const toggleSelectedCostReductionModelId = (costReductionModelId) => {
		setSelectedCostReductionModelIds((previousState) => {
			if (previousState.includes(costReductionModelId)) {
				return previousState.filter((id) => id !== costReductionModelId)
			}

			return [
				...previousState,
				costReductionModelId,
			]
		})
	}

	const handleSelectClick = () => {
		const {
			openCostReductionModel: { id: openCostReductionModelId },
		} = costReductionModelExplorerCurrentState

		toggleSelectedCostReductionModelId(openCostReductionModelId)
	}

	const handleCloseClick = () => {
		setCostReductionModelExplorerCurrentState((previousState) => ({
			...previousState,
			openCostReductionModel: null,
			openCostReductionModelCategoryId: null,
		}))
	}

	const {
		openCostReductionModel,
		openCostReductionModelCategoryId,
	} = costReductionModelExplorerCurrentState

	const isOpen = !!openCostReductionModel
	const isSelected = openCostReductionModel
		&& selectedCostReductionModelIds.includes(openCostReductionModel.id)

	return (
		<Modal
			open={isOpen}
			onBackdropClick={handleCloseClick}
			className={styles.modal}
		>
			<Box className={styles.box}>
				{isOpen && (
					<div
						key={openCostReductionModel.id}
						className={`
							card
							${cardStyles.card}
							${cardStyles[`deck--${openCostReductionModelCategoryId}`]}
						`}
					>
						<div className={`
							card__header
							${cardStyles.cardHeader}
						`}
						>
							{openCostReductionModel.title}
						</div>
						<div className={`
							card__body
							${cardStyles.cardBody}
						`}
						>
							<div className={cardStyles.cardBodyDetail}>
								{openCostReductionModel.detail}
							</div>
						</div>
						<div className={`
							card__footer
							${cardStyles.cardFooter}
						`}
						>
							<div className="button-set button-set--vertical">
								<button
									className={`
										button
										button--block
										${isSelected ? 'button--success' : 'button--secondary button--outline'}
									`}
									type="button"
									onClick={handleSelectClick}
								>
									{isSelected
										? 'Selected'
										: 'Select this cost reduction model'}
								</button>
								<button
									className="button button--link"
									type="button"
									onClick={handleCloseClick}
								>
									Close
								</button>
							</div>

						</div>
					</div>

				)}
			</Box>
		</Modal>
	)
}

export default CostReductionModelExplorerModal
