/*
	Conforming to line-length requirements for this file, which is essentially an on-disk datastore,
	would be incredibly onerous. Disabling it.
*/
/* eslint-disable max-len */
import React from 'react'
import Link from '@docusaurus/Link'

const costReductionModelsData = {
	inkind: {
		costReductionModels: [
			{
				id: 'inkind--1',
				title: 'Goods in Kind',
				summary: 'Goods in kind are physical assets that are donated for organizations to use for social impact.',
				detail: (
					<p>Goods in kind are physical assets that are donated for organizations to use for social impact. An example is companies and organizations donating hardware, which helps reduce costs in the short term. However, these are usually one-off donations and, therefore, not sustainable by nature. They can potentially cause long-term pain because they paint a false picture of an organization’s true operating costs. To ascertain the likely sustainability of a Goods in Kind gift, make an assessment of how often you would need to purchase the good. E.g. if a business provides your organization with a server, this isn’t something you would buy every week, month or even year, whereas if it a gift is a core part of you service, such as tablets if you are an Education technology solution, it is not likely to be a sustainable solution unless the business is willing to commit to an ongoing supply for free.</p>
				),
			},
			{
				id: 'inkind--2',
				title: 'Services in Kind',
				summary: 'Services in kind are professional services provided by one organization to another without a fee.',
				detail: (
					<>
						<p>Services in kind are professional services provided by one organization to another without a fee. They are services that the business carries out as part of its normal operations, such as logistics, consultancy services, technical assistance, administrative support, and free training.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									In-Kind Services Provided by the Private Sector
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<Link href="https://www.a4id.org">
										Advocates for International Development (A4ID)
									</Link>
									{' '}
									works with an extensive network of legal partners, including leading international law firms, barristers, in-house lawyers, and legal academics, who are willing to provide their expertise free of charge to development and humanitarian organizations. By connecting its legal partners with its development partners, A4ID is able to support projects that are designed to help achieve the Sustainable Development Goals (SDGs).
								</p>
								<p>One of A4ID’s development partners is the Right to Education Initiative, which advocates for nations to legally commit to the implementation of the right to education and works to make quality education accessible to all under national and international law. The initiative works to ensure that governments are held accountable for failing to uphold the right to education and helps empower marginalized communities to claim and enforce their right to education. The organization’s ultimate goal is to enable children and adolescents of all backgrounds to enjoy the power of learning without fear of discrimination.</p>
								<p>The Right to Education Initiative approached A4ID for assistance with researching the national legal frameworks underpinning the right to education among refugees, asylum seekers, and internally displaced persons across 14 jurisdictions (Argentina, Canada, France, Germany, Greece, Italy, Russia, Saudi Arabia, South Africa, Spain, Uganda, Ukraine, Uruguay, and Uzbekistan). A4ID’s legal partners, Allen & Overy, DLA Piper, Onyango & Company Advocates, and Orrick supported this research, helping the initiative identify and share best practices for enforcing the right to education. The law firms worked to compile and analyze national laws, policies, and guidelines concerning the right to education of migrant groups and proposed strategies for their effective enforcement, which were articulated in a background paper for the GEM Report.</p>
								<p className="inline-credit">
									Taken from
									{' '}
									<Link href="https://www.a4id.org/case-studies/analysing-and-enforcing-migrants-rights-to-education/">
										Analysing and Enforcing Migrants’ Rights to Education
									</Link>
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'inkind--3',
				title: 'Time in Kind',
				summary: 'Time in kind is when an external organization or business facilitates the volunteering of its staff.',
				detail: (
					<p>Time in kind is when an external organization or business facilitates the volunteering of its staff. The business or organization itself does not provide the service, rather it takes on the responsibility of organizing its staff members to provide support as a personal endeavor. These volunteers can help with things like coding, technical support, and training. The key to this model is that the external organization or business is carrying out the hard work of identifying and coordinating the volunteer support.</p>
				),
			},
			{
				id: 'inkind--4',
				title: 'Low Bono',
				summary: 'Low bono is when services are provided to an organization at a lower rate or on a cost basis.',
				detail: (
					<p>Low bono is when services are provided to an organization at a lower rate or on a cost basis. Many consulting companies provide significantly lower rates for social impact projects, such as Accenture Development Partners and Thought works. The staff of these companies sometimes take a pay cut in order to work on these projects. The services are best used for specific time-bound projects where you need external specialist support.</p>
				),
			},
		],
	},
	crowdsourcing: {
		costReductionModels: [
			{
				id: 'crowdsourcing--1',
				title: 'Volunteer Communities',
				summary: 'This model involves recruiting, training, and supporting an online or offline community of volunteers.',
				detail: (
					<>
						<p>This model involves recruiting, training, and supporting an online or offline community of volunteers. It often requires you to have at least one person who can act as a community builder in your organization. It also requires a way to effectively manage community contributions and task workflows, such as a backlog of tasks, prioritization, and allocation processes. Key features of strong online communities are:</p>
						<ol>
							<li>
								<b>Membership</b>
								: There should be a sense of belonging and identification, as well as alignment with other community members with similar goals and interests.
							</li>
							<li>
								<b>Influence</b>
								: Members of a community should feel empowered to have influence over what a group does, otherwise there’s no motivation to participate. Group cohesiveness depends on the group having some influence over its members.
							</li>
							<li>
								<b>Integration and fulfillment of needs</b>
								: Members should feel rewarded for their participation and get value from being part of the group.
							</li>
							<li>
								<b>Shared emotional connection</b>
								: Emotional connection is the definitive element for true community. A shared emotional connection comes from quality interactions and bonds that are created. Your emotional connection to a community can be elevated by receiving credit from within the community. It is diminished if you’re embarrassed in front of the community.
								<sup>1</sup>
							</li>
						</ol>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Volunteer Communities
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									The
									{' '}
									<Link href="https://www.hotosm.org/">
										Humanitarian OpenStreetMap Team
									</Link>
									{' '}
									(HOT) is a global community of volunteers, community leaders, and professionals who collaborate to create free, up-to-date maps that help reach people in need. HOT provides map data that revolutionizes disaster management, reduces risks, and contributes to the achievement of the SDGs. This community is structured around a code of conduct, and members participate in the organization’s governance by voting on who will serve on the board of directors.
								</p>
								<p>For example, in places like Fiji and Vanuatu that have experienced significant typhoon damage in recent years, the “view from above” provided by satellites, aircraft, and drones is a crucial resource for disaster risk management—from preparedness to response to recovery.</p>
								<p>
									The Pacific Drone Imagery Dashboard (PacDID) is a platform that leverages the
									{' '}
									<Link href="https://openaerialmap.org">
										OpenAerialMap
									</Link>
									{' '}
									(OAM) concept to make open imagery collected by satellites and drones easily available to humanitarian actors across all Pacific island countries (PICs). OAM has become the reference platform for finding and sharing openly licensed imagery made available for preparing, responding, and recovering from natural disasters.
								</p>
								<p>The PacDID project was formed out of the need to further enable imagery as a resource within Pacific island communities through the Pacific Humanitarian Challenge (PHC) and built on the work of OAM.</p>
								<p>HOT collaborated with disaster specialists at the GeoScience Division of the Pacific Community (SPC) to implement PacDID, a customized version of OAM that addresses the specific needs of working with imagery in the challenging PIC environment. This platform provides some key innovative features:</p>
								<ul>
									<li>A single entry point to finding aerial and satellite imagery that is traditionally stored in disconnected spaces and disparate formats</li>
									<li>An easy mechanism for drone pilots to upload and rapidly share imagery they collected with decision-makers, analysts, and responders</li>
									<li>A visual map tool for DRM managers to request new imagery for specific areas and for drone pilots to coordinate surveys on the ground</li>
									<li>Scalable imagery web services for integration with GIS and mapping software</li>
								</ul>
								<p>OpenAerialMap allows easy access to thousands of openly licensed images that are provided by more than 160 providers. OAM is built using open source software and common data standards that enable fast indexing and interoperability with other mapping systems.</p>
								<p className="inline-credit">
									Taken from
									{' '}
									<Link href="https://www.hotosm.org/projects/uav-imagery-for-disaster-response-pacific-drone-imagery-dashboard-pacdid">
										UAV Imagery for Disaster Response: Pacific Drone Imagery Dashboard
									</Link>
								</p>
							</div>
						</div>
						<div className="footnotes">
							<hr />
							<ol>
								<li>
									Taken from Chavis, D.M., Hogge, J.H., McMillan, D.W., & Wandersman, A. (1986).
									{' '}
									<Link href="https://www.thinkific.com/blog/how-to-build-an-online-community/#how-to-build-an-online-community">
										Sense of community through Brunswick’s lens: A first look.
									</Link>
									{' '}
									<i>Journal of Community Psychology</i>
									, 14(1), 24-40.
								</li>
							</ol>
						</div>
						]
					</>
				),
			},
			{
				id: 'crowdsourcing--2',
				title: 'Platform: Ideas',
				summary: 'Using an ideas crowdsourcing platform, an organization can ask well-defined questions about a problem and pay members of an online community for innovative ideas or solutions.',
				detail: (
					<p>
						Using an ideas crowdsourcing platform, an organization can ask well-defined questions about a problem and pay members of an online community for innovative ideas or solutions. This type of platform could reduce costs by providing organizations with cost-effective solutions that would not have been applied to their work before.
						{' '}
						<Link href="https://www.innocentive.com/">
							InnoCentive
						</Link>
						, which works to find solutions to challenging issues, is an example of a platform for this type of approach.
					</p>
				),
			},
			{
				id: 'crowdsourcing--3',
				title: 'Platform: Freelance Workers',
				summary: 'Platforms for freelance workers allow organizations to hire people with specialized skills on a temporary basis for a reasonable sum.',
				detail: (
					<>
						<p>
							Platforms for freelance workers allow organizations to hire people with specialized skills on a temporary basis for a reasonable sum. This method allows organizations to avoid the costs of hiring a large in-house team of professionals. Freelancing websites include
							{' '}
							<Link href="https://visit.figure-eight.com/People-Powered-Data-Enrichment_T">
								CrowdFlower
							</Link>
							, Samasource,
							{' '}
							<Link href="https://www.mturk.com/">
								Amazon Mechanical Turk
							</Link>
							,
							{' '}
							<Link href="https://www.fiverr.com/">
								Fiverr
							</Link>
							, and
							{' '}
							<Link href="https://www.upwork.com/">
								Upwork
							</Link>
							.
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Platforms for Work
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<Link href="https://www.re-coded.com/">
										Re:Coded
									</Link>
									{' '}
									is an organization that trains disadvantaged young people to code and provides a platform to help them fill opportunities available in the software development market. As an organization it has moved from just providing training, to becoming a platform for businesses seeking to hire coders. It highlights how a core capability (training) can be leveraged to deliver another value proposition (a platform for connecting training graduates to employers) for organizations like yours.
								</p>
							</div>
						</div>
					</>
				),
			},
		],
	},
	lifecycle: {
		costReductionModels: [
			{
				id: 'lifecycle--1',
				title: 'Reuse',
				summary: 'This model involves looking for ways to adapt and enhance existing products, resources, and approaches instead of starting from scratch.',
				detail: (
					<>
						<p>This model involves looking for ways to adapt and enhance existing products, resources, and approaches instead of starting from scratch.</p>
						<p>
							Since the biggest cost in software development is developer time, the more you can reduce developer time the lower your costs will be. One of the ways to do this is to follow the digital principle of
							{' '}
							<Link href="https://digitalprinciples.org/principle/reuse-and-improve/">
								re-use and improve
							</Link>
							{' '}
							and build on free and open source software that already exists. The other is to build or use APIs for other software packages and apps, which will increase interoperability and reduce the functions and features that you have to build for your solution, as they can be leveraged from other solutions.
						</p>
						<p>
							We have used the re-use and improve principle to develop this guide, as it is based upon the original
							{' '}
							<Link href="https://en.wikipedia.org/wiki/Business_Model_Canvas">
								business model canvas
							</Link>
							{' '}
							of Alex Osterwalder and Yves Pigneur.
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Reusing the ODK code
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<Link href="https://www.kobotoolbox.org/">
										Kobo Toolbox
									</Link>
									{' '}
									and
									{' '}
									<Link href="https://smap.com.au/">
										SMAP FieldTask
									</Link>
									{' '}
									are both good examples of reusing the
									{' '}
									<Link href="https://getodk.org/">
										ODK
									</Link>
									{' '}
									code. SMAP FieldTask is proprietary software built on ODK providing data collection, analysis, and visualization. It’s an off-the-shelf system that users are not able to change by themselves. Kobo Toolbox is a product suite built on ODK providing an out-of-the-box product experience, including a form builder, analytic tools, and visualizations. The user can download Kobo and use all or part of the suite, and they can configure it to meet their needs. The user can even take parts of Kobo and integrate it with their own system.
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'lifecycle--2',
				title: 'Revising the Service Model ',
				summary: 'Another way to manage costs is to evaluate your service model and, if needed, rethink its cost effectiveness.',
				detail: (
					<>
						<p>Another way to manage costs is to evaluate your service model and, if needed, rethink its cost effectiveness. Services such as in-person training that may have been affordable when you  began may no longer be practical, feasible, or cost-effective as the organization grows and expands. As demand for a service you offer increases, you’ll have to decide to either continue as is and see costs escalate or revise the way the service is being offered. Revising your model will allow you to offer services in a way that helps your organization grow, scale, and remain sustainable into the future.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Revising the Service Model
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<Link href="https://change-machine.org/">
										Change Machine
									</Link>
									, a national nonprofit founded in 2005, partners with social service agencies working to fight poverty in low-income communities across the United States to sustainably embed financial security services into their existing programs. Change Machine’s proprietary platform amplifies a partner’s impact as they work with customers to achieve their financial goals, connect with a community of practice, and contribute to insights that influence lasting economic change. More than 6,000 practitioners have used the platform and other services to amplify their impact.
								</p>
							</div>
						</div>

					</>
				),
			},
			{
				id: 'lifecycle--3',
				title: 'Outsourcing Activities',
				summary: 'Outsourcing activities such as training and marketing services can help an organization to not only reduce costs, but also promote growth, scalability, and long-term sustainability.',
				detail: (
					<>
						<p>
							Outsourcing activities such as training and marketing services can help an organization to not only reduce costs, but also promote growth, scalability, and long-term sustainability. This might involve renegotiating vendor contracts, switching vendors, or engaging with new partners. It’s important to look over your key activities and determine whether any would be more sustainable and cost effective if performed by another organization. (See
							{' '}
							<Link to="../key-activities">
								Key Activities
							</Link>
							.)
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Outsourcing Inputs and Activities
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									Many digital products start with developing code in house and then moving to outsourcing code development to a specialized software development company.
									{' '}
									<Link href="https://lmms.org">
										LMMS
									</Link>
									{' '}
									has done this, but has always retained the solution architecture function in house.
								</p>
								<p>
									Another example is
									{' '}
									<Link href="https://www.kiva.org">
										Kiva
									</Link>
									{' '}
									and
									{' '}
									<Link href="https://www.globalgiving.org/">
										GlobalGiving
									</Link>
									, which provide a marketing platform for projects and small charities to connect with donors. The project or charity pays a fee for using the platform, allowing it to focus on other aspects of its business model.
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'lifecycle--4',
				title: 'Insourcing Inputs and Activities',
				summary: 'This strategy involves organizations managing input costs by opting to produce (or insource) certain inputs or activities themselves.',
				detail: (
					<>
						<p>
							This strategy involves organizations managing input costs by opting to produce (or insource) certain inputs or activities themselves. An organization might choose to do this if there has been a change in the market that makes it more advantageous or cost effective to insource or if a problem arose from having the activity outsourced, such as quality issues or long lead times. The organization might simply have more available capacity to insource the input or activity or a change in the marketplace might have made insourcing less costly. It is critical to determine which activities to in-source or out-source in your organization (See
							{' '}
							<Link to="../key-activities">
								Key Activities
							</Link>
							.)
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Insourcing Inputs and Activities
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									When World Vision started working with
									{' '}
									<Link href="https://www.dimagi.com/commcare/">
										Dimagi’s CommCare
									</Link>
									, all the training and support was carried out by Dimagi. Over time and as the use of CommCare increased, World Vision selected staff members to be trained in depth by the team at Dimagi so that they could provide training, setup, and level 1 and 2 training support to World Vision project teams. Level 3 support was still done by Dimagi. This was more cost effective and efficient for World Vision. For Dimagi, it freed up some of its in-house customer support team to focus on other customers.
								</p>
							</div>
						</div>
					</>
				),
			},
		],
	},
	partnerships: {
		costReductionModels: [
			{
				id: 'partnerships--1',
				title: 'Getting Others to Pay for Overhead Costs',
				summary: 'Partnering with other organizations, including private-sector companies, to help pay for your operating costs in exchange for branding privileges and/or access to end users.',
				detail: (
					<>
						<p>
							Partnering with other organizations, including private-sector companies, to help pay for your operating costs in exchange for branding privileges and/or access to end users could be greatly beneficial.
							<sup>1</sup>
							{' '}
							This strategy will not only provide your organization with the financial stability needed to ensure long-term sustainability, it may also enhance your reputation integrity. In order for this to work, however, your organization may need to be willing to commit to transparency and provide evidence of impact, perhaps through an SROI approach.
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study:
									Getting Funds to Pay for Overhead Costs
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<Link href="https://www.charitywater.org/our-approach/100-percent-model">
										Charity Water
									</Link>
									{' '}
									is an organization that has adopted the idea of overhead-free donations. It depends mostly on professional philanthropists such as entrepreneurs and business leaders to cover overhead costs through a companion program called The Well. These philanthropists provide financial support but also become teammates, partners, and outspoken champions.
								</p>
								<p>Through its 100% Model, Charity Water promises that all donations go directly toward clean water projects. It highlights the fact that its clean water projects and operations side of the business are different, which helps guarantee that every dollar given as a donation helps bring clean water to people in need. Charity Water tries to be as transparent as possible in letting donors know where it spends their money.</p>
							</div>
						</div>
						<div className="footnotes">
							<hr />
							<ol>
								<li>Beyond Scale, Strategy Module 1, DIAL, 37.</li>
							</ol>
						</div>
					</>
				),
			},
			{
				id: 'partnerships--2',
				title: 'Sharing Resources With Partners',
				summary: 'This model focuses on partnering with similar types of organizations to share cost centers.',
				detail: (
					<p>One way to improve efficiency and reduce costs is to draw on your partners’ strengths and share resources for a wide range of back-end or support services, such as human resources, finance, and marketing. This can also include getting free or subsidized space by sharing with another organization. This model focuses on partnering with similar types of organizations to share cost centers and can help you adapt your operations, extend your capabilities, and reduce costs.</p>
				),
			},
		],
	},
}

export default costReductionModelsData
