import React, { useCallback } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableFooter,
	TableRow,
	TableCell,
} from '@mui/material'
import styles from '../Tools.modules.css'
import { replaceKeyedObject } from '../../../utils'
import InnerTableAlertRow from '../InnerTableAlertRow'
import networkStakeholdersAtom from '../../../recoil/data/networkStakeholders'
import ValueNetworkEditorRow from './ValueNetworkEditorRow'
import ValueNetworkEditorFooter from './ValueNetworkEditorFooter'
import { generateNetworkStakeholder } from './utils'

function Codification() {
	const networkStakeholders = useRecoilValue(networkStakeholdersAtom)
	const setNetworkStakeholders = useSetRecoilState(networkStakeholdersAtom)

	const updateNetworkStakeholder = useCallback((networkStakeholder) => {
		setNetworkStakeholders((previousNetworkStakeholders) => (
			replaceKeyedObject(networkStakeholder, previousNetworkStakeholders)
		))
	}, [])

	const removeNetworkStakeholder = useCallback((networkStakeholder) => {
		setNetworkStakeholders((previousNetworkStakeholders) => (
			previousNetworkStakeholders.filter(
				(item) => item.key !== networkStakeholder.key,
			)
		))
	}, [])

	const addNetworkStakeholder = useCallback(() => {
		const newNetworkStakeholder = generateNetworkStakeholder()
		setNetworkStakeholders((previousNetworkStakeholders) => ([
			...previousNetworkStakeholders,
			newNetworkStakeholder,
		]))
	}, [])

	const hasNetworkStakeholders = networkStakeholders.length > 0

	// TODO: Build out Download PDF functionality (#129)
	// const showDownloadButton = (hasNetworkStakeholders)
	const showDownloadButton = false

	return (
		<div className={styles.interactive}>
			<TableContainer>
				<Table aria-label="Value Network Tool">
					<TableHead>
						<TableRow className={styles.header}>
							<TableCell component="th">
								Stakeholder
							</TableCell>
							<TableCell component="th">
								Key Information
							</TableCell>
							<TableCell component="th">
								Give
							</TableCell>
							<TableCell component="th">
								Get
							</TableCell>
							<TableCell component="th">
								Network
							</TableCell>
							<TableCell component="th">
								Actions
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{hasNetworkStakeholders
							? networkStakeholders.map((networkStakeholder) => (
								<ValueNetworkEditorRow
									key={networkStakeholder.key}
									networkStakeholder={networkStakeholder}
									updateNetworkStakeholder={updateNetworkStakeholder}
									removeNetworkStakeholder={removeNetworkStakeholder}
								/>
							)) : (
								<InnerTableAlertRow colSpan={6}>
									It looks like you don’t have any stakeholders!
									Click “Add another stakeholder” below.
								</InnerTableAlertRow>
							)}
					</TableBody>
					<TableFooter>
						<TableRow>
							<TableCell colSpan={6}>
								<ValueNetworkEditorFooter
									addNetworkStakeholder={addNetworkStakeholder}
									showDownloadButton={showDownloadButton}
								/>
							</TableCell>
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
			<p className="inline-credit">© Ian Gray</p>
		</div>
	)
}

export default Codification
