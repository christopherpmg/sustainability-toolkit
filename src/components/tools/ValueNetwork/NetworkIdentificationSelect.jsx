import React from 'react'
import {
	Select,
	MenuItem,
} from '@mui/material'
import { NETWORK_IDENTIFICATION } from '../../../constants'

function NetworkIdentificationSelect(props) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props
	// so that this component can be treated identically to the MUI Select component.
	return (
		<Select
			{...props}
			fullWidth
		>
			<MenuItem value={NETWORK_IDENTIFICATION.IVN}>
				IVN (Inner)
			</MenuItem>
			<MenuItem value={NETWORK_IDENTIFICATION.OVN}>
				OVN (Outer)
			</MenuItem>
		</Select>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}

export default NetworkIdentificationSelect
