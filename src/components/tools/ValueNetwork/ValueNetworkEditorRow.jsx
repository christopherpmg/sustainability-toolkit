import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	IconButton,
	TextField,
} from '@mui/material'
import {
	XCircleIcon,
} from '@heroicons/react/outline'
import { networkStakeholderType } from '../../../types'
import styles from '../Tools.modules.css'
import NetworkIdentificationSelect from './NetworkIdentificationSelect'

function ValueNetworkEditorRow({
	networkStakeholder,
	updateNetworkStakeholder,
	removeNetworkStakeholder,
}) {
	const handleLabelChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			label: event.target.value,
		})
	}

	const handleKeyInformationChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			keyInformation: event.target.value,
		})
	}

	const handleGiveChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			give: event.target.value,
		})
	}

	const handleGetChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			get: event.target.value,
		})
	}

	const handleNetworkIdentificationChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			networkIdentification: event.target.value,
		})
	}

	const handleActionsChange = (event) => {
		updateNetworkStakeholder({
			...networkStakeholder,
			actions: event.target.value,
		})
	}

	const handleRemoveClick = () => {
		// This is only temporary until we add a proper MUI Dialog confirmation
		// eslint-disable-next-line no-alert
		if (window.confirm('Are you sure you want to delete this stakeholder?')) {
			removeNetworkStakeholder(networkStakeholder)
		}
	}

	return (
		<TableRow>
			<TableCell>
				<TextField
					aria-label="Stakeholder"
					value={networkStakeholder.label}
					onChange={handleLabelChange}
				/>
			</TableCell>
			<TableCell>
				<TextField
					aria-label="Key Information"
					value={networkStakeholder.keyInformation}
					onChange={handleKeyInformationChange}
					multiline
				/>
			</TableCell>
			<TableCell>
				<TextField
					aria-label="Give"
					value={networkStakeholder.give}
					onChange={handleGiveChange}
					multiline
				/>
			</TableCell>
			<TableCell>
				<TextField
					aria-label="Get"
					value={networkStakeholder.get}
					onChange={handleGetChange}
					multiline
				/>
			</TableCell>
			<TableCell>
				<NetworkIdentificationSelect
					aria-label="Network"
					value={networkStakeholder.networkIdentification}
					onChange={handleNetworkIdentificationChange}
				/>
			</TableCell>
			<TableCell>
				<TextField
					aria-label="Actions"
					value={networkStakeholder.actions}
					onChange={handleActionsChange}
					multiline
				/>
			</TableCell>
			<TableCell>
				<IconButton
					className={styles.remove}
					aria-label="Delete stakeholder"
					onClick={handleRemoveClick}
					size="small"
				>
					<XCircleIcon width="24" />
				</IconButton>
			</TableCell>
		</TableRow>
	)
}

ValueNetworkEditorRow.propTypes = {
	networkStakeholder: networkStakeholderType.isRequired,
	updateNetworkStakeholder: PropTypes.func.isRequired,
	removeNetworkStakeholder: PropTypes.func.isRequired,
}

export default React.memo(ValueNetworkEditorRow)
