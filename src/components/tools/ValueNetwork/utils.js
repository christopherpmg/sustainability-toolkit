import { v4 as uuidv4 } from 'uuid'

export function generateNetworkStakeholder() {
	return {
		key: uuidv4(),
		label: '',
		keyInformation: '',
		give: '',
		get: '',
		networkIdentification: '',
		actions: '',
	}
}

export function generateNetworkStakeholders(count) {
	return Array.from(Array(count), () => generateNetworkStakeholder())
}
