/* eslint-disable import/prefer-default-export */
import { v4 as uuidv4 } from 'uuid'
import {
	SEGMENT_TYPE,
	SEGMENT_DESIRABIILTY,
} from '../../../constants'

export function generateValueProposition(segmentType = '') {
	return {
		key: uuidv4(),
		label: '',
		userSegmentationDesirability: segmentType === SEGMENT_TYPE.USERS
			? SEGMENT_DESIRABIILTY.FIVE
			: '',
		buyerSegmentationDesirability: segmentType === SEGMENT_TYPE.BUYERS
			? SEGMENT_DESIRABIILTY.FIVE
			: '',
		targetImpactSegmentationDesirability: segmentType === SEGMENT_TYPE.TARGET_IMPACT
			? SEGMENT_DESIRABIILTY.FIVE
			: '',
	}
}

export function generateValuePropositions(count, segmentType = '') {
	return Array.from(
		{ length: count },
		() => generateValueProposition(segmentType),
	)
}

export function getSegmentTypeLabel(segmentCategory) {
	switch (segmentCategory) {
	case SEGMENT_TYPE.USERS:
		return 'Users'
	case SEGMENT_TYPE.BUYERS:
		return 'Buyers'
	case SEGMENT_TYPE.TARGET_IMPACT:
		return 'Target Impact'
	default:
		return ''
	}
}
