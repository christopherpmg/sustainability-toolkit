import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
} from '@mui/material'
import valuePropositionsAtom from '../../../recoil/data/valuePropositions'
import styles from '../Tools.modules.css'
import { SEGMENT_TYPE } from '../../../constants'
import ButiSegmentationRowSet from './ButiSegmentationRowSet'

function ButiSegmentation() {
	const valuePropositions = useRecoilValue(valuePropositionsAtom)
	const setValuePropositions = useSetRecoilState(valuePropositionsAtom)

	const updateValuePropositions = (newValuePropositions) => {
		setValuePropositions(newValuePropositions)
	}

	return (
		<div className={styles.interactive}>
			<TableContainer>
				<Table aria-label="BUTI Segmentation Tool">
					<TableHead>
						<TableRow>
							<TableCell
								component="th"
								colSpan={2}
							/>
							<TableCell
								component="th"
								colSpan={3}
								className={styles.superHeaderCell}
							>
								<div className={styles.superHeaderCellInner}>
									Desirability to Segment
								</div>
							</TableCell>
						</TableRow>
						<TableRow className={styles.header}>
							<TableCell component="th" />
							<TableCell component="th">
								Jobs To Be Done (JTBD)
							</TableCell>
							<TableCell component="th" className={styles.hasSuperHeader}>
								User
							</TableCell>
							<TableCell component="th" className={styles.hasSuperHeader}>
								Buyer
							</TableCell>
							<TableCell component="th" className={styles.hasSuperHeader}>
								Target Impact
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<ButiSegmentationRowSet
							key={SEGMENT_TYPE.USERS}
							segmentType={SEGMENT_TYPE.USERS}
							updateValuePropositions={updateValuePropositions}
							valuePropositions={valuePropositions}
						/>
						<ButiSegmentationRowSet
							key={SEGMENT_TYPE.BUYERS}
							segmentType={SEGMENT_TYPE.BUYERS}
							updateValuePropositions={updateValuePropositions}
							valuePropositions={valuePropositions}
						/>
						<ButiSegmentationRowSet
							key={SEGMENT_TYPE.TARGET_IMPACT}
							segmentType={SEGMENT_TYPE.TARGET_IMPACT}
							updateValuePropositions={updateValuePropositions}
							valuePropositions={valuePropositions}
						/>
					</TableBody>
				</Table>
			</TableContainer>
			<p className="inline-credit">© Ian Gray</p>
		</div>
	)
}

export default ButiSegmentation
