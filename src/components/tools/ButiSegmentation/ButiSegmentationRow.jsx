import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	TextField,
} from '@mui/material'
import { valuePropositionType } from '../../../types'
import { SEGMENT_TYPE } from '../../../constants'
import SegmentDesirabilitySelect from './SegmentDesirabilitySelect'

function ButiSegmentationRow({
	segmentType,
	valueProposition,
	updateValueProposition,
}) {
	const handleLabelChange = (event) => {
		updateValueProposition({
			...valueProposition,
			label: event.target.value,
		})
	}

	const handleUserSegmentationDesirabilityChange = (event) => {
		updateValueProposition({
			...valueProposition,
			userSegmentationDesirability: event.target.value,
		})
	}

	const handleBuyerSegmentationDesirabilityChange = (event) => {
		updateValueProposition({
			...valueProposition,
			buyerSegmentationDesirability: event.target.value,
		})
	}

	const handleTargetImpactSegmentationDesirabilityChange = (event) => {
		updateValueProposition({
			...valueProposition,
			targetImpactSegmentationDesirability: event.target.value,
		})
	}

	return (
		<TableRow>
			<TableCell>
				<TextField
					aria-label="Value Proposition"
					value={valueProposition.label}
					onChange={handleLabelChange}
				/>
			</TableCell>
			<TableCell width="80px">
				<SegmentDesirabilitySelect
					aria-label="User Segmentation Desirability"
					value={valueProposition.userSegmentationDesirability}
					onChange={handleUserSegmentationDesirabilityChange}
					disabled={segmentType === SEGMENT_TYPE.USERS}
				/>
			</TableCell>
			<TableCell width="80px">
				<SegmentDesirabilitySelect
					aria-label="Buyer Segmentation Desirability"
					value={valueProposition.buyerSegmentationDesirability}
					onChange={handleBuyerSegmentationDesirabilityChange}
					disabled={segmentType === SEGMENT_TYPE.BUYERS}
				/>
			</TableCell>
			<TableCell width="80px">
				<SegmentDesirabilitySelect
					aria-label="Target Impact Segmentation Desirability"
					value={valueProposition.targetImpactSegmentationDesirability}
					onChange={handleTargetImpactSegmentationDesirabilityChange}
					disabled={segmentType === SEGMENT_TYPE.TARGET_IMPACT}
				/>
			</TableCell>
		</TableRow>
	)
}

ButiSegmentationRow.propTypes = {
	segmentType: PropTypes.string.isRequired,
	valueProposition: valuePropositionType.isRequired,
	updateValueProposition: PropTypes.func.isRequired,
}

export default ButiSegmentationRow
