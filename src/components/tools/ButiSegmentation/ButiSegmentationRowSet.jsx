import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
} from '@mui/material'
import { valuePropositionsType } from '../../../types'
import { SEGMENT_TYPE } from '../../../constants'
import { replaceKeyedObject } from '../../../utils'
import toolStyles from '../Tools.modules.css'
import ButiSegmentationRow from './ButiSegmentationRow'
import styles from './ButiSegmentation.modules.css'
import { getSegmentTypeLabel } from './utils'

function getValuePropositionsBySegmentType(valuePropositions, segmentType) {
	switch (segmentType) {
	case SEGMENT_TYPE.USERS:
		return valuePropositions.users
	case SEGMENT_TYPE.BUYERS:
		return valuePropositions.buyers
	case SEGMENT_TYPE.TARGET_IMPACT:
		return valuePropositions.targetImpact
	default:
		return []
	}
}

function ButiSegmentationRowSet({
	segmentType,
	valuePropositions,
	updateValuePropositions,
}) {
	const updateValueProposition = (valueProposition) => {
		const newValuePropositions = {
			...valuePropositions,
		}
		switch (segmentType) {
		case SEGMENT_TYPE.USERS:
			newValuePropositions.users = replaceKeyedObject(
				valueProposition,
				valuePropositions.users,
			)
			break
		case SEGMENT_TYPE.BUYERS:
			newValuePropositions.buyers = replaceKeyedObject(
				valueProposition,
				valuePropositions.buyers,
			)
			break
		case SEGMENT_TYPE.TARGET_IMPACT:
			newValuePropositions.targetImpact = replaceKeyedObject(
				valueProposition,
				valuePropositions.targetImpact,
			)
			break
		default:
		}

		updateValuePropositions(newValuePropositions)
	}

	const filteredValuePropositions = getValuePropositionsBySegmentType(
		valuePropositions,
		segmentType,
	)

	return (
		<>
			<TableRow>
				<TableCell
					component="th"
					scope="row"
					rowSpan={filteredValuePropositions.length + 1}
					className={`
						${toolStyles.rotatedCell}
						${styles.segmentTypeLabelCell}
						segment-background-color--${segmentType}
					`}
				>
					<div className={toolStyles.rotatedCellInner}>
						{getSegmentTypeLabel(segmentType)}
					</div>
				</TableCell>
			</TableRow>
			{filteredValuePropositions.map((valueProposition) => (
				<ButiSegmentationRow
					key={valueProposition.key}
					segmentType={segmentType}
					valueProposition={valueProposition}
					updateValueProposition={updateValueProposition}
				/>
			))}
		</>
	)
}

ButiSegmentationRowSet.propTypes = {
	segmentType: PropTypes.string.isRequired,
	valuePropositions: valuePropositionsType.isRequired,
	updateValuePropositions: PropTypes.func.isRequired,
}

export default ButiSegmentationRowSet
