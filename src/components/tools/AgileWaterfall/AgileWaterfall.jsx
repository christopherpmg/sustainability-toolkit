import React, { useCallback } from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import {
	TableContainer,
	Table,
} from '@mui/material'
import toolStyles from '../Tools.modules.css'
import { withAgileWaterfallFilters } from '../../../recoil/data/networkStakeholders'
import agileEvaluationsAtom, {
	withAgileWaterfallStructure,
} from '../../../recoil/data/agileEvaluations'
import AgileWaterfallBody from './AgileWaterfallBody'
import AgileWaterfallHead from './AgileWaterfallHead'
import AgileWaterfallFoot from './AgileWaterfallFoot'
import styles from './AgileWaterfall.module.css'

function getAgileEvaluationHash(agileEvaluation) {
	return `${agileEvaluation.evaluationCategory}_${agileEvaluation.networkStakeholderKey}`
}

function AgileWaterfall() {
	const networkStakeholders = useRecoilValue(withAgileWaterfallFilters)
	const agileEvaluations = useRecoilValue(withAgileWaterfallStructure)
	const setAgileEvaluationsHashMap = useSetRecoilState(agileEvaluationsAtom)

	const updateAgileEvaluation = useCallback((
		evaluationCategory,
		networkStakeholderKey,
		evaluation,
	) => {
		const agileEvaluation = {
			evaluationCategory,
			networkStakeholderKey,
			evaluation,
		}
		setAgileEvaluationsHashMap((previousAgileEvaluationsHashMap) => {
			const newAgileEvaluationsHashMap = {
				...previousAgileEvaluationsHashMap,
			}
			const agileEvaluationHash = getAgileEvaluationHash(agileEvaluation)
			newAgileEvaluationsHashMap[agileEvaluationHash] = agileEvaluation

			return newAgileEvaluationsHashMap
		})
	}, [])

	return (
		<div className={toolStyles.interactive}>
			<TableContainer>
				<Table
					aria-label="Agile / Waterfall Tool"
					className={styles.agileWaterfallTable}
				>
					<AgileWaterfallHead
						networkStakeholders={networkStakeholders}
					/>
					<AgileWaterfallBody
						networkStakeholders={networkStakeholders}
						agileEvaluations={agileEvaluations}
						updateAgileEvaluation={updateAgileEvaluation}
					/>
					<AgileWaterfallFoot
						agileEvaluations={agileEvaluations}
					/>
				</Table>
			</TableContainer>
			<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>
		</div>
	)
}

export default AgileWaterfall
