import React from 'react'
import PropTypes from 'prop-types'
import {
	TableHead,
	TableRow,
	TableCell,
} from '@mui/material'
import { networkStakeholderType } from '../../../types'

function AgileWaterfallHead({
	networkStakeholders,
}) {
	return (
		<TableHead>
			<TableRow>
				<TableCell component="th" />
				<TableCell component="th">
					Your Organization
				</TableCell>
				{ networkStakeholders.map((networkStakeholder) => (
					<TableCell
						key={networkStakeholder.key}
						component="th"
					>
						{ networkStakeholder.label }
					</TableCell>
				))}
			</TableRow>
		</TableHead>
	)
}

AgileWaterfallHead.propTypes = {
	networkStakeholders: PropTypes.arrayOf(
		networkStakeholderType,
	).isRequired,
}

export default AgileWaterfallHead
