/*
	Conforming to line-length requirements for this file, which is essentially an on-disk datastore,
	would be incredibly onerous. Disabling it.
*/
/* eslint-disable max-len */
import React from 'react'

const agileWaterfallData = {
	evaluationLabels: {
		projectPlanning: (
			<>
				<b>Project Planning:</b>
				{' '}
				We use agile project planning methods, such as sprints.
			</>
		),
		projectActivityManagement: (
			<>
				<b>Project Activity Management:</b>
				{' '}
				We use agile project management methods, such as Kanban.
			</>
		),
		accounting: (
			<>
				<b>Accounting:</b>
				{' '}
				We use agile budgeting and reporting methods, such as product, rather than activity budgeting.
			</>
		),
		iteration: (
			<>
				<b>Iteration:</b>
				{' '}
				We use agile methods of engaging our customer segments, such as user centered design, throughout the project lifecycle and iterate our approach based on feedback.
			</>
		),
		experimentation: (
			<>
				<b>Experimentation:</b>
				{' '}
				We use agile methods of assumption and hypothesis testing as a fundamental way of testing our assumptions in our projects.
			</>
		),
		change: (
			<>
				<b>Change:</b>
				{' '}
				We monitor our context regularly and the project team are empowered to make project decisions and changes based upon their assessment of the context without waiting for a hierarchical decision making process.
			</>
		),
	},
	evaluationAriaLabels: {
		projectPlanning: 'Project Planning',
		projectActivityManagement: 'Project Activity Management',
		accounting: 'Accounting',
		iteration: 'Iteration',
		experimentation: 'Experimentation',
		change: 'Change',
	},
}

export default agileWaterfallData
