import React from 'react'
import {
	Select,
	MenuItem,
} from '@mui/material'
import { AGILE_EVALUATION_LEVEL } from '../../../constants'

function getLabelForValue(value) {
	switch (value) {
	case AGILE_EVALUATION_LEVEL.ONE:
		return '1'
	case AGILE_EVALUATION_LEVEL.TWO:
		return '2'
	case AGILE_EVALUATION_LEVEL.THREE:
		return '3'
	case AGILE_EVALUATION_LEVEL.FOUR:
		return '4'
	case AGILE_EVALUATION_LEVEL.FIVE:
		return '5'
	default:
		return ''
	}
}

function AutomationLevelSelect(props) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props
	// so that this component can be treated identically to the MUI Select component.
	return (
		<Select
			{...props}
			renderValue={getLabelForValue}
		>
			<MenuItem value={AGILE_EVALUATION_LEVEL.ONE}>
				1 – You use all traditional methods for your work
			</MenuItem>
			<MenuItem value={AGILE_EVALUATION_LEVEL.TWO}>
				2
			</MenuItem>
			<MenuItem value={AGILE_EVALUATION_LEVEL.THREE}>
				3 – You use a balanced mix of agile and traditional
			</MenuItem>
			<MenuItem value={AGILE_EVALUATION_LEVEL.FOUR}>
				4
			</MenuItem>
			<MenuItem value={AGILE_EVALUATION_LEVEL.FIVE}>
				5 – You use agile methods for all your work
			</MenuItem>
		</Select>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}

export default AutomationLevelSelect
