import React from 'react'
import PropTypes from 'prop-types'
import {
	TableFooter,
	TableRow,
	TableCell,
} from '@mui/material'
import { AGILE_EVALUATION_LEVEL } from '../../../constants'
import styles from './AgileWaterfall.module.css'

function getEvaluationScore(evaluationLevel) {
	switch (evaluationLevel) {
	case AGILE_EVALUATION_LEVEL.ONE:
		return 1
	case AGILE_EVALUATION_LEVEL.TWO:
		return 2
	case AGILE_EVALUATION_LEVEL.THREE:
		return 3
	case AGILE_EVALUATION_LEVEL.FOUR:
		return 4
	case AGILE_EVALUATION_LEVEL.FIVE:
		return 5
	default:
		return 0
	}
}

function isEvaluationCompleteForStakeholder(agileEvaluations, stakeholderIndex) {
	return agileEvaluations.reduce(
		(isComplete, agileEvaluationForOneQuestion) => isComplete
			&& agileEvaluationForOneQuestion[stakeholderIndex] !== '',
		true,
	)
}

function convertGapToAlignmentLevel(gap) {
	if (gap <= 5) {
		return 'high'
	}

	if (gap <= 15) {
		return 'medium'
	}

	return 'low'
}

function AgileWaterfallFoot({
	agileEvaluations,
}) {
	const evaluationScores = new Array(agileEvaluations[0].length).fill(0)
	agileEvaluations.forEach((agileEvaluationForOneQuestion) => {
		agileEvaluationForOneQuestion.forEach((evaluationLevel, stakeholderIndex) => {
			evaluationScores[stakeholderIndex] += getEvaluationScore(evaluationLevel)
		})
	})
	const baseScore = evaluationScores[0]

	return (
		<TableFooter>
			<TableRow>
				<TableCell component="th">
					<b>Gap between you and your partner:</b>
				</TableCell>
				{
					evaluationScores.map((evaluationScore, index) => {
						// The index is all we have here.
						/* eslint-disable react/no-array-index-key */
						if (index === 0) {
							return (
								<TableCell key={index} />
							)
						}

						if (!isEvaluationCompleteForStakeholder(agileEvaluations, 0)) {
							return (<TableCell key={index} />)
						}

						if (!isEvaluationCompleteForStakeholder(agileEvaluations, index)) {
							return (<TableCell key={index}>…</TableCell>)
						}

						const evaluationGap = Math.abs(baseScore - evaluationScore)

						return (
							<TableCell
								key={index}
								className={styles[`alignment--${convertGapToAlignmentLevel(evaluationGap)}`]}
							>
								{ evaluationGap }
							</TableCell>
						)
						/* eslint-enable react/no-array-index-key */
					})
				}
			</TableRow>
		</TableFooter>
	)
}

AgileWaterfallFoot.propTypes = {
	agileEvaluations: PropTypes.arrayOf(
		PropTypes.arrayOf(PropTypes.string),
	).isRequired,
}

export default AgileWaterfallFoot
