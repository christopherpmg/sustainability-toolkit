import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	TextField,
} from '@mui/material'
import TextareaAutosize from 'react-textarea-autosize'
import { ArrowSmRightIcon } from '@heroicons/react/solid'
import { productComponentType } from '../../../types'
import styles from '../Tools.modules.css'
import AutomationLevelSelect from './AutomationLevelSelect'

function CodificationEditorRow({
	productComponent,
	updateProductComponent,
}) {
	const handleCurrentAutomationLevelChange = (event) => {
		updateProductComponent({
			...productComponent,
			currentAutomationLevel: event.target.value,
		})
	}

	const handleTargetAutomationLevelChange = (event) => {
		updateProductComponent({
			...productComponent,
			targetAutomationLevel: event.target.value,
		})
	}

	const handleAutomationLevelTimelineChange = (event) => {
		updateProductComponent({
			...productComponent,
			automationLevelTimeline: event.target.value,
		})
	}

	const handleActionsChange = (event) => {
		updateProductComponent({
			...productComponent,
			actions: event.target.value,
		})
	}

	return (
		<TableRow>
			<TableCell
				component="th"
				scope="row"
			>
				{ productComponent.id }
			</TableCell>
			<TableCell>
				<div className={styles.static__title}>{productComponent.name}</div>
				<div className={styles.static__subtitle}>
					{productComponent.now || '(none)'}
					<ArrowSmRightIcon className="svg--inline" />
					{productComponent.future || '(none)'}
				</div>
			</TableCell>
			<TableCell>
				<AutomationLevelSelect
					value={productComponent.currentAutomationLevel}
					onChange={handleCurrentAutomationLevelChange}
					aria-label="Current automation level"
				/>
			</TableCell>
			<TableCell>
				<AutomationLevelSelect
					value={productComponent.targetAutomationLevel}
					onChange={handleTargetAutomationLevelChange}
					aria-label="Target automation level"
				/>
			</TableCell>
			<TableCell>
				<TextField
					value={productComponent.automationLevelTimeline}
					onChange={handleAutomationLevelTimelineChange}
					aria-label="By when?"
				/>
			</TableCell>
			<TableCell>
				<TextareaAutosize
					value={productComponent.actions}
					onChange={handleActionsChange}
					aria-label="Actions to make change"
				/>
			</TableCell>
		</TableRow>
	)
}

CodificationEditorRow.propTypes = {
	productComponent: productComponentType.isRequired,
	updateProductComponent: PropTypes.func.isRequired,
}

export default CodificationEditorRow
