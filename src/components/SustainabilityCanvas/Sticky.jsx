import React from 'react'
import PropTypes from 'prop-types'
import { PlusIcon } from '@heroicons/react/outline'
import { useSetRecoilState } from 'recoil'
import currentCanvasStateAtom from '../../recoil/data/currentCanvasState'
import styles from './Sticky.module.css'

function Sticky({
	body, segment, block, order, variant,
}) {
	const setCurrentCanvasState = useSetRecoilState(currentCanvasStateAtom)

	const isAddSticky = variant === 'add'

	const clickHandler = (e) => {
		if (e.type === 'click' || e.key === 'Enter') {
			e.preventDefault()

			// Set the current sticky as selected
			setCurrentCanvasState({
				selectedBlockSlug: block,
				selectedStickyOrder: order,
				isInEditMode: true,
			})
		}
	}

	return (
		<div
			role="button"
			tabIndex={0}
			className={styles.stickyWrapper}
			onClick={clickHandler}
			onKeyDown={clickHandler}
		>
			<div
				className={`
					${styles.sticky}
					${styles.fold}
					${segment ? styles[`segment--${segment || 'none'}`] : ''}
					${isAddSticky && styles.addSticky}
				`}
			>
				{isAddSticky && <PlusIcon className={styles.addIcon} />}
				{body}
			</div>
		</div>
	)
}

Sticky.propTypes = {
	body: PropTypes.string.isRequired,
	segment: PropTypes.string,
	block: PropTypes.string.isRequired,
	order: PropTypes.number,
	variant: PropTypes.string,
}

Sticky.defaultProps = {
	segment: null,
	order: null,
	variant: 'default',
}

export default Sticky
