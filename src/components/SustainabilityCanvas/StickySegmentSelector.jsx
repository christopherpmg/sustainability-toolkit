import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
	Radio,
	RadioGroup,
	FormControlLabel,
	FormControl,
	FormLabel,
} from '@mui/material'
import { useRadioGroup } from '@mui/material/RadioGroup'
import stickyStyles from './Sticky.module.css'
import styles from './StickySegmentSelector.module.css'

const UncheckedIcon = 'span'

const CheckedIcon = UncheckedIcon

// Inspired by blueprintjs
function CustomRadioButton(props) {
	const { value } = props

	return (
		<Radio
			sx={{
				'&:hover': {
					bgcolor: 'transparent',
				},
			}}
			className={styles.radio}
			disableRipple
			checkedIcon={(
				<CheckedIcon
					className={`
						${styles.icon}
						${styles.checkedIcon}
						${stickyStyles[`segment--${value}`]}
						${stickyStyles.fold}
					`}
				/>
			)}
			icon={(
				<UncheckedIcon
					className={`
						${styles.icon}
						${stickyStyles[`segment--${value}`]}
						${stickyStyles.fold}
					`}
				/>
			)}
			// Allow prop spreading here because this is a stylizedversion of
			// radio control that should retain support for all standard props.
			// eslint-disable-next-line react/jsx-props-no-spreading
			{...props}
		/>
	)
}

CustomRadioButton.propTypes = {
	value: PropTypes.string,
}

CustomRadioButton.defaultProps = {
	value: 'default',
}

function RadioControlLabel(props) {
	const radioGroup = useRadioGroup()

	const { value } = props

	let checked = false

	if (radioGroup) {
		checked = radioGroup.value === value
	}

	return (
		<FormControlLabel
			control={<CustomRadioButton value={value} />}
			checked={checked}
			// Allow prop spreading here because this is a stylizedversion of
			// radio control that should retain support for all standard props.
			// eslint-disable-next-line react/jsx-props-no-spreading
			{...props}
		/>
	)
}

RadioControlLabel.propTypes = {
	value: PropTypes.string,
}

RadioControlLabel.defaultProps = {
	value: 'default',
}

function StickySegmentSelector({ selection, onChange }) {
	const [selected, setSelected] = useState(selection || 'default')

	const changeHandler = (e) => {
		const newValue = e.target.value
		setSelected(newValue)
		if (onChange) {
			onChange(e)
		}
	}

	return (
		<FormControl component="fieldset">
			<FormLabel component="legend">
				<strong>Optional:</strong>
				{' '}
				Classify by BUTI segment
			</FormLabel>
			<RadioGroup
				row
				defaultValue="default"
				aria-label="BUTI Segment"
				name="customized-radios"
				value={selected}
				onChange={changeHandler}
				className={styles.radioGroup}
			>
				<RadioControlLabel
					value="default"
					label="None"
				/>
				<RadioControlLabel
					value="buyer"
					label="Buyers"
				/>
				<RadioControlLabel
					value="user"
					label="Users"
				/>
				<RadioControlLabel
					value="target"
					label="Target impact"
				/>
			</RadioGroup>
		</FormControl>
	)
}

StickySegmentSelector.propTypes = {
	selection: PropTypes.string,
	onChange: PropTypes.func,
}

StickySegmentSelector.defaultProps = {
	selection: 'default',
	onChange: null,
}

export default StickySegmentSelector
