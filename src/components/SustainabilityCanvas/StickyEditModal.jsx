import React, { useState, useEffect } from 'react'
import {
	useRecoilValueLoadable,
	useResetRecoilState,
	useSetRecoilState,
} from 'recoil'
import {
	Box,
	Modal,
} from '@mui/material'
import currentCanvasStateAtom, {
	withSelectedStickyDetails,
} from '../../recoil/data/currentCanvasState'
import stickiesAtom from '../../recoil/data/stickies'
import styles from './StickyEditModal.module.css'
import StickyEditField from './StickyEditField'
import StickySegmentSelector from './StickySegmentSelector'

function StickyEditModal() {
	const [open, setOpen] = useState(false)
	const [currSticky, setCurrSticky] = useState(null)
	const [isNew, setIsNew] = useState(false)
	const [currCanvasState, setCurrCanvasState] = useState(null)

	const resetCurrentCanvasState = useResetRecoilState(currentCanvasStateAtom)

	const setStickies = useSetRecoilState(stickiesAtom)

	const handleClose = () => {
		resetCurrentCanvasState()
		setOpen(false)
	}

	const currCanvasStateLoadable = useRecoilValueLoadable(currentCanvasStateAtom)

	const currStickyLoadable = useRecoilValueLoadable(withSelectedStickyDetails())

	useEffect(() => {
		if (currCanvasStateLoadable.state === 'hasValue') {
			setCurrCanvasState(currCanvasStateLoadable.contents)
			setOpen(currCanvasStateLoadable.contents && !!currCanvasStateLoadable.contents.isInEditMode)
		}
	}, [currCanvasStateLoadable])

	useEffect(() => {
		if (currStickyLoadable.state === 'hasValue') {
			const { contents } = currStickyLoadable
			setCurrSticky(contents)
			setIsNew(contents && contents.body === '')
		}
	}, [currStickyLoadable])

	const textChangeHandler = (e) => {
		setCurrSticky({
			...currSticky,
			body: e.target.value,
		})
	}

	const SegmentChangeHandler = (e) => {
		setCurrSticky({
			...currSticky,
			segment: e.target.value,
		})
	}

	const deleteHandler = () => {
		// Browser-native alerts and confirmations have a user-hostile UX, so we should replace them
		// with an on-page dialog (TODO: #66). However, it's less user-hostile than *no* confirmation.
		// eslint-disable-next-line no-alert
		if (window.confirm('Are you sure you want to delete this sticky?')) {
			setStickies((currentStickies) => {
				const newValue = { ...currentStickies }

				const stickiesInBlock = (currentStickies[currCanvasState.selectedBlockSlug] || [])
				const allOtherStickiesInBlock = stickiesInBlock.filter((s) => s.order !== currSticky.order)

				newValue[currCanvasState.selectedBlockSlug] = [
					...allOtherStickiesInBlock,
				]

				return newValue
			})
			handleClose()
		}
	}

	const saveHandler = () => {
		setStickies((currentStickies) => {
			const newValue = { ...currentStickies }

			const stickiesInBlock = (currentStickies[currCanvasState.selectedBlockSlug] || [])
			const allOtherStickiesInBlock = stickiesInBlock.filter((s) => s.order !== currSticky.order)

			newValue[currCanvasState.selectedBlockSlug] = [
				...allOtherStickiesInBlock,
				{
					body: currSticky.body,
					order: currSticky.order,
					segment: currSticky.segment,
				},
			]

			return newValue
		})
		handleClose()
	}

	return (
		<div>
			{open
			&& currSticky
			&& (
				<Modal
					open={open}
					onClose={handleClose}
					style={{ overflow: 'scroll' }}
				>
					<Box className={styles.stickyModal}>
						<h2>{currSticky && currSticky.blockName}</h2>

						<StickyEditField
							value={currSticky ? currSticky.body : ''}
							segment={currSticky.segment}
							onChange={textChangeHandler}
						/>

						<StickySegmentSelector
							selection={currSticky && currSticky.segment}
							onChange={SegmentChangeHandler}
						/>

						<div className={styles.buttons}>
							<button
								type="submit"
								className={`
							button
							button--primary
							${styles.alignCenter}
							${currSticky.body.length === 0 ? 'disabled' : ''}
						`}
								onClick={saveHandler}
							>
								{isNew ? 'Add to your canvas' : 'Update Sticky'}
							</button>
							<button
								type="button"
								className={`
							button
							button--link
							${styles.alignCenter}
							${styles.linkButton}
						`}
								onClick={handleClose}
							>
								{isNew ? 'Discard and close' : 'Close without changing'}
							</button>
							{!isNew && (
								<button
									type="button"
									className={`
							button
							button--link
							${styles.alignCenter}
							${styles.linkButton}
							${styles.deleteButton}
						`}
									onClick={deleteHandler}
								>
									Delete Sticky
								</button>
							)}
						</div>

						{/* TODO: Add building-block-specific guidance (#121). */}
					</Box>
				</Modal>
			)}
		</div>
	)
}

export default StickyEditModal
