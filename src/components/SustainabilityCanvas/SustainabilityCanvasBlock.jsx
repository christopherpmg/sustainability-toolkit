import React, {
	useEffect,
	useState,
} from 'react'
import PropTypes from 'prop-types'
import { useRecoilValueLoadable } from 'recoil'
import { withSortingByBlock } from '../../recoil/data/stickies'
import { NEW_STICKY_TEXT } from '../../constants'
import Sticky from './Sticky'
import styles from './SustainabilityCanvasBlock.module.css'

function SustainabilityCanvasBlock({ displayName, slug }) {
	const [sortedStickies, setSortedStickies] = useState([])

	const sortedStickiesLoadable = useRecoilValueLoadable(withSortingByBlock(slug))

	useEffect(() => {
		if (sortedStickiesLoadable.state === 'hasValue') {
			setSortedStickies(sortedStickiesLoadable.contents)
		}
	}, [sortedStickiesLoadable])

	const showExistingStickiesWrapper = sortedStickies.length > 0

	return (
		<div
			className={`
				${styles.block}
				${styles[`block--${slug}`]}
			`}
			key={slug}
		>
			<div
				className={styles.label}
				key="label"
			>
				{displayName}
			</div>
			{showExistingStickiesWrapper ? (
				<div
					className={styles.stickiesWrapper}
					key="existingStickiesWrapper"
				>
					{sortedStickies.map((sticky) => (
						<Sticky
							key={`${slug}${sticky.order}`}
							body={sticky.body}
							segment={sticky.segment}
							order={sticky.order}
							block={slug}
						/>
					))}
				</div>
			) : null}
			<div
				className={styles.stickiesWrapper}
				key="addStickiesWrapper"
			>
				<Sticky
					key={`add-${slug}`}
					body={NEW_STICKY_TEXT}
					block={slug}
					variant="add"
				/>
			</div>
		</div>
	)
}

SustainabilityCanvasBlock.propTypes = {
	displayName: PropTypes.string.isRequired,
	slug: PropTypes.string.isRequired,
}

export default SustainabilityCanvasBlock
