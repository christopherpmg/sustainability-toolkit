import React from 'react'
import PropTypes from 'prop-types'
import styles from './AccountCard.module.css'

function AccountCard({
	handleLogoutClick,
	authToken,
	authUsername,
}) {
	const authTokenPreview = authToken.substring(0, 10)

	return (
		<div className={`
			card
			shadow--md
			${styles.accountCard}
		`}
		>
			<div className="card__header">
				<h2>
					You are logged in to the Toolkit.
				</h2>
			</div>
			<div className="card__body">
				{authUsername
					? (
						<p>
							You’re logged in with the username
							{' '}
							<code>
								{authUsername}
							</code>
							.
						</p>
					)
					: (
						<p>
							You’re logged in with the token
							{' '}
							<code>
								{authTokenPreview}
								…
							</code>
						</p>
					)}
				<button
					type="button"
					onClick={handleLogoutClick}
					className={`
					button
					button--secondary
					button--outline
					button--block
				`}
				>
					Log out
				</button>
			</div>
		</div>

	)
}

AccountCard.propTypes = {
	handleLogoutClick: PropTypes.func.isRequired,
	authToken: PropTypes.string.isRequired,
	authUsername: PropTypes.string,
}

AccountCard.defaultProps = {
	authUsername: null,
}

export default AccountCard
