export const ACTIVITY_CHANNEL = {
	ONLINE: 'online',
	CONFERENCE: 'conference',
	CONVENING: 'convening',
	WORD_OF_MOUTH: 'wordOfMouth',
	PARTNERSHIPS: 'partnerships',
	PROCUREMENT_PROCESSES: 'procurementProcesses',
}
