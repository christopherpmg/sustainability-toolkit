export const AGILE_EVALUATION_CATEGORY = {
	PROJECT_PLANNING: 'projectPlanning',
	PROJECT_ACTIVITY_MANAGEMENT: 'projectActivityManagement',
	ACCOUNTING: 'accounting',
	ITERATION: 'iteration',
	EXPERIMENTATION: 'experimentation',
	CHANGE: 'change',
}
