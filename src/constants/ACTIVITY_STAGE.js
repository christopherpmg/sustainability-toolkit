export const ACTIVITY_STAGE = {
	AWARENESS: 'awareness',
	EVALUATION: 'evaluation',
	PURCHASE: 'purchase',
	DELIVERY: 'delivery',
	AFTER_SALES: 'afterSales',
}
