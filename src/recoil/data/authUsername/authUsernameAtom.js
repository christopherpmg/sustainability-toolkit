import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'

const ATOM_KEY = 'authUsername'

const authUsernameAtom = atom({
	key: ATOM_KEY,
	default: null,
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default authUsernameAtom
