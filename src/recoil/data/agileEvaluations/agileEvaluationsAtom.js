import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'

const ATOM_KEY = 'agileEvaluations'

const agileEvaluationsAtom = atom({
	key: ATOM_KEY,
	default: {},
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default agileEvaluationsAtom
