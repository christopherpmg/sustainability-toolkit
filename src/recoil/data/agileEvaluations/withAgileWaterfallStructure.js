import { selector } from 'recoil'
import {
	AGILE_EVALUATION_CATEGORY_DISPLAY_ORDER,
	SELF_NETWORK_STAKEHOLDER_KEY,
} from '../../../constants'
import { generateGridOfValue } from '../../../utils'
import withAgileWaterfallFilters from '../networkStakeholders/withAgileWaterfallFilters'
import agileEvaluationsAtom from './agileEvaluationsAtom'

function generateAgileWaterfallStructure(
	agileEvaluationsHashMap,
	networkStakeholderKeys,
) {
	const structuredSet = generateGridOfValue(
		networkStakeholderKeys.length + 1,
		AGILE_EVALUATION_CATEGORY_DISPLAY_ORDER.length,
		'',
	)

	Object.entries(agileEvaluationsHashMap).forEach(
		(entry) => {
			const agileEvaluation = entry[1]
			const {
				evaluationCategory,
				networkStakeholderKey,
				evaluation,
			} = agileEvaluation
			const evaluationCategoryIndex = AGILE_EVALUATION_CATEGORY_DISPLAY_ORDER.indexOf(
				evaluationCategory,
			)

			const networkStakeholderIndex = (networkStakeholderKey === SELF_NETWORK_STAKEHOLDER_KEY)
				? 0
				: networkStakeholderKeys.indexOf(networkStakeholderKey)

			const networkStakeholderIndexOffset = (networkStakeholderKey === SELF_NETWORK_STAKEHOLDER_KEY)
				? 0
				: 1

			if (evaluationCategoryIndex !== -1 && networkStakeholderIndex !== -1) {
				const resolvedNetworkStakeholderIndex = networkStakeholderIndex
					+ networkStakeholderIndexOffset
				structuredSet[evaluationCategoryIndex][resolvedNetworkStakeholderIndex] = evaluation || ''
			}
		},
	)

	return structuredSet
}

const SELECTOR_KEY = 'agileEvaluationsWithAgileWaterfallStructure'

const withAgileWaterfallStructure = selector({
	key: SELECTOR_KEY,
	get: ({ get }) => {
		const agileEvaluationsHashMap = get(agileEvaluationsAtom)
		const networkStakeholders = get(withAgileWaterfallFilters)
		const networkStakeholderKeys = networkStakeholders.map(
			(networkStakeholder) => networkStakeholder.key,
		)

		return generateAgileWaterfallStructure(
			agileEvaluationsHashMap,
			networkStakeholderKeys,
		)
	},
})

export default withAgileWaterfallStructure
