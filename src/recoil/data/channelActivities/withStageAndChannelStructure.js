import { selector } from 'recoil'
import {
	ACTIVITY_CHANNEL_DISPLAY_ORDER,
	ACTIVITY_STAGE_DISPLAY_ORDER,
} from '../../../constants'
import { generateGridOfValue } from '../../../utils'
import channelActivitiesAtom from './channelActivitiesAtom'

function generateStageAndChannelStructure(channelActivitiesHashMap) {
	const structuredSet = generateGridOfValue(
		ACTIVITY_CHANNEL_DISPLAY_ORDER.length,
		ACTIVITY_STAGE_DISPLAY_ORDER.length,
		'',
	)

	Object.entries(channelActivitiesHashMap).forEach(
		(entry) => {
			const channelActivity = entry[1]
			const {
				stage,
				channel,
				activity,
			} = channelActivity
			const channelIndex = ACTIVITY_CHANNEL_DISPLAY_ORDER.indexOf(channel)
			const stageIndex = ACTIVITY_STAGE_DISPLAY_ORDER.indexOf(stage)
			if (channelIndex !== -1 && stageIndex !== -1) {
				structuredSet[stageIndex][channelIndex] = activity
			}
		},
	)

	return structuredSet
}

const SELECTOR_KEY = 'channelActivitiesWithStageAndChannelStructure'

const withStageAndChannelStructure = selector({
	key: SELECTOR_KEY,
	get: ({ get }) => {
		const channelActivitiesHashMap = get(channelActivitiesAtom)

		return generateStageAndChannelStructure(
			channelActivitiesHashMap,
		)
	},
})

export default withStageAndChannelStructure
