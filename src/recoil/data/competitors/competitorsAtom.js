import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { DEFAULT_COMPETITORS_COUNT } from '../../../constants'
import { generateCompetitors } from '../../../components/tools/ComponentComparison/utils'

const ATOM_KEY = 'competitors'

const competitorsAtom = atom({
	key: ATOM_KEY,
	default: generateCompetitors(DEFAULT_COMPETITORS_COUNT),
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default competitorsAtom
