import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { DEFAULT_STAKEHOLDER_COUNT } from '../../../constants'
import { generateStakeholders } from '../../../components/tools/StakeholderMatrix/utils'

const ATOM_KEY = 'stakeholders'

const stakeholdersAtom = atom({
	key: ATOM_KEY,
	default: generateStakeholders(DEFAULT_STAKEHOLDER_COUNT),
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default stakeholdersAtom
