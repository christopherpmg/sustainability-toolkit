import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'

const ATOM_KEY = 'authToken'

const authTokenAtom = atom({
	key: ATOM_KEY,
	default: null,
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default authTokenAtom
