import { selectorFamily } from 'recoil'
import productComponentsAtom from './productComponentsAtom'

const SELECTOR_KEY = 'productComponentsThroughNowFilter'

const throughNowFilter = selectorFamily({
	key: SELECTOR_KEY,
	get: (nowTypes) => ({ get }) => {
		const productComponents = get(productComponentsAtom)

		return productComponents.filter(
			(productComponent) => nowTypes.includes(productComponent.now),
		)
	},
})

export default throughNowFilter
