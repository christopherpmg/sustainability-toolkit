import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { generateValuePropositions } from '../../../components/tools/ButiSegmentation/utils'
import {
	DEFAULT_VALUE_PROPOSITIONS_PER_SEGMENT,
	SEGMENT_TYPE,
} from '../../../constants'

const ATOM_KEY = 'butiValuePropositions'

const valuePropositionsAtom = atom({
	key: ATOM_KEY,
	default: {
		users: generateValuePropositions(
			DEFAULT_VALUE_PROPOSITIONS_PER_SEGMENT,
			SEGMENT_TYPE.USERS,
		),
		buyers: generateValuePropositions(
			DEFAULT_VALUE_PROPOSITIONS_PER_SEGMENT,
			SEGMENT_TYPE.BUYERS,
		),
		targetImpact: generateValuePropositions(
			DEFAULT_VALUE_PROPOSITIONS_PER_SEGMENT,
			SEGMENT_TYPE.TARGET_IMPACT,
		),
	},
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default valuePropositionsAtom
