import { atom } from 'recoil'

const ATOM_KEY = 'revenueModelExplorerCurrentState'

const revenueModelExplorerCurrentStateAtom = atom({
	key: ATOM_KEY,
	default: {
		openRevenueModel: null,
		openRevenueModelCategoryId: null,
	},
})

export default revenueModelExplorerCurrentStateAtom
