import { selectorFamily } from 'recoil'
import {
	createSticky,
	getBlockNameBySlug,
} from '../../../utils'
import {
	withBlockNameBySlugAndOrder,
	withMaxOrderByBlock,
} from '../stickies'
import { DEFAULT_NEW_STICKY } from '../../../constants'
import currentCanvasStateAtom from './currentCanvasStateAtom'

const SELECTOR_KEY = 'currentSticky'

const withSelectedStickyDetails = selectorFamily({
	key: SELECTOR_KEY,
	get: () => async ({ get }) => {
		const { selectedBlockSlug, selectedStickyOrder } = get(currentCanvasStateAtom)

		// Invalid sticky condition
		if (!selectedBlockSlug) {
			return Promise.resolve(null)
		}

		// New sticky. i.e. slug is provided but no order
		// Addiional condition to prevent false flagging 0th order stickies
		if (selectedStickyOrder !== 0 && !selectedStickyOrder) {
			const maxOrder = get(withMaxOrderByBlock(selectedBlockSlug))

			return Promise.resolve({
				...createSticky(
					DEFAULT_NEW_STICKY.body,
					DEFAULT_NEW_STICKY.segment,
					maxOrder + 1,
				),
				blockName: getBlockNameBySlug(selectedBlockSlug),
			})
		}

		// Existing sticky
		const sticky = get(withBlockNameBySlugAndOrder({
			blockSlug: selectedBlockSlug,
			order: selectedStickyOrder,
		}))

		return Promise.resolve(sticky)
	},
})

export default withSelectedStickyDetails
