import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'

const ATOM_KEY = 'currCanvasState'

const currentCanvasStateAtom = atom({
	key: ATOM_KEY,
	default: {
		selectedBlockSlug: null,
		selectedStickyOrder: null,
		isOpen: false,
	},
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default currentCanvasStateAtom
