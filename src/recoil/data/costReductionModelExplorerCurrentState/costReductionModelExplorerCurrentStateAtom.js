import { atom } from 'recoil'

const ATOM_KEY = 'costReductionModelExplorerCurrentState'

const costReductionModelExplorerCurrentStateAtom = atom({
	key: ATOM_KEY,
	default: {
		openCostReductionModel: null,
		openCostReductionCategoryId: null,
	},
})

export default costReductionModelExplorerCurrentStateAtom
