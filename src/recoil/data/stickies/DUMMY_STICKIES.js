// TODO: Delete file once API is wired up
const DUMMY_STICKIES = {
	'value-proposition': [
		{
			body: 'Online Education app for Refugees',
			segment: null,
			order: 0,
		},
		{
			body: 'Personalized development through a customizable course builder',
			segment: null,
			order: 1,
		},
	],
	'customer-segments': [
		{
			body: 'Buyers - INGOs and Local NGOs',
			segment: 'buyer',
			order: 0,
		},
		{
			body: 'Users - High School teachers',
			segment: 'user',
			order: 1,
		},
		{
			body: 'Target Impact Group - 16-18 year old Refugees',
			segment: 'target',
			order: 2,
		},
	],
	channels: [
		{
			body: 'Unicef and UNHCR (to reach local NGOs)',
			segment: null,
			order: 0,
		},
		{
			body: 'Education conferences (pre-purchase marketing)',
			segment: null,
			order: 1,
		},
	],
	'customer-relationships': [
		{
			body: 'Buyers - close partnerships',
			segment: 'buyer',
			order: 0,
		},
		{
			body: 'Users - online training and support',
			segment: 'user',
			order: 1,
		},
		{
			body: 'Target Impact Group - mediated by partners, but feedback function in the app',
			segment: 'target',
			order: 2,
		},
	],
	'revenue-streams': [
		{
			body: 'Sub-grants from implementing partners',
			segment: null,
			order: 0,
		},
		{
			body: 'SAAS hosting charges',
			segment: null,
			order: 1,
		},
		{
			body: 'Training and deployment support services',
			segment: null,
			order: 2,
		},
	],
	'key-activities': [
		{
			body: 'IVN partner - Software development',
			segment: null,
			order: 0,
		},
		{
			body: 'Curriculum development (with Ministry of Education)',
			segment: null,
			order: 1,
		},
		{
			body: 'Training',
			segment: null,
			order: 2,
		},
		{
			body: 'IVN partner - Technical support',
			segment: null,
			order: 3,
		},
	],
	'key-resources': [
		{
			body: 'Us - Trainers',
			segment: null,
			order: 0,
		},
		{
			body: 'Us - Partner managers',
			segment: null,
			order: 1,
		},
		{
			body: 'Us - code',
			segment: null,
			order: 2,
		},
		{
			body: 'IVN partner - Coders and technical support team',
			segment: null,
			order: 3,
		},
	],
	'key-partnerships': [
		{
			body: 'UNICEF',
			segment: null,
			order: 0,
		},
		{
			body: 'UNHCR',
			segment: null,
			order: 1,
		},
		{
			body: 'Software development partner',
			segment: null,
			order: 2,
		},
	],
	'cost-structure': [
		{
			body: 'Coding',
			segment: null,
			order: 0,
		},
		{
			body: 'Travel for training',
			segment: null,
			order: 1,
		},
		{
			body: 'Office and equipment',
			segment: null,
			order: 2,
		},
	],
	'organizational-development': [
		{
			body: 'Our Values of Openness and Courage',
			segment: null,
			order: 0,
		},
		{
			body: 'Moving to a Social Business model',
			segment: null,
			order: 1,
		},
		{
			body: 'Recruiting an Advisory board',
			segment: null,
			order: 2,
		},
	],
	'end-game': [
		{
			body: 'Government adoption in urban host community settings',
			segment: null,
			order: 0,
		},
		{
			body: 'Transition to UNHCR, UNICEF, INGO partnership for refugee camps with our ongoing training and support.',
			segment: null,
			order: 1,
		},
	],
}

export default DUMMY_STICKIES
