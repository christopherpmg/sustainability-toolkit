import { selectorFamily } from 'recoil'
import stickiesAtom from './stickiesAtom'

const SELECTOR_KEY = 'stickiesSortedForBlock'

const withSortingByBlock = selectorFamily({
	key: SELECTOR_KEY,
	get: (blockSlug) => async ({ get }) => {
		const stickies = get(stickiesAtom)
		const stickiesInBlock = stickies[blockSlug] ? [...stickies[blockSlug]] : []
		const sortedStickies = stickiesInBlock
			.sort((s1, s2) => s1.order - s2.order)

		return Promise.resolve(sortedStickies)
	},
})

export default withSortingByBlock
