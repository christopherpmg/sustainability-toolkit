import { selectorFamily } from 'recoil'
import { getBlockNameBySlug } from '../../../utils'
import stickiesAtom from './stickiesAtom'

const SELECTOR_KEY = 'stickyWithBlockNameBySlugAndOrder'

const withBlockNameBySlugAndOrder = selectorFamily({
	key: SELECTOR_KEY,
	get: ({ blockSlug, order }) => async ({ get }) => {
		const stickies = get(stickiesAtom)
		const stickiesInBlock = [...stickies[blockSlug]] || []
		const sticky = stickiesInBlock.filter((s) => s.order === order)

		const blockName = getBlockNameBySlug(blockSlug)

		return Promise.resolve({
			blockName,
			...sticky[0],
		})
	},
})

export default withBlockNameBySlugAndOrder
