import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import { SESSION_STATE } from '../../../constants'

const ATOM_KEY = 'sessionState'

const sessionStateAtom = atom({
	key: ATOM_KEY,
	default: SESSION_STATE.LOGGED_OUT,
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default sessionStateAtom
