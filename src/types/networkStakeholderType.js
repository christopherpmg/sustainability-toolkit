import PropTypes from 'prop-types'

export const networkStakeholderType = PropTypes.shape({
	key: PropTypes.string,
	label: PropTypes.string,
	keyInformation: PropTypes.string,
	give: PropTypes.string,
	get: PropTypes.string,
	networkIdentification: PropTypes.string,
	actions: PropTypes.string,
})
