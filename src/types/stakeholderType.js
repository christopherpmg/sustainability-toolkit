import PropTypes from 'prop-types'

export const stakeholderType = PropTypes.shape({
	key: PropTypes.string,
	fullName: PropTypes.string,
	important: PropTypes.string,
	role: PropTypes.string,
	impact: PropTypes.string,
	influence: PropTypes.string,
	objections: PropTypes.string,
	enablers: PropTypes.string,
	engagementActivities: PropTypes.string,
	contact: PropTypes.string,
})
