import PropTypes from 'prop-types'
import { valuePropositionType } from './valuePropositionType'

export const valuePropositionsType = PropTypes.shape({
	users: PropTypes.arrayOf(valuePropositionType),
	buyers: PropTypes.arrayOf(valuePropositionType),
	targetImpact: PropTypes.arrayOf(valuePropositionType),
})
