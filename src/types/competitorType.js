import PropTypes from 'prop-types'

export const competitorType = PropTypes.shape({
	key: PropTypes.string,
	label: PropTypes.string,
	componentComparisons: PropTypes.objectOf(PropTypes.string),
})
