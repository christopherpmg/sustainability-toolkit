import PropTypes from 'prop-types'

export const costReductionModelType = PropTypes.shape({
	id: PropTypes.string,
	title: PropTypes.string,
	summary: PropTypes.string,
	detail: PropTypes.node,
})
