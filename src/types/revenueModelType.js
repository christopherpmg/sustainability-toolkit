import PropTypes from 'prop-types'

export const revenueModelType = PropTypes.shape({
	id: PropTypes.string,
	title: PropTypes.string,
	summary: PropTypes.string,
	detail: PropTypes.node,
})
