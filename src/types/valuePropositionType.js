import PropTypes from 'prop-types'

export const valuePropositionType = PropTypes.shape({
	key: PropTypes.string,
	label: PropTypes.string,
	userSegmentationDesirability: PropTypes.string,
	buyerSegmentationDesirability: PropTypes.string,
	targetImpactSegmentationDesirability: PropTypes.string,
})
