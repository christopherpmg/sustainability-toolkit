#  Matomo plugin for Docusaurus

This directory contains a customized version of the Matomo plugin for Docusaurus. The original plugin is at https://github.com/karser/docusaurus-plugin-matomo and the original author is Dmitrii Poddubnyi (https://github.com/karser).

This version has been customized by Justin Reese (https://github.com/reefdog) with Bad Idea Factory on behalf of the Digital Impact Alliance for its Business Model Sustainability Toolkit project (https://gitlab.com/dial/principles/sustainability-toolkit). These customizations include expanded configuration options and a syntax compatible with our ESLint configuration, as well as different configuration defaults for some options.

These changes will be offered back to the original author as a pull request, but in the interest of time we are including the plugin directly in our project. Please note that this plugin is governed by a different [license](./LICENSE) than the overall Sustainability Toolkit project.
