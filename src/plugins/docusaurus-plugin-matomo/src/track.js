import ExecutionEnvironment from '@docusaurus/ExecutionEnvironment'

function matomoTrackRouteUpdates() {
	if (!ExecutionEnvironment.canUseDOM) {
		return null
	}

	return {
		onRouteUpdate({ location }) {
			// `_paq` is defined by the Matomo plugin, so we need to live with it.
			// eslint-disable-next-line no-underscore-dangle
			const paq = window._paq || []
			paq.push(['setCustomUrl', location.pathname])
			paq.push(['setDocumentTitle', document.title])
			paq.push(['trackPageView'])
		},
	}
}

export default (matomoTrackRouteUpdates())
