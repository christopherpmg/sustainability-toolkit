const path = require('path')

module.exports = function matomoTracking(context) {
	const {
		siteConfig: {
			themeConfig: {
				matomo: matomoConfig,
			},
		},
	} = context

	if (!matomoConfig) {
		throw new Error('Please specify a `matomo` object in `themeConfig` with `matomoUrl` and `siteId` properties to use docusaurus-plugin-matomo')
	}

	const isProd = process.env.NODE_ENV === 'production'

	const defaultConfig = {
		matomoUrl: null,
		siteId: null,
		cookieDomain: '',
		domains: [],
		doNotTrack: false,
		enabled: isProd, // By default, only enable tracking in production environments.
		enableHeartBeatTimer: true,
		enableLinkTracking: true,
		jsLoader: 'matomo.js',
		phpLoader: 'matomo.php',
		requestMethod: 'POST', // Set to `false` to use Matomo library default (GET).
		trackPageView: true,
	}

	// Construct the actual config object by seeding it first with the default config options, then
	// with the user-defined config options (which override the defaults).
	const config = {
		...defaultConfig,
		...matomoConfig,
	}

	if (!config.matomoUrl) {
		throw new Error('Please specify the `matomoUrl` property in the `themeConfig.matomo` object.')
	}

	if (!config.siteId) {
		throw new Error('Please specify the `siteId` property in the `themeConfig.matomo` object.')
	}

	if (!Array.isArray(config.domains)) {
		throw new Error('`themeConfig.matomo.domains` must be an array or be omitted.')
	}

	// Handle it when users don't enter the status domain with a trailing slash.
	if (config.matomoUrl.charAt(config.matomoUrl.length - 1) !== '/') {
		config.matomoUrl += '/'
	}

	return {
		name: 'docusaurus-plugin-matomo',

		getClientModules() {
			if (!config.enabled) {
				return []
			}

			return [
				path.resolve(__dirname, './track'),
			]
		},

		injectHtmlTags() {
			if (!config.enabled) {
				return {}
			}

			return {
				headTags: [
					{
						tagName: 'link',
						attributes: {
							rel: 'preconnect',
							href: `${config.matomoUrl}`,
						},
					},
					{
						tagName: 'script',
						innerHTML: `
							var _paq = window._paq = window._paq || [];
							${config.cookieDomain ? `_paq.push(['setCookieDomain', '${config.cookieDomain}']);` : ''}
							${config.domains.length > 0 ? `_paq.push(['setDomains', ['${config.domains.join("','")}']]);` : ''}
							${config.doNotTrack ? `_paq.push(['setDoNotTrack', ${config.doNotTrack}]);` : ''}
							${config.enableHeartBeatTimer ? "_paq.push(['enableHeartBeatTimer']);" : ''}
							${config.enableLinkTracking ? "_paq.push(['enableLinkTracking']);" : ''}
							${config.requestMethod ? `_paq.push(['setRequestMethod', '${config.requestMethod}']);` : ''}
							${config.trackPageView ? "_paq.push(['trackPageView']);" : ''}
							(function() {
								var u="${config.matomoUrl}";
								${config.requestMethod ? `_paq.push(['setRequestMethod', '${config.requestMethod}']);` : ''}
								_paq.push(['setTrackerUrl', u+'${config.phpLoader}']);
								_paq.push(['setSiteId', '${config.siteId}']);
								var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
								g.type='text/javascript'; g.async=true; g.src=u+'${config.jsLoader}'; s.parentNode.insertBefore(g,s);
							})();
						`,
					},
				],
			}
		},
	}
}
