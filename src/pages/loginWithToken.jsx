import React, { useEffect } from 'react'
import { useSetRecoilState } from 'recoil'
import { toast } from 'react-toastify'
import { Redirect } from '@docusaurus/router'
import { SESSION_STATE } from '../constants'
import { getParamFromUrl } from '../utils'
import authTokenAtom from '../recoil/data/authToken'
import authUsernameAtom from '../recoil/data/authUsername'
import sessionStateAtom from '../recoil/data/sessionState'

export default function LoginWithToken() {
	const setAuthToken = useSetRecoilState(authTokenAtom)
	const setAuthUsername = useSetRecoilState(authUsernameAtom)
	const setSessionState = useSetRecoilState(sessionStateAtom)

	const id = getParamFromUrl('id')
	const username = getParamFromUrl('username')
	toast.dismiss('logout')

	useEffect(() => {
		setSessionState(SESSION_STATE.LOGGING_IN)

		if (id) {
			setAuthToken(id)
			setAuthUsername(username)
		} else {
			toast.error('We were not able to log you in.', {
				toastId: 'login',
				autoClose: false,
			})
		}
	})

	return <Redirect to="/account" />
}
