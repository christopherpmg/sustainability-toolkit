import React, { useEffect } from 'react'

export default function Auth() {
	useEffect(() => {
		window.location.href = '/api/auth/auth?callbackPath=/loginWithToken'
	}, [])

	return (
		<div />
	)
}
