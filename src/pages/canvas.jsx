import React from 'react'
import Layout from '@theme/Layout'
import SustainabilityCanvas from '../components/SustainabilityCanvas'

export default function Canvas() {
	return (
		<Layout title="Canvas">
			<main>
				<SustainabilityCanvas />
			</main>
		</Layout>
	)
}
