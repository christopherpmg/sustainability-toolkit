import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import { toast } from 'react-toastify'
import Layout from '@theme/Layout'
import { SESSION_STATE } from '../constants'
import LoginCard from '../components/LoginCard'
import AccountCard from '../components/AccountCard'
import authTokenAtom from '../recoil/data/authToken'
import authUsernameAtom from '../recoil/data/authUsername'
import sessionStateAtom from '../recoil/data/sessionState'

export default function Account() {
	const authToken = useRecoilValue(authTokenAtom)
	const authUsername = useRecoilValue(authUsernameAtom)
	const setSessionState = useSetRecoilState(sessionStateAtom)

	const handleLogoutClick = () => {
		setSessionState(SESSION_STATE.LOGGING_OUT)

		toast.dismiss()
		toast('You’ve been logged out.', {
			toastId: 'logout',
			autoClose: 3000,
		})
		window.location.href = '/api/auth/deauth'
	}

	return (
		<Layout
			title="Account"
			description="Log in to or manage your Business Model Sustainability Toolkit account."
		>
			<main>
				{authToken ? (
					<AccountCard
						authToken={authToken}
						authUsername={authUsername}
						handleLogoutClick={handleLogoutClick}
					/>
				) : (
					<LoginCard />
				)}
			</main>
		</Layout>
	)
}
