// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

require('dotenv').config()

const lightCodeTheme = require('prism-react-renderer/themes/github')
const darkCodeTheme = require('prism-react-renderer/themes/dracula')

/** @type {import('@docusaurus/types').Config} */
const config = {
	title: 'The Business Model Sustainability Toolkit',
	tagline: 'Guidance, tools, and case studies to help digital product/service owners and advisors in the aid sector design sustainable business models. Powered by Build for Sustainability – one of the Principles for Digital Development.',
	url: process.env.DOMAIN || 'http://localhost',
	baseUrl: '/',
	onBrokenLinks: 'throw',
	onBrokenMarkdownLinks: 'warn',
	favicon: 'img/favicon.ico',
	organizationName: 'dial',
	projectName: 'sustainability-toolkit',

	customFields: {
		nodeEnv: process.env.NODE_ENV || 'development',
	},

	plugins: [
		process.env.MATOMO_ENABLED === 'true' ? require.resolve('./src/plugins/docusaurus-plugin-matomo/src') : false,
	],

	presets: [
		[
			'@docusaurus/preset-classic',
			/** @type {import('@docusaurus/preset-classic').Options} */
			({
				docs: {
					routeBasePath: '/',
					sidebarPath: require.resolve('./sidebars.js'),
				},
				theme: {
					customCss: [
						require.resolve('react-toastify/dist/ReactToastify.min.css'),
						require.resolve('./src/css/custom.css'),
					],
				},
				gtag: (process.env.GTAG_TRACKING_ID ? {
					trackingID: process.env.GTAG_TRACKING_ID || '',
					anonymizeIP: true,
				} : false),
			}),
		],
	],

	themeConfig:
		/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
		({
			hideableSidebar: true,
			colorMode: {
				disableSwitch: true,
			},
			navbar: {
				title: 'Business Model Sustainability Toolkit',
				style: 'dark',
				items: [
					{
						type: 'doc',
						docId: 'guide/introduction',
						position: 'right',
						label: 'Guide',
						className: 'toolkit__navbar__item toolkit__navbar__item--has-icon toolkit__navbar__item--guide',
					},
					{
						label: 'Canvas',
						to: 'canvas',
						position: 'right',
						className: 'toolkit__navbar__item toolkit__navbar__item--has-icon toolkit__navbar__item--canvas',
					},
					{
						label: 'Account',
						to: 'account',
						position: 'right',
						className: 'toolkit__navbar__item toolkit__navbar__item--has-icon toolkit__navbar__item--user',
					},
				],
			},
			footer: {
				style: 'dark',
				links: [
					// NB: Custom footer styling in custom.css depends on the specific order of these columns.
					//     If you change the column order here, adjust `.footer__col:nth-child()` accordingly.
					{
						title: 'Guide',
						items: [
							{
								label: 'Introduction',
								to: 'guide',
							},
							{
								label: 'Value Proposition',
								to: 'guide/value-proposition',
							},
							{
								label: 'Customer Segments',
								to: 'guide/customer-segments',
							},
							{
								label: 'Channels',
								to: 'guide/channels',
							},
							{
								label: 'Customer Relationships',
								to: 'guide/customer-relationships',
							},
							{
								label: 'Revenue Streams',
								to: 'guide/revenue-streams',
							},
							{
								label: 'Key Activities',
								to: 'guide/key-activities',
							},
						],
					},
					{
						title: 'Guide (cont.)',
						items: [
							{
								label: 'Key Resources',
								to: 'guide/key-resources',
							},
							{
								label: 'Key Partnerships',
								to: 'guide/key-partnerships',
							},
							{
								label: 'Cost Structure',
								to: 'guide/cost-structure',
							},
							{
								label: 'Organizational Development',
								to: 'guide/organizational-development',
							},
							{
								label: 'End Game',
								to: 'guide/end-game',
							},
							{
								label: 'Glossary',
								to: 'guide/glossary',
							},
							{
								label: 'Contribute to the Guide',
								href: 'https://gitlab.com/dial/principles/sustainability-toolkit/-/tree/main/docs/guide',
							},
						],
					},
					{
						title: 'Toolkit',
						items: [
							{
								label: 'Canvas',
								to: 'canvas',
							},
							{
								label: 'Acknowledgements',
								to: 'acknowledgements',
							},
							{
								label: 'Email the team',
								href: 'mailto:info@digitalimpactalliance.org',
							},
							{
								label: 'Privacy Policy',
								href: 'https://digitalprinciples.org/privacy-policy/',
							},
							{
								label: 'Contribute to the Toolkit',
								href: 'https://gitlab.com/dial/principles/sustainability-toolkit/',
							},
						],
					},
					{
						title: 'Partners',
						items: [
							{
								html: `
									<div class="footer__partner-logo footer__partner-logo--pdd">
										<div class="footer__partner-logo-label">Powered by the</div>
										<a href="https://digitalprinciples.org/" class="footer__partner-logo-link" target="_blank">
											<img class="footer__partner-logo-img" src="/img/pdd-logo-white.png" width="206" height="39" alt="Principles for Digital Development logo">
										</a>
									</div>
									<div class="footer__partner-logo footer__partner-logo--botnar">
										<div class="footer__partner-logo-label">Generously funded by</div>
										<a href="https://www.fondationbotnar.org/" class="footer__partner-logo-link" target="_blank" rel="noopener noreferrer">
											<img class="footer__partner-logo-img" src="/img/fondation-botnar-logo-pink-467.png" width="156" height="52" alt="Fondation Botnar logo">
										</a>
									</div>
								`,
							},
						],
					},
				],
				copyright: `
					<a href="https://gitlab.com/dial/principles/sustainability-toolkit/">Business Model Sustainability Toolkit software</a> is licensed under a <a href="https://gitlab.com/dial/principles/sustainability-toolkit/-/blob/main/LICENSE">GNU Affero GPL (AGPL) 3.0 License</a>.
					<br>
					<a href="https://gitlab.com/dial/principles/sustainability-toolkit/-/tree/main/docs/guide">Guide content</a> is licensed under a <a href="https://gitlab.com/dial/principles/sustainability-toolkit/-/blob/main/docs/guide/LICENSE">Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) License</a>.
				`,
			},
			prism: {
				theme: lightCodeTheme,
				darkTheme: darkCodeTheme,
			},
			matomo: {
				cookieDomain: process.env.MATOMO_COOKIE_DOMAIN,
				domains: (process.env.MATOMO_TRACKING_DOMAINS || '').split(','),
				doNotTrack: true,
				enabled: process.env.MATOMO_ENABLED === 'true',
				enableHeartBeatTimer: false, // Overwrite a plugin default
				matomoUrl: process.env.MATOMO_STATS_DOMAIN,
				requestMethod: false, // Overwrite a plugin default
				siteId: process.env.MATOMO_SITE_ID,
			},
		}),
}

module.exports = config
