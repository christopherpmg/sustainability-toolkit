Thank you for your contribution to making a better Business Model Sustainability Toolkit! We are tracking your contribution as issue %{ISSUE_ID} and will keep you up to date on its progress.
